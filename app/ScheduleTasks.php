<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScheduleTasks extends Model
{
    protected $table = "scheduler_tasks";
    protected $fillable = [
        "schedule_id", "title", "description", "location", "scheduled_date", "schedule_time"
    ];

    public function scheduler(){
        return $this->hasOne("App\Scheduler", "id");
    }
}
