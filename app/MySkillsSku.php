<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MySkillsSku extends Model
{
    protected $table = "my_skills_sku";
    protected $primaryKey = "sku_id";
    protected $fillable = [
        "skill_id", "currency", "price", "stripe_product_id", "active", "image"
    ];

}
