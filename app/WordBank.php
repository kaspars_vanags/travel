<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WordBank extends Model
{
    protected $primaryKey = "word_id";
    protected $table = "word_bank";

    protected $fillable = [
      "language_id", "owner_id", "word_origin", "word_translated", "audio_file"
    ];

    public function users(){
        return $this->hasOne("App\User", "user_id", "owner_id");
    }

    public function wordBankLanguages(){
        return $this->hasOne("App\WordBankLanguages", "language_id", "language_id");
    }
}
