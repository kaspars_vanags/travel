<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSettings extends Model
{
    protected $table = "user_settings";
    protected $primaryKey = "setting_id";

    protected $fillable = [
        "owner_id", "color_scheme"
    ];


    //TODO: there will be all settings like

    /*
    - Default currency
    - Default language
    - Font size?
    - etc
    */
}
