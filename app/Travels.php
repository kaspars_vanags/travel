<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Travels extends Model
{
    protected $table = "travels";

    protected $fillable = [
      "owner_id", "name", "description", "status", 'step', 'budget','currency', 'country_id', 'budget_type', 'travel_image'
    ];
}
