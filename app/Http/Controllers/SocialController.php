<?php

namespace App\Http\Controllers;

use App\User;
use App\UserSettings;
use Illuminate\Http\Request;
use Auth;
use Socialite;


class SocialController extends Controller
{

    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function providerAuth($provider){

//        if($provider != "facebook") {
            try {
                $userReal = Socialite::driver($provider)->user();
            } catch (\Exception $e) {
                flash("There was some error!", "danger");
                return redirect("/");
            }



  //      } else {
   //         $userReal = Socialite::driver($provider)->userFromToken($_GET["token"]);
   //     }

        //TODO: ADD check if system already have EMAIL so you could join accounts!
        //Check if already is registered
        $user = [
            "name" => $userReal->getName(),
            "email" => $userReal->getEmail(),
            "provider_id" => $userReal->getId(),
            "provider" => $provider,
            "access_token" => $userReal->token,
            "password" => "0"
        ];

        $dbUser = User::where("provider_id", "=", $user["provider_id"])
            ->orWhere("email", $user["email"])
            ->first();

        if(count($dbUser) == 0) {
            try {

                if(is_null($user["email"]) || $user["email"] == "") {
                    throw new \Exception("Please provide email address!");
                }

                $dbUser = User::create($user);
            } catch (\Exception $e) {
                flash($e->getMessage(), "danger");
                return redirect()->back();
            }

            UserSettings::create([
                "owner_id" => $dbUser->user_id
            ]);

        } else {
            $dbUser->provider_id = $user["provider_id"];
            $dbUser->provider = $provider;
            $dbUser->save();
        }

        Auth::login($dbUser, true);

        if(Auth::check()) {
            return redirect("/profile");
        }

        return redirect()->back();
    }


}
