<?php

namespace App\Http\Controllers\Api;

use App\General\Schedules;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ScheduleController extends ApiController
{

    /*
     * TODO:
     * Add Time column for scheduler items
     * Save items to the database
     * Get items from database
     * Edit existing items
     *
     *
     */



    public function getItems(){

        $scheduleId = $_GET["id"];
        $schedule = new Schedules($scheduleId);

        $response = [
            "dates" => $schedule->getDateList(),
            "items" => $schedule->getTasks()
        ];

        return new JsonResponse($response, 200);
    }


    public function addItem(){

    }

    public function editItem(){

    }

    public function deleteItem(){

    }

}
