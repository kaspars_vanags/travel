<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class UserController extends Controller
{

    public function getUser(){

        $user = [
            "name" => Auth::user()->name,
            "email" => Auth::user()->email,
            "about_me" => Auth::user()->about_me,
            "profile_image" => Auth::user()->profile_image
        ];

        return response()->json($user);
    }

    public function updateUser(Request $request) {

        $req = $request->all();
        $user = Auth::user();

        if(strlen($req["about_me"]) < 250) {
            User::where("user_id", $user->user_id)->update(["about_me" => $req["about_me"]]);
        }

 //       die(var_dump($req["name"]));

        return 1;
    }

}
