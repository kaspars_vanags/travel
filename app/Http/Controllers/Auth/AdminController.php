<?php

namespace App\Http\Controllers\Auth;

use App\CountryCapitals;
use App\MySkills;
use App\MySkillsSku;
use App\User;
//use Cartalyst\Stripe\Stripe;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Webpatser\Uuid\Uuid;
use Auth;

use Cartalyst\Stripe\Stripe;

//use Cartalyst\Stripe\Laravel\Facades\Stripe;
class AdminController extends Controller
{

    CONST ALLOWED_EXTENSION  = array('jpg', 'jpge', 'gif', 'png');

    public function dashboard(){
        return view("admin.pages.index");
    }

    public function users(){

        $getAllUsers = User::orderBy("user_id", "DESC")->get();
        $response = [
            "users" => $getAllUsers
        ];

        return view("admin.pages.users")->with("response", $response);
    }

    public function skills(){

        $skills = MySkills::all();
        $response = [
            "skills" => $skills
        ];

        return view("admin.pages.skills")->with("response", $response);
    }

    public function payments(){
        return view("admin.pages.payments");
    }

    public function support(){
        return view("admin.pages.support");
    }

    public function singleUser($id) {

        $user = User::where("user_id", "=", $id)->first();

        $response = [
            "user" => $user,
            "travels" => $user->Travels()->get(),
            "affiliates" => $user->AffiliateTravels()->get()
        ];

        //TODO: Add later all other information, skill options, payments, etc


        return view("admin.pages.singleuser")->with("response", $response);
    }


    public function createSkill(Request $request){

        $req = $request->all();
        $dir = public_path();

        try {
            //Check if skill already exist
            $existSkill = MySkills::where("skill_name", "=", $req["skill_name"])->first();

            if($existSkill) {
                throw new \Exception("This skill already exist!", "danger");
            }

            $stripeProduct = [
                "name" => $req["skill_name"],
                "description" => $req["skill_description"],
                "active" => ($req["skill_active"] == 1) ? true : false,
                "shippable" => false,
            ];

            //ADD IMAGE FIRST
            if(isset($req['skill_image'])) {
                $file = $req['skill_image'];
                if (filesize($req['skill_image']) > 2000699) {
                    throw new \Exception('File is too large.');
                }

                $file_extension = pathinfo($file['name']);
                if ($this->checkFileExtension($file_extension["extension"])) {
                    throw new \Exception('This file extension is not allowed.');
                }

                $file_name = Uuid::generate(4);
                $fileFullName = $file_name . '.' . $file_extension['extension'];

                move_uploaded_file($file['tmp_name'], config('app.FileDestinationPath') . "/img/skills/" . $fileFullName);

                $fullPath = "tripsavage.com/img/skills/".$fileFullName;

                $stripeProduct["images"] = [$fullPath];
            }
            //Check if price is set
            //$stripe = new Stripe(env("STRIPE_KEY"), "2017-08-15");

            $stripe = Stripe::make(env("STRIPE_SECRET"));
           //dd($stripe);


            $product =  $stripe->products()->create($stripeProduct);

            $newStripeProduct = MySkills::create([
                "skill_name" => $req["skill_name"],
                "skill_description" => $req["skill_description"],
                "skill_limits" => $req["skill_limits"],
                "skill_duration" => $req["skill_duration"],
                "skill_type" => $req["skill_type"],
                "skill_price" => $req["skill_price_gb"], //TODO: change to eur/usd/etc
                "skill_image" => (isset($req['skill_image'])) ? $fileFullName : NULL,
                "parent_skill" => $req["parent_skill"],
                "skill_active" => $req["skill_active"],
                "stripe_id" => $product["id"]
            ]);


            //create stripe sku
            //TODO: change into something more dynamical later
            if($req["skill_price_gb"] != 0) {

                $sku = $stripe->skus()->create([
                    "currency" => "gbp",
                    "inventory" => [
                        "type" => "infinite",  //Inventory type. Possible values are finite, bucket (not quantified), and infinite.
                        "value" => null, //An indicator of the inventory available. Possible values are in_stock, limited, and out_of_stock. Will be present if and only if type is bucket.
                        //"quantity" => //The count of inventory available. Will be present if and only if type is finite.
                    ],
                    "price" => ($req["skill_price_gb"]/100),
                    "product" => $product["id"]
                ]);

                MySkillsSku::create([
                    "skill_id" => $newStripeProduct["skill_id"],
                    "currency" => "gbp",
                    "price" => $req["skill_price_gb"],
                    "stripe_product_id" => $product["id"],
                    "active" => $req["skill_active"],
                ]);
            }

/*            if($req["skill_price_eur"] != 0) {

                $sku = $stripe->skus()->create([
                    "currency" => "eur",
                    "inventory" => [
                        "type" => "infinite",
                    ],
                    "price" => $req["skill_price_eur"],
                    "product" => $product["id"]
                ]);

                MySkillsSku::create([
                    "skill_id" => $newStripeProduct["skill_id"],
                    "currency" => "eur",
                    "price" => $req["skill_price_eur"],
                    "stripe_product_id" => $product["id"],
                    "active" => $req["skill_active"],
                ]);

            }

            if($req["skill_price_usd"] != 0) {

                $sku = $stripe->skus()->create([
                    "currency" => "usd",
                    "inventory" => [
                        "type" => "infinite",
                    ],
                    "price" => $req["skill_price_usd"],
                    "product" => $product["id"]
                ]);

                MySkillsSku::create([
                    "skill_id" => $newStripeProduct["skill_id"],
                    "currency" => "usd",
                    "price" => $req["skill_price_usd"],
                    "stripe_product_id" => $product["id"],
                    "active" => $req["skill_active"],
                ]);

            }*/
        } catch (\Exception $e) {
            //TODO: Change later to better error message
            flash($e->getMessage(), "danger");
            return redirect()->back();
        }


        flash("Skill has been created!", "success");
        return redirect()->back();

    }

    protected function checkFileExtension($file){

        if(array_search($file, self::ALLOWED_EXTENSION) === false) {
            return 0;
        } else {
            return 1;
        }
    }

    /*
    ALL SCRAMBLING STUFF
    */
    public function scramble(){
        return view('admin.pages.scramble');
    }

    public function scrambleUrl(Request $request){

        $req = $request->all();
        $urlContent = file_get_contents($req["url"]);

        $countryList = \GuzzleHttp\json_decode($urlContent);

/*
        {#235 ▼
            +"CountryName": "Somaliland"
        +"CapitalName": "Hargeisa"
        +"CapitalLatitude": "9.55"
        +"CapitalLongitude": "44.050000"
        +"CountryCode": "NULL"
        +"ContinentName": "Africa"
}
//"country_name", "capital_name", "lat", "long", "country_code", "continent"


*/

        foreach ($countryList as $country) {

            try {
                $newCountry = new CountryCapitals();

                $newCountry->country_name = $country->CountryName;
                $newCountry->capital_name = $country->CapitalName;
                $newCountry->lat = $country->CapitalLatitude;
                $newCountry->long = $country->CapitalLongitude;
                if ($country->CountryCode != "NULL") {
                    $newCountry->country_code = $country->CountryCode;
                }
                $newCountry->continent = $country->ContinentName;


//                $newCountry->save();

            } catch (\Exception $e) {
                dd($e);
            }
        }

        dd("success");






        //dd($countryList[0]);
      //  $ch = curl_init();
/*        curl_setopt_array($ch, array(
            CURLOPT_URL => $req["url"],
        ));

        $response = curl_exec($ch);
        dd($response[0]["CountrasdasdasasddasyName"]);*/


      /*  $urlContent = file_get_contents($req["url"]);*/

/*        $domEl = new \DOMDocument();
        $domEl->loadHTML($urlContent);

        $tableHeader = $domEl->getElementsByTagName("thead")*/

        //dd($urlContent);

    }

    public function secretLogin(){
        return view("admin.pages.login");
    }

    public function adminLogin(Request $request){

        $req = $request->all();

        if(!isset($req["email"])) {
            flash("Please enter email address!", "danger")->important();
            return redirect()->back();
        }

        if(!isset($req["password"])) {
            flash("Please enter password!", "danger")->important();
            return redirect()->back();
        }

        if(Auth::attempt(["email"=>$req["email"], "password"=>$req["password"], "admin_level"=>50])) {
            return redirect("/admin");
        }

        flash("Its Looks like you don't have access to this page!");
        return redirect()->back();
        //do stuff
    }
}
