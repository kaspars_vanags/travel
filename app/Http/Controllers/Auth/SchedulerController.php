<?php

namespace App\Http\Controllers\Auth;

use App\Scheduler;
use App\ScheduleTasks;
use App\Travels;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PhpParser\Node\Expr\Array_;

class SchedulerController extends Controller
{

    public function mainScheduler($id){
        //check if have shceduler created
        $schedule = Scheduler::where("travel_id", "=", $id)->first();
        if(!$schedule) {
            return redirect("/scheduler/register/{$id}");
        }

        $from = Carbon::createFromFormat("Y-m-d H:i:s", $schedule["date_from"]);
        $to   = Carbon::createFromFormat("Y-m-d H:i:s", $schedule["date_to"]);
        $diff = $from->diffInDays($to);

        $schedule_dates = [];
        $curr = $from;
        for($i=0; $i<=$diff; $i = $i+1) {
            $schedule_dates[$curr->toDateString()] = [];
            $curr = $from->addDay();
        }

        foreach($schedule_dates as $key => &$val) {
            $tasks = ScheduleTasks::where("scheduled_date", "=", $key)->get();
            foreach($tasks as &$tsk) {
                $tsk["location"] = json_decode($tsk["location"]);
            }
            $temp_date = Carbon::createFromFormat("Y-m-d", $key);

            $val = ["date_faker"=>$temp_date->toFormattedDateString(), "tasks"=>$tasks];
        }

        $response = [
            "scheduleId"=>$id,
            "scheduler"=>$schedule_dates,
            "locations"=>json_decode($schedule["locations"])
        ];

        return view("auth.schedule")->with("response", $response);
    }

    public function creatingPage($id) {
        $travel = Travels::where("id", "=", $id)->first();
        return view("auth.modal.scheduler")->with("travel", $travel);
    }

    public function createScheduler(Request $request, $id){
        $req = $request->all();

        try {

            $from = Carbon::createFromFormat("Y-m-d H:i:s", $req["date-from"]." 00:00:00");
            $to = Carbon::createFromFormat("Y-m-d H:i:s", $req["date-to"]." 23:59:59");

            $object = [];
            $_temp = true;
            $_counter = 0;

            while ($_temp) {
                $_key1 = "location_".$_counter;
                $_key2 = "location_color_".$_counter;

                if(isset($req[$_key1])) {
                    $object[$_counter] =  array($req[$_key1], $req[$_key2]);
                    $_counter ++;
                } else {
                    $_temp = false;
                }
            }

            $locations = json_encode($object);

            Scheduler::create([
                "travel_id"=>$id,
                "date_from"=>$from,
                "date_to"=>$to,
                "locations"=>$locations
            ]);

        } catch(\Exception $e) {
            dd($e->getMessage());
        }
        return redirect("/scheduler/{$id}");
    }

    public function createTask(Request $request) {

        $req = $request->all();

        try {
         //   $_l = explode(",", $req["location"]);
        //    $locations = [$_l[0], $_l[1]];

            ScheduleTasks::create([
                "schedule_id" => $req["schedule_id"],
                "title" => $req["title"],
                "description" => $req["description"],
                "location" => $req["location"],
                "scheduled_date" => $req["scheduled_date"],
                "schedule_time" => $req["schedule_time"]
            ]);

        } catch (\Exception $e) {
            $response = [
                "success" => 0,
                "message" => $e->getMessage()
            ];

            return new JsonResponse($response, 500);
        }


        $response = [
            "success" => 1,
            "message" => "Item has been added"
        ];

        return new JsonResponse($response, 200);
        // Change it as it uses now vue and should not reload page
       // return redirect()->back();
    }

    public function editTask(Request $request, $taskId) {

        $req = $request->all();

/*        $_l = explode(",",$req["location"]);
        $locations = [$_l[0], $_l[1]];*/
//die(var_dump($req));
        try {
            $edit = ScheduleTasks::where("id", "=", $taskId)
                ->update([
                    "title" => $req["title"],
                    "description" => $req["description"],
                    "location" => $req["location"]
                ]);

        } catch (\Exception $e) {

            $response = [
                "success" => 0,
                "message" => $e->getMessage()
            ];

            return new JsonResponse($response, 500);
        }

        $response = [
            "success" => 1,
            "message" => "Task is updated"
        ];

        return new JsonResponse($response, 200);
    }

    public function deleteTask($id){
        try {

            ScheduleTasks::where("id", "=", $id)->delete();

        } catch (\Exception $e) {

            $response = [
                "success" => 0,
                "message" => $e->getMessage()
            ];

            return new JsonResponse($response, 500);

        }

        $response = [
            "success" => 1,
            "message" => "Task has been deleted"
        ];

        return new JsonResponse($response, 200);
    }


    public function addLocation(Request $request){

        $req = $request->all();

        try {
            $existingLocation = Scheduler::where("travel_id", $req["scheduler_id"])
                ->first();

            $locations = json_decode($existingLocation["locations"]);

            $newLocation = array($req["location"], $req["location-color"]);
            array_push($locations, $newLocation);

            Scheduler::where("travel_id", $req["scheduler_id"])
                ->update(["locations" => json_encode($locations)]);

        } catch (\Exception $e) {
            flash("We have problems with your request! Please try again.", "danger");
            return redirect()->back();
        }

        flash("Location has been added!", "success");
        return redirect()->back();
    }


}
