<?php

namespace App\Http\Controllers\Auth;

use App\AffiliateTravels;
use App\Travels;
use App\User;
use App\UserSettings;
use App\WordBank;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Webpatser\Uuid\Uuid;

class AuthController extends Controller
{
    public function index(){

        $user = Auth::user();

        $travels = Travels::where("owner_id", "=", $user->user_id)
            ->join("country_capitals as cp", "cp.capital_id", "=", "travels.country_id")
            ->select("travels.*", "cp.country_name", "cp.capital_name")
            ->orderBy("created_at", "DESC")->get();
        $affiliates = AffiliateTravels::where("user_id", "=", $user->user_id)->where("owner_id", "!=",  $user->user_id)->get();
        $words = WordBank::where("owner_id", $user->user_id)->get()->count();

/*        foreach($affiliates as &$af) {

            $af["owner_name"] = $af->user()->name;
            $af["travel_name"] = $af->travel()->name;
        }
        */
        $travel_count = count($travels);
        $affiliate_count = count($affiliates);

        //$lastTravels =

        $response = [
            "travels_total"=>$travel_count,
            "affiliate_total"=>$affiliate_count,
            "words_saved" =>$words,
            "travels" => $travels->filter(function($e){ return $e["step"] == 3;})->slice(0,3)
        ];

        return view("auth.feed")->with("response", $response);
    }

    public function addTravel(Request $request){

        $req = $request->all();
        //TODO: Add custom starting point

        //Check if user can create new travel
        $travelAmount = 3;

        if(Auth::user()->hasSkill(4)){ //id of skill
            $travelAmount = 10;
        }

        $allTravels = Travels::where("owner_id", "=", Auth::user()->user_id)->get();
        if(count($allTravels) >= $travelAmount) {
            flash("You have reached maximum travel destination count!", "danger")->important();
            return redirect()->back();
        }

        //If limit is not excited then create new travel destination
        try {
            $travel = Travels::create($req);
        } catch (\Exception $e) {
            return redirect()->back();
        }

        flash('Travel destination has been created', 'success');
        return redirect()->back();
    }

}
