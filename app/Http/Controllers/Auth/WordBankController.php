<?php

namespace App\Http\Controllers\Auth;

use App\WordBank;
use App\WordBankLanguages;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Auth\AuthController;
use Auth;

class WordBankController extends AuthController
{

    public function words(){

        $user = Auth::user();
        $languages = WordBankLanguages::where("owner_id", "=", $user["user_id"])->get();
        $words = WordBank::where("word_bank.owner_id", "=", $user["user_id"])
            ->join("word_bank_languages", "word_bank_languages.language_id","word_bank.language_id")
            ->select("word_bank.*", "word_bank_languages.long_name")
            ->get();

        $response = [
            "languages" => $languages,
            "words" => $words
        ];

        return view("auth.words")->with("response", $response);
    }

    public function addLanguage(Request $request) {

        $req = $request->all();

        if(!isset($req["short_name"]) || empty($req["short_name"])) {
            $shortName = $this->shortNameGenerator($req["long_name"]);
        } else {
            $shortName = $req["short_name"];
        }

        WordBankLanguages::create([
            "long_name" => $req["long_name"],
            "short_name" => $shortName,
            "owner_id" => Auth::user()->user_id
        ]);

        flash("Language has been added.", "success");
        return redirect()->back();
    }

    public function addWord(Request $request){

        $req = $request->all();

        try {
            WordBank::create([
                "language_id" => $req["language"],
                "owner_id" => Auth::user()->user_id,
                "word_origin" => $req["original_word"],
                "word_translated" => $req["translated_word"]
            ]);

        } catch (\Exception $e) {
            flash($e->getMessage(), "danger")->important();
            return redirect()->back();
        }

        flash("Word has been added", "success");
        return redirect()->back();
    }

    protected function shortNameGenerator($longName){
        $temp = implode(',',$longName);

        if($temp[0] == " ") {
            $temp[0] = "W";
        }
        if($temp[1] == " " || is_null($temp[1])) {
            $temp[1] = "B";
        }

        return $temp[0].$temp[1];
    }
}
