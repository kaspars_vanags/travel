<?php

namespace App\Http\Controllers\Auth;

use App\AffiliateTravels;
use App\CountryCapitals;
use App\Scheduler;
use App\TravelBasket;
use App\TravelInvitationQueue;
use App\Travels;
use App\TravelStops;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\DB;
use Psy\Util\Json;

class TravelController extends Controller
{

    public function trips(){

        $user = Auth::user();

        $travels = Travels::where("travels.owner_id", "=", $user->user_id)
            ->join("country_capitals as cp", "cp.capital_id", "=", "travels.country_id")
            ->leftJoin("affiliate_travels as at", function($join) use ($user) { 
                $join->on("at.travel_id", "=", "travels.id")->where("at.user_id", "=", $user->user_id);
            })
            ->select("travels.*", "cp.country_name", "cp.capital_name", "cp.country_code", "at.token")
            ->orderBy("created_at", "DESC")->get();

        //Travels::where("owner_id", "=", $user->user_id)->get();
        $affiliates = AffiliateTravels::where("user_id", "=", $user->user_id)->get();

        foreach($affiliates as &$af) {

            $af["owner_name"] = $af->user()->name;
            $af["travel_name"] = $af->travel()->name;
        }

        $response = [
            "travels"=>$travels,
            "affiliates"=>$affiliates
        ];

        return view("auth.trips")->with("response", $response);
    }

    public function getTravel($id){

        $response = [
            "travel_id"=>$id
        ];

        return view("auth.map")->with("response", $response);
    }

    public function getMarkers($id){
        $markers = TravelStops::where("travel_id", "=", $id)->orderBy("status", "asc")->get();
/*
        $list = [];

        foreach ($markers as $marker) {
            $list[] = [$marker["info"], $marker["lat"], $marker["lng"]];
        }*/

        return new JsonResponse($markers, 200);
    }

    public function addMarker(Request $request){

        $req = $request->all();

        try {
            $data = [
                "travel_id"=>$req["travel_id"],
                "name"=>$req["info"],
                "info"=>"",
                "lat"=>$req["lat"],
                "lng"=>$req["long"],
                "adress"=>"",
                "img_link"=>""
            ];

            $travel = TravelStops::create($data);

        }catch (\Exception $e) {
            $response = [
                "status"=>"ERROR",
                "message"=>$e->getMessage()
            ];

            return new JsonResponse($response, 500);
        }

        $response = [
            "status"=>"SUCCESS",
            "message"=>"Marker has been added"
        ];

        return new JsonResponse($response, 200);
    }

    public function editMarker(Request $request) {

        $req = $request->all();

        TravelStops::where("id", "=", $req["id"])
            ->update([
                "name"=>$req["name"],
                "info"=>$req["info"] ? $req["info"] : "",
                "img_link"=>$req["img_link"] ? $req["img_link"] : "",
                "status"=>$req["status"],
                "type"=>$req["type"]
            ]);

        return redirect()->back();
    }

    public function deleteMarker($id) {

        TravelStops::where("id", "=", $id)->delete();

        $response = [
            "status"=>"SUCCESS",
            "message"=>"Marker has been deleted"
        ];

        return new JsonResponse($response, 200);
    }

    public function createAffiliate($id){

        try {

            $is_affiliated = AffiliateTravels::where('travel_id', '=', $id)->first();

            if($is_affiliated) {
                throw new \Exception('You already created link for this trip.');
            }

            $travel_info = Travels::where("id", "=", $id)->first();



            $token = md5(Auth::user()->name."travelIsCool".$travel_info["name"]);

            AffiliateTravels::create([
                "owner_id"=>Auth::user()->user_id,
                "user_id"=>Auth::user()->user_id,
                "token"=>$token,
                "travel_id"=>$id,
            ]);

        } catch (\Exception $e) {
            flash($e->getMessage(), 'danger');
            return redirect()->back();
        }

        flash('Affiliate trip has been created.', 'success');
        return redirect()->back();
    }


    public function openTravel($id){

        $user = Auth::user();

        //redirect if step is not 1 and there is no travel id
/*        if($step > 1 && $id == 0) {
            flash("Something wrong with your request!", "danger");
            return redirect("/profile");
        }*/


        $response = [
            "id" => (isset($id)) ? $id : 0,
            "hasValues" => 0,
        ];

        $countries = CountryCapitals::orderBy("country_name", "asc")->get();
        $response["countries"] = $countries;

        return view("auth.travel")->with("response", $response);

        //CHANGE TO VUE

        if($id != 0) {

            $travel = Travels::where("id", "=", $id)
                ->where("owner_id", "=", $user->user_id)
                ->first();

            if(count($travel) == 0) {
                flash("I guess this is not your trip!", "danger");
                return redirect("/profile");
            }

        }

        $response = [
            "id" => (isset($id)) ? $id : 0,
            "step" => (isset($step)) ? $step : 1,
            "hasValues" => 0,
        ];


        if($step == 1) {

            //get all countries
            $countries = CountryCapitals::orderBy("country_name", "asc")->get();
            //dd($countries);
            $response["countries"] = $countries;

            if($response["id"] != 0) {
                //if not 0 then get existing travel
                $existingTravel = Travels::where("id", "=", $response["id"])
                    ->where("owner_id", "=", Auth::user()->user_id)
                    ->first();

                if(count($existingTravel) > 0) {
                    $response["hasValues"] = 1;
                    $response["travelDetails"] = $existingTravel;
                }
            }

        }

        if($step == 2) {

            $existingTravel = Travels::where("id", "=", $response["id"])
                ->where("owner_id", "=", $user->user_id)
                ->first();

            if(count($existingTravel) == 0) {
                flash("We could not retrieve your travel!", "danger");
                return redirect("/profile");
            }

            $response["locations"] = 0;

            if($existingTravel->step == 2) {
                $response["hasValues"] = 1;
                $response["scheduler"] = Scheduler::where("travel_id", "=", $response["id"])->first();

                $dateFrom = Carbon::createFromFormat("Y-m-d H:i:s", $response["scheduler"]->date_from);
                $dateTo = Carbon::createFromFormat("Y-m-d H:i:s", $response["scheduler"]->date_to);

                $response["scheduler"]->date_from = $dateFrom->year."-".$dateFrom->month."-".$dateFrom->day;
                $response["scheduler"]->date_to = $dateTo->year."-".$dateTo->month."-".$dateTo->day;
            }

        }

        if($step == 3) {
            $invitations = TravelInvitationQueue::where("owner_id", $user->user_id)
                ->where("travel_id", $response["id"])
                ->get();

            if(count($invitations) > 0) {
                $response["hasValues"] = 1;
                $response["invitations"] = $invitations;
            }

            $response["invitation_count"] = 3;
        }


        if($step == 4) {
            //TODO: Do I need this?
        }


        return view("auth.travel")->with("response", $response);
    }


    public function addTravel(Request $request){

        $req = $request->all();
        $user = Auth::user();

        if($req["step"] == 1) {

            //check lenght
            if(strlen($req["name"]) < 5 || strlen($req["name"]) > 40) {
                flash("Name should be between 5 and 40 characters!", "danger");
                return redirect()->back();
            }

            if(strlen($req["currency"]) > 15 && $req["currency"] != "") {
                flash("Currency should not exceed 15 characters.", "danger");
                return redirect()->back();
            }

            $travel = Travels::updateOrCreate(
                ["id"=>$req["travel_id"], "owner_id"=>$user->user_id],
                $req
            );

            return redirect("/travel/".$travel->id."/step/2");
        }

        if($req["step"] == 2) {
            $from = Carbon::createFromFormat("Y-m-d H:i:s", $req["date-from"]." 00:00:00");
            $to = Carbon::createFromFormat("Y-m-d H:i:s", $req["date-to"]." 23:59:59");

            $object = [];
            $_counter = 0;
            if(!is_null($req["location"])) {
                foreach ($req["location"] as $key => $value) {
                    $object[$_counter] = array($value, $req["location_color"][$key]);
                    $_counter++;
                }
            }

            $locations = json_encode($object);

            $newScheduler = [
                "travel_id"=>$req["travel_id"],
                "date_from"=>$from,
                "date_to"=>$to,
                "locations"=>$locations
            ];

            //TODO: Add owner column to table

            $this->updateStep($req["travel_id"], 2);

            Scheduler::updateOrCreate(
                ["travel_id" => $req["travel_id"]],
                $newScheduler
            );

            return redirect("/travel/".$req["travel_id"]."/step/3");
        }

        if($req["step"] == 3) {

            $emails = $req["invite"];
            $isCreated = false;

            $is_affiliated = AffiliateTravels::where('travel_id', '=', $req["travel_id"])->first();

            if($is_affiliated) {
                $isCreated = true;
            }

            $travelInfo = Travels::where("id", "=", $req["travel_id"])->first();

            if(!$isCreated) {
                $token = md5($user->name . "travelIsCool" . $travelInfo["name"]);

                AffiliateTravels::create([
                    "owner_id" => $user->user_id,
                    "user_id" => $user->user_id,
                    "token" => $token,
                    "travel_id" => $req["travel_id"],
                ]);
            } else {
                $token = $is_affiliated->token;
            }

            foreach ($emails as $em) {
                if(!is_null($em)) {
                    TravelInvitationQueue::updateOrCreate(["travel_id"=>$req["travel_id"], "receiver_email"=>$em],[
                        "owner_id" => $user->user_id,
                        "receiver_email" => $em,
                        "travel_id" =>$req["travel_id"],
                        "token" => $token
                    ]);
                }
            }

            $this->updateStep($req["travel_id"], 3);
            return redirect("/travel/".$req["travel_id"]."/step/4");
        }

    }

    protected function updateStep($id, $step){

        $user = Auth::user();

        Travels::where("id", $id)
            ->where("owner_id", "=", $user->user_id)
            ->update(["step" => $step]);
    }


    /*
     *
     * TRAVEL BASKET CONTROLLER
     *
     */

    public function basket($id){

        $user = Auth::user();

        $basketItems = TravelBasket::where("owner_id", "=", $user->user_id)
            ->where("travel_id", $id)
            ->get();

        $travel = Travels::where("owner_id", $user->user_id)
            ->where("id", $id)
            ->first();

        if(!$travel) {
            flash("There is something wrong with your request!","danger");
            return redirect()->back();
        }

        $response = [
            "travel" => $travel,
            "basket" => $basketItems,
            "amount" => count($basketItems)
        ];

        return view("auth.basket")->with("response", $response);
    }

    public function setBudget(Request $request, $id) {

        $user = Auth::user();
        $req = $request->all();

        Travels::where("id", $id)
            ->where("owner_id", "=", $user->user_id)
            ->update(["currency"=>$req["currency"], "budget" => $req["budget"], "budget_type" => $req["budget_type"]]);

        return redirect()->back();
    }

    public function addItem(Request $request, $id) {

        $req = $request->all();
        $user = Auth::user();

        $body = [
            "owner_id" => $user->user_id,
            "item_name" => $req["item_name"],
            "description" => $req["description"],
            "price" => $req["price"],
            "optional" => $req["optional"],
            "travel_id" => $id
        ];

        $travel = Travels::where("id", $id)->first();

        if($travel["budget_type"] == "strict" && (!is_null($travel["budget"]) && $travel["budget"] > 0)) {

            $countBasket = DB::table("travel_basket")
                ->where("travel_id", "=", $id)
                ->sum("price");

            $countBasket+= $req["price"];

            if($countBasket > $travel["budget"]) {
                flash("Your budget is in 'Strict' mode. You can't go over the budget.")->important();
                return redirect()->back();
            }

        }


        TravelBasket::updateOrCreate(["basket_id" => $req["item_id"]],
            $body);

        return redirect()->back();
    }

    public function removeItem(Request $request, $id) {
        $req = $request->all();
        $user = Auth::user();

        TravelBasket::where("basket_id", $req["item_id"])
            ->where("owner_id", $user->user_id)
            ->delete();

        return redirect()->back();
    }



    /*
     *
     * Travel API
     *
     */

    public function fetchTravel($id){

        $user = Auth::user();
        $response = [];

        $travel = Travels::where("travels.id", $id)
            ->where("travels.owner_id", $user->user_id)
            ->leftJoin("scheduler", "scheduler.travel_id", "=", "travels.id")
            ->select("travels.*", "scheduler.date_from", "scheduler.date_to")
            ->first();

        if($travel) {
            $response["payload"] = $travel;
            $response["success"] = 1;
        } else {
            $response["success"] = 0;
        }

        $response["invites"] = [];
        $invites = TravelInvitationQueue::where("travel_id", "=", $id)->get();

        $response["invites"] = $invites;

        return new JsonResponse($response,200 );
    }


    public function createTravel(Request $request) {
        $req = $request->all();
        $user = Auth::user();
        $response = [];
        $travel = [];

        //dd($req);
        //  id: "0", name: null, description: null, status: null, country_id: "10", date_from: null,…}

        //check name
        if(is_null($req["name"]) || $req["name"] == "") {
            $country = CountryCapitals::where("capital_id", "=", $req["country_id"])->first();
            $travel["name"] = $country["country_name"];
        } elseif(strlen($req["name"]) > 45) {
            $response["success"] = 0;
            $response["error"] = 1;
            $response["message"] = "Travel name is too long";
            return new JsonResponse($response,200 );
        } else {
            $travel["name"] = $req["name"];
        }

        //check description length
        if(!is_null($req["description"]) && strlen($req["description"]) > 1400) {
            $response["success"] = 0;
            $response["error"] = 1;
            $response["message"] = "Description is too long!";
            return new JsonResponse($response,200 );
        }

        $travel["description"] = $req["description"];
        $travel["step"] = ($req["step"] >= 3) ? $req["step"] : $req["step"]+1;
        $travel["country_id"] = $req["country_id"];
        $travel["owner_id"] = $user->user_id;
        $response["success"] = 1;

        //create new travel or not
        $newTravel = Travels::updateOrCreate(["id" => $req["id"]],
            $travel);

        $response["payload"] = $newTravel;

        //check both dates
        if(!is_null($req["date_from"]) && $req["date_from"] != "") {
            //if date is correct
            $dateFrom = Carbon::createFromFormat("Y-m-d H:i:s", $req["date_from"]." 00:00:00");

            if(!is_null($req["date_to"])) {
                $dateTo = Carbon::createFromFormat("Y-m-d H:i:s", $req["date_to"] . " 23:59:59");
            } else {
                $dateTo = Carbon::createFromFormat("Y-m-d H:i:s", $req["date_from"]." 23:59:59");
            }

            $diff = $dateFrom->diffInMinutes($dateTo);

            if($diff < 0) {
                $response["success"] = 0;
                $response["error"] = 1;
                $response["message"] = "'Date To' is before 'Date From'";
                return new JsonResponse($response,200 );
            }

            Scheduler::updateOrCreate(
                ["travel_id" => $newTravel["id"]],
                [
                    "travel_id" => $newTravel["id"],
                    "date_to" => $dateTo,
                    "date_from" => $dateFrom
                ]
            );

        }


        $emails = $req["invite"];
        $isCreated = false;

        $is_affiliated = AffiliateTravels::where('travel_id', '=', $newTravel["id"])->first();

        if($is_affiliated) {
            $isCreated = true;
        }

        if(!$isCreated) {
            $token = md5($user->name . "travelIsCool" . $newTravel["name"]);

            AffiliateTravels::create([
                "owner_id" => $user->user_id,
                "user_id" => $user->user_id,
                "token" => $token,
                "travel_id" => $newTravel["id"],
            ]);
        } else {
            $token = $is_affiliated->token;
        }

        foreach ($emails as $em) {
            if(!is_null($em)) {
                TravelInvitationQueue::updateOrCreate(["travel_id"=>$newTravel["id"], "receiver_email"=>$em],[
                    "owner_id" => $user->user_id,
                    "receiver_email" => $em,
                    "travel_id" =>$newTravel["id"],
                    "token" => $token
                ]);
            }
        }


        return new JsonResponse($response,200 );
    }



}
