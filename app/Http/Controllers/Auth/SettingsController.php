<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\UserSettings;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Webpatser\Uuid\Uuid;

class SettingsController extends Controller
{



    public function index(){
        return view("auth.settings.profile");
    }

    public function updateProfileImage(Request $request) {

        $dir = public_path();
        $user = Auth::user();

        $allowed_ext = array('jpg', 'jpge', 'gif', 'png');

        $req = $request->all();
        try {
            if(!isset($req['profile_image']) || is_null($req['profile_image'])) {
                throw new \Exception('Please add image to upload.');
            }

            $file = $_FILES['profile_image'];
            if(filesize($req['profile_image']) > 2000699) {
                throw new \Exception('File is too large.');
            }

            $file_extension = pathinfo($file['name']);
            if(array_search($file_extension['extension'], $allowed_ext) === false) {
                throw new \Exception('This file extension is not allowed.');
            }
            //check if has directory
            /*            if(!is_dir($dir."/img/users/".$user->user_id."/")) {
                            mkdir($dir."/img/users/".$user->user_id."/");

                          // $place = 'img\teens\\';
                        }*/
            $file_name = Uuid::generate(4);
            $fileFullName = $file_name.'.'.$file_extension['extension'];
            $uploadDir = config('app.FileDestinationPath')."/img/users/".$user->user_id;

            if(!is_dir($uploadDir)) {
                mkdir($uploadDir);
            }

            move_uploaded_file($file['tmp_name'], $uploadDir."/".$fileFullName);
        } catch (\Exception $e) {
            flash($e->getMessage(), 'danger')->important();
            return redirect()->back();
        }

        User::where('user_id', '=', $user->user_id)->update(["profile_image"=>$fileFullName]);
        flash('Changes has been saved', 'success');
        return redirect()->back();
    }

    public function changeSettings(Request $request) {
        $user = Auth::user();

        $req = $request->all();

        UserSettings::where("owner_id", "=", $user->user_id)
            ->update(["color_scheme"=>$req["color_scheme"]]);

        return redirect()->back();
    }


}
