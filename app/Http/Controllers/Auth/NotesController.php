<?php

namespace App\Http\Controllers\Auth;

use App\Notes;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;

class NotesController extends Controller
{
    public function notes(){

        $notes = Notes::where('owner_id', '=', Auth::user()->user_id)
            ->orderBy("created_at", "DESC")
            ->get();

        $response = [
          'notes'=>$notes
        ];

        return view('auth.notes')->with('response', $response);
    }


    public function createNote(Request $request){

        $req = $request->all();
        $user = Auth::user();
        try {

            if(!isset($req['title']) && !isset($req['body'])){
                throw new \Exception('Set at least one of two attributes (Title/Description)');
            }

            if(strlen($req["title"]) > 41) {
                throw new \Exception('Title exceeded 40 characters.');
            }

            if(strlen($req["body"]) > 1401 && $req["body"] != "") {
                throw new \Exception('Title exceeded 1400 characters.');
            }

            //TODO: later need to check type and if it has file
            Notes::updateOrCreate([
                "notes_id" => $req["notes_id"], "owner_id" => $user->user_id
            ],[
                'owner_id'=>$user->user_id,
                'title'=>$req['title'],
                'body'=>$req['body'],
                'notes_color'=>$req['notes_color']
            ]);

        } catch (\Exception $e) {
            flash($e->getMessage(), 'danger')->important();
            return redirect()->back();
        }

        flash('Note has been created.', 'success');
        return redirect()->back();

    }

    public function removeNote(Request $request){

        $req = $request->all();
        $user = Auth::user();
        try {

            Notes::where("owner_id", "=", $user->user_id)
                ->where("notes_id", "=", $req["note_id"])
                ->delete();

        } catch (\Exception $e) {
            flash("We failed to delete '".$req["title"]."' note!", "danger")->important();
            return redirect()->back();
        }

        flash("Note has been removed.", "success");
        return redirect()->back();
    }
}
