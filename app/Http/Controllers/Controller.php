<?php

namespace App\Http\Controllers;

use App\AffiliateTravels;
use App\Travels;
use App\User;
use App\UserSettings;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function index(){

        if(Auth::check()) {
            return redirect("/feed");
        }

        $result = DB::table("travels")
            ->select(DB::raw("count(*) as row_count"))
            ->first();

        $response = [
            "travel_count" => $result->row_count
        ];

        return view("pages.login")->with("response", $response);
    }

    public function logout(){
        Auth::logout();

        return redirect("/");
    }

    public function login(Request $request){

        $req = $request->all();

        if (Auth::attempt(["name" => $req["name"], "password" => $req["password"] ])) {
            return redirect("/feed");
        }

        flash("Incorrect Username or Password! Please try again!", "danger")->important();
        return redirect()->back();
    }

    public function register(Request $request){

        $req = $request->all();

        try {
            $req["password"] = bcrypt($req["password"]);

            $user = User::create($req);

            UserSettings::create([
                "owner_id" => $user->user_id
            ]);

            Auth::login($user, true);
        } catch (\Exception $e) {
            return redirect()->back();
        }

        return redirect("/feed");
    }

    public function openTravel($token){

        $travel = AffiliateTravels::where("token", "=", $token)->first();

        if(count($travel) <=0){
            return redirect("/");
        }

        $owner = User::where("user_id", "=", $travel["owner_id"])->first();
        $real_travel = Travels::where('id', '=', $travel['travel_id'])->first();

        return view("pages.affiliate")->with("token", $token)->with("name", $owner["name"])->with("travel_name", $real_travel['name']);

      // /affiliate/join/8421ac8e37cc0ddd58e90c78511e9764


    }

    public function joinTravel($token){

        $travel = AffiliateTravels::where("token", "=", $token)->first();

        if(count($travel) <=0){
            return redirect()->back();
        }

        $owner = User::where("user_id", "=", $travel["owner_id"])->first();

        AffiliateTravels::create([
            "owner_id"=>$owner["user_id"],
            "user_id"=>Auth::user()->user_id,
            "travel_id"=>$travel["travel_id"],
            "token"=>$token
        ]);

        return redirect("/feed");
    }

    public function errorPage($code){

        $response = [
            "code" => $code,
        ];

        switch ($code) {
            case 404: {
                $response["message"] = "Sorry, we can't find this page!";
                $response["description"] = "We think this page never existed, but if you think otherwise, then please contact us!";
                break;
            }
            case 500: {
                $response["message"] = "Server could not complete your request!";
                $response["description"] = "We don't know why, but you could try again!";
                break;
            }
            default: {
                $response["message"] = "Something went bad!";
                $response["description"] = "Like so bad, that we even don't know what!";
                break;
            }
        }

        return view("pages.errors.404")->with("response", $response);
    }


    public function maintenance(){
        return view("pages.nope");
    }

}
