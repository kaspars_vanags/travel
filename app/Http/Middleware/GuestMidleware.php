<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class GuestMidleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(env("APP_MAINTENANCE")) {
            if (!Auth::check() || Auth::user()->admin_level < 30) {
                return redirect("/nope");
            }
        }

        return $next($request);
    }
}
