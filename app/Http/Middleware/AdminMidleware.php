<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AdminMidleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(!Auth::check()) {
            return redirect("/");
        }

        if(Auth::user()->admin_level < 40) {
            return redirect("/");
        }

        return $next($request);
    }
}
