<?php
/**
 * Created by PhpStorm.
 * User: Kaspars
 * Date: 3/25/2018
 * Time: 14:47
 */

namespace App\General;


use App\Scheduler;
use App\ScheduleTasks;
use Carbon\Carbon;

class Schedules
{

    const WEEK_DAYS = array("Sunday", "Monday","Tuesday","Wednesdays", "Thursday", "Friday", "Saturday");

    private $scheduleId;
    private $travelId;

    private $dateFrom;
    private $dateTo;
    private $dateList = [];

    private $locations = [];

    private $tasks = [];


    public function setScheduleId($id){
        $this->scheduleId = $id;
        return $this->scheduleId;
    }

    public function getScheduleId(){
        return $this->scheduleId;
    }

    public function setTravelId($id){
        $this->travelId = $id;
        return $this->travelId;
    }

    public function getTravelId(){
        return $this->travelId;
    }

    public function setDateFrom($date){
        $this->dateFrom = $date;
        return $this->dateFrom;
    }

    public function getDateFrom(){
        return $this->dateFrom;
    }

    public function setDateTo($date){
        $this->dateTo = $date;
        return $this->dateTo;
    }

    public function getDateTo(){
        return $this->dateTo;
    }

    public function setLocations($locations){
        $this->locations = $locations;
        return $this->locations;
    }

    public function getLocations(){
        return $this->locations;
    }

    public function getTasks(){
        return $this->tasks;
    }

    public function getDateList(){
        return $this->dateList;
    }

    public function __construct($id = "")
    {

        Carbon::setWeekStartsAt(Carbon::MONDAY);
        Carbon::setWeekEndsAt(Carbon::SUNDAY);

        if($id != "") {

            $schedule = Scheduler::where("travel_id", $id)->first();

            if(is_null($schedule)) {
                return;
            }

            $this->scheduleId = $schedule["id"];
            $this->travelId = $schedule["travel_id"];
            $this->dateFrom = Carbon::createFromFormat("Y-m-d H:i:s", $schedule["date_from"]);
            $this->dateTo = Carbon::createFromFormat("Y-m-d H:i:s", $schedule["date_to"]);
            $this->locations = $schedule["locations"];

            $this->tasks = $this->setTasks($id);
            $this->setDateList();

            return;
        }
    }

    private function setTasks($id){
        $tasks = ScheduleTasks::where("schedule_id", $id)->orderBy("scheduled_date", "DESC")->get();
        $newTasks = [];

        foreach ($tasks as $key => &$val) {
           // $val["location"] =  json_decode($val["location"]);
            $val["schedule_time"] = substr($val["schedule_time"], 0, (strlen($val["schedule_time"]) - 3)); // cut microseconds
            $newTasks[$val["scheduled_date"]][] = $val;
        }

        return $newTasks;
    }

    private function setDateList(){
        $diff = $this->dateFrom->diffInDays($this->dateTo);
        $curr = $this->dateFrom;

        for($i=0; $i<=$diff; $i = $i+1) {

            $this->dateList[] = array(
                "name" => $this->getDayName($curr->dayOfWeek),
                "date" => $curr->day,
                "fullDate" => $curr->toDateString(),
            );
          //  var_dump($dt->dayOfWeek);
           //$schedule_dates[$curr->toDateString()] = [];
            $curr = $curr->addDay();
        }

        return $this->dateList;
    }

    private function getDayName($day){
        return self::WEEK_DAYS[$day];
    }





}

