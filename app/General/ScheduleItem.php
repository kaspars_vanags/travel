<?php
/**
 * Created by PhpStorm.
 * User: Kaspars
 * Date: 3/25/2018
 * Time: 13:16
 */

namespace App\General;


use App\ScheduleTasks;
use Carbon\Carbon;

class ScheduleItem
{


    private $taskId;
    private $scheduleId;

    private $title = "";
    private $description = "";
    private $location;

    private $scheduleDate;
    private $scheduleTime;

    public function setTaskId($id) {
        $this->taskId = $id;
        return $this->taskId;
    }

    public function getTaskId(){
        return $this->taskId;
    }

    public function setScheduleId($id){
        $this->scheduleId = $id;
        return $this->scheduleId;
    }

    public function getScheduleId() {
        return $this->scheduleId;
    }

    public function setTitle($title){
        $this->title = $title;
        return $this->title;
    }

    public function getTitle(){
        return $this->title;
    }

    public function setDescription($description) {
        $this->description = $description;
        return $this->description;
    }

    public function getDescription(){
        return $this->description;
    }

    public function setScheduleDate($date = "") {

        if($date == "") {
            $date = Carbon::now();
        }

        $this->scheduleDate = $date;
        return $this->scheduleDate;
    }

    public function getScheduleDate() {
        return $this->scheduleDate;
    }

    public function setScheduleTime($time = "") {

        if($time == "") {
            $time = Carbon::now()->format("h:i:s");
        }

        $this->scheduleTime = $time;
        return $this->scheduleTime;
    }

    public function getScheduleTime(){
        return $this->scheduleTime;
    }


    public function setLocation($location){
        $this->location = $location;
        return $this->location;
    }

    public function getLocation() {
        return $this->location;
    }



    public function __construct($id = "")
    {

        if($id != "") {
            $task = ScheduleTasks::where("id", $id)->first();

            if($id){
                $this->taskId = $id;
                $this->scheduleId = $task["schedule_id"];
                $this->title = $task["title"];
                $this->description = $task["description"];
                $this->location = $task["location"];
                $this->scheduleDate = $task["scheduled_date"];
                $this->scheduleTime = $task["schedule_time"];

                return;
            }
          //  $this->taskId =
        }

    }


}