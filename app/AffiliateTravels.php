<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AffiliateTravels extends Model
{
    protected $table = "affiliate_travels";

    protected $fillable = [
      "travel_id", "owner_id", "user_id", "token"
    ];

    public function travel(){
        return Travels::where("id", "=", $this->travel_id)->first();
    }

    public function user(){
        return User::where("user_id", "=", $this->owner_id)->first();
    }
}
