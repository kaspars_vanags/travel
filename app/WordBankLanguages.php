<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WordBankLanguages extends Model
{
    protected $primaryKey = "language_id";
    protected $table = "word_bank_languages";

    protected $fillable = [
      "owner_id", "long_name", "short_name"
    ];

    public function users(){
        return $this->hasOne("App\User", "user_id", "owner_id");
    }
}
