<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notes extends Model
{
    protected $primaryKey = "notes_id";
    protected $table = "notes";

    protected $fillable = [
      "owner_id", "title", "body", "notes_type", "notes_color"
    ];

}
