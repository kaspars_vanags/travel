<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TravelStops extends Model
{
    protected $table = "travel_stops";

    protected $fillable = [
        "travel_id", "name", "lat", "lng", "info","adress", "img_link", "status", "type"
    ];

}
