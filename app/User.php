<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    protected $primaryKey = 'user_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','profile_image', 'about_me', 'country', "provider_id", "access_token", "provider"
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function Travels(){
        return $this->hasMany("App\Travels", "owner_id");
    }

    public function AffiliateTravels(){
        return $this->hasMany("App\AffiliateTravels", "owner_id");
    }

    public function hasSkill($skillId){

        $hasSkill = UserSkillList::where("skill_id", "=", $skillId)
                    ->where("user_id", "=", $this->user_id)
                    ->first();

        if($hasSkill) {
            return true;
        } else {
            return false;
        }

    }

    public function settings(){
        $settings = UserSettings::where("owner_id", $this->user_id)->first();
        return $settings;
    }
}

