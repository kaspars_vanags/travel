<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Scheduler extends Model
{
    protected $table = "scheduler";

    protected $fillable = [
        "travel_id",  "date_from", "date_to", "locations"
    ];

    public function travel(){
        return $this->hasOne("App\Travels", "id");
    }
}
