<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MySkills extends Model
{

    protected $table = "my_skills";
    protected $primaryKey = "skill_id";
    protected $fillable = [
        "skill_name", "skill_description", "skill_price", "skill_limits", "skill_duration", "skill_type", "skill_image", "parent_skill", "skill_active", "stripe_id"
    ];



/*Schema::create('my_skills', function (Blueprint $table) {
    $table->increments('skill_id');
    $table->string("skill_name", 60);
    $table->text("skill_description")->nullable();
    $table->string("skill_price")->nullable();
    $table->text("skill_limits")->nullable();
    $table->integer("skill_duration")->nullable();
    $table->enum("skill_type", ["subscription", "time_limit", "single_purchase"])->default("single_purchase");
    $table->string("skill_image")->nullable();
    $table->integer("parent_skill")->nullable();
    $table->timestamps();
});*/
}
