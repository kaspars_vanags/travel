<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TravelBasket extends Model
{

    protected $primaryKey = "basket_id";
    protected $table = "travel_basket";

    protected $fillable = [
        "owner_id", "user_id", "item_name", "description", "price", "optional", "travel_id"
    ];

}
