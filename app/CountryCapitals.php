<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CountryCapitals extends Model
{
    protected $table = "country_capitals";
    protected $primaryKey = "capital_id";

    protected $fillable = [
        "country_name", "capital_name", "lat", "long", "country_code", "continent"
    ];

}
