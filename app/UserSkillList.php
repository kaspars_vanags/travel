<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSkillList extends Model
{
    protected $table = "user_skill_list";
    protected $primaryKey = "us_id";

    protected $fillable = [
        "user_id", "skill_id", "type", "time_expire", "stripe_order_id"
    ];

}
