<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TravelInvitationQueue extends Model
{
    protected $primaryKey = "invite_id";
    protected $table = "travel_invitation_queue";

    protected $fillable = [
        "owner_id", "receiver_email", "invitation_status", "travel_id", "token"
    ];

}
