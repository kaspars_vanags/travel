<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/** MAINTENANCE */

Route::get("/nope", "Controller@maintenance");

Route::get("/secretlogin", "Auth\AdminController@secretLogin");
Route::post("/secretlogin", "Auth\AdminController@adminLogin");

Route::group(["middleware" => "global"], function(){

    Route::get("/", "Controller@index");
    Route::post("/register", "Controller@register");
    Route::post("/login", "Controller@login");
    Route::get("/logout", "Controller@logout");

    Route::get("/affiliate/open/{token}", "Controller@openTravel");
    Route::get("/affiliate/join/{token}", "Controller@joinTravel");

    Route::get("/redirect/{provider}", "SocialController@redirectToProvider");
    Route::get("/auth/{provider}", "SocialController@providerAuth");

    Route::get("/privacy", "Controller@privacy");
    Route::get("/terms", "Controller@terms");


    Route::get("/problem/{code}", "Controller@errorPage");



});




Route::group(["middleware"=>["auth","global"]], function(){
    Route::get("/feed", "Auth\AuthController@index");
    Route::get("/trips", "Auth\TravelController@trips");
    Route::get("/notes", "Auth\NotesController@notes");
    Route::get("/words", "Auth\WordBankController@words");
    Route::get("/profile", "Auth\ProfileController@index");

    //Your travel rout
    Route::get("/travel/{id}", "Auth\TravelController@getTravel");

    //ADD TRAVEL NEW
    Route::get("/travel/{id}/step", "Auth\TravelController@openTravel");
    //Travel api
    Route::get("/api/travel/{id}", "Auth\TravelController@fetchTravel");
    Route::post("/api/travel/update", "Auth\TravelController@createTravel");


    //Add new travel
    Route::post("/travel", "Auth\TravelController@addTravel");
   // Route::get('/', 'Controller@index');

    //Basket controller
    Route::get("/basket/{id}", "Auth\TravelController@basket");
    Route::post("/basket/{id}/budget", "Auth\TravelController@setBudget");
    Route::post("/basket/{id}/item", "Auth\TravelController@addItem");
    Route::post("/basket/{id}/remove", "Auth\TravelController@removeItem");

    //add language
    Route::post("/language", "Auth\WordBankController@addLanguage");
    Route::post("/word", "Auth\WordBankController@addWord");


    //Map api management
    Route::post("/marker", "Auth\TravelController@addMarker");
    Route::get("/marker/{id}", "Auth\TravelController@getMarkers");
    Route::post("/marker/update", "Auth\TravelController@editMarker");
    Route::get("/marker/delete/{id}", "Auth\TravelController@deleteMarker");


    //Affiliate creation and manage
    Route::get("/create/affiliate/{id}", "Auth\TravelController@createAffiliate");

    //Travel scheduler
    Route::get("/scheduler/{id}", "Auth\SchedulerController@mainScheduler");
    Route::get("/scheduler/register/{id}", "Auth\SchedulerController@creatingPage");
    Route::post("/scheduler/register/{id}", "Auth\SchedulerController@createScheduler");
    Route::post("/scheduler/location/add", "Auth\SchedulerController@addLocation");

    //Tasks options for scheduler
    Route::post("/api/task", "Auth\SchedulerController@createTask");
    Route::post("/api/task/{id}", "Auth\SchedulerController@editTask");
    Route::get("/api/task/delete/{id}", "Auth\SchedulerController@deleteTask");


    //edit notes
    Route::post('/notes', 'Auth\NotesController@createNote');
    //delete note
    Route::post('/notes/remove', 'Auth\NotesController@removeNote');

    //Settings section
    Route::get("/profile/settings", 'Auth\SettingsController@index');
    Route::post('/profile/settings/image', 'Auth\SettingsController@updateProfileImage');
    Route::post('/user/settings', 'Auth\SettingsController@changeSettings');


    //API - TODO:move it to routes/api.php
    Route::get('/api/user', 'Api\UserController@getUser');
    Route::post('/api/user', 'Api\UserController@updateUser');


    Route::get('/api/schedule', 'Api\ScheduleController@getItems');
});


Route::group(["middleware"=>"admin"], function(){
    Route::get("/admin", "Auth\AdminController@dashboard");

    Route::get("/admin/users", "Auth\AdminController@users");
    Route::get("/admin/skills", "Auth\AdminController@skills");
    Route::get("/admin/payments", "Auth\AdminController@payments");
    Route::get("/admin/support", "Auth\AdminController@support");
    Route::get("/admin/scramble", "Auth\AdminController@scramble");

    Route::get("/admin/user/{id}", "Auth\AdminController@singleUser");


    //All post actions
    Route::post("/admin/skill", "Auth\AdminController@createSkill");
    Route::post("/admin/scramble", "Auth\AdminController@scrambleUrl");
});