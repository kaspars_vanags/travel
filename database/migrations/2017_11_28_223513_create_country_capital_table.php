<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountryCapitalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('country_capitals', function (Blueprint $table) {

            $table->increments("capital_id");
            $table->string("country_name");
            $table->string("capital_name");
            $table->string("lat", 20);
            $table->string("long", 20);
            $table->string("country_code", 3)->nullable();
            $table->string("continent");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('country_capitals');
    }
}
