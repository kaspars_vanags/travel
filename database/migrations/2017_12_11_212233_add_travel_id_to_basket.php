<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTravelIdToBasket extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('travel_basket', function (Blueprint $table) {
            $table->integer("travel_id")->unsigned();
            $table->foreign("travel_id")
                ->references("id")
                ->on("travels")
                ->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('travel_basket', function (Blueprint $table) {
            $table->dropColumn("travel_id");
        });
    }
}
