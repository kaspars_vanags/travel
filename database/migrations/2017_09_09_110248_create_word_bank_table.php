<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWordBankTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('word_bank', function (Blueprint $table) {
            $table->increments('word_id');
            $table->integer("language_id")->unsigned();
            $table->foreign("language_id")
                ->references("language_id")
                ->on("word_bank_languages")
                ->onDelete("cascade");
            $table->integer("owner_id")->unsigned();
            $table->foreign("owner_id")
                ->references("user_id")
                ->on("users")
                ->onDelete("cascade");

            $table->string("word_origin");
            $table->string("word_translated");
            $table->string("audio_file")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('word_bank');
    }
}
