<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTravelStopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('travel_stops', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("travel_id")->unsigned();
            $table->foreign("travel_id")
                ->references("id")
                ->on("travels")
                ->onDelete("cascade");
            $table->string("name");
            $table->string("adress")->nullable();
            $table->float("lat", 10, 6);
            $table->float("lng", 10, 6);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('travel_stops');
    }
}
