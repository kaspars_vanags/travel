<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScheduleTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scheduler_tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("schedule_id")->unsigned();
            $table->foreign("schedule_id")
                ->references("id")
                ->on("scheduler")
                ->onDelete("cascade");
            $table->string("title");
            $table->string("description")->nullable();
            $table->string("location")->nullable();
            $table->dateTime("scheduled_date");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scheduler_tasks');
    }
}
