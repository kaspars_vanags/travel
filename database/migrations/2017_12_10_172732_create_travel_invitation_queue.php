<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTravelInvitationQueue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('travel_invitation_queue', function (Blueprint $table) {
            $table->increments('invite_id');
            $table->integer("owner_id")->unsigned();
            $table->foreign("owner_id")
                ->references("user_id")
                ->on("users")
                ->onDelete("cascade");
            $table->string("receiver_email", 100);
            $table->enum("invitation_status", ["waiting", "sent", "accepted", "denied"])->default("waiting");
            $table->integer("travel_id")->unsigned();
            $table->foreign("travel_id")
                ->references("id")
                ->on("travels")
                ->onDelete("cascade");
            $table->string("token")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('travel_invitation_queue');
    }
}
