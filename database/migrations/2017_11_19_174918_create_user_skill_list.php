<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserSkillList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_skill_list', function (Blueprint $table) {
            $table->increments('us_id');
            $table->integer("user_id")->unsigned();
            $table->foreign("user_id")
                ->references("user_id")
                ->on("users")
                ->onDelete("cascade");

            $table->integer("skill_id")->unsigned();
            $table->foreign("skill_id")
                ->references("skill_id")
                ->on("my_skills")
                ->onDelete("cascade");

            $table->string("type")->default("single_purchase");
            $table->dateTime("time_expire")->nullable();
            $table->string("stripe_order_id")->nullable(); // NULL if we give it as a gift or something like that

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_skill_list');
    }
}
