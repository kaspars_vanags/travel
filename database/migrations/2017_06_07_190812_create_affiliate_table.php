<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAffiliateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('affiliate_travels', function (Blueprint $table) {
            $table->increments('aff_id');
            $table->integer("travel_id")->unsigned();
            $table->foreign("travel_id")
                ->references("id")
                ->on("travels")
                ->onDelete("cascade");
            $table->integer("owner_id")->unsigned();
            $table->foreign("owner_id")
                ->references("user_id")
                ->on("users")
                ->onDelete("cascade");
            $table->integer("user_id")->unsigned();
            $table->foreign("user_id")
                ->references("user_id")
                ->on("users")
                ->onDelete("cascade");
            $table->string("token");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('affiliate_travels');
    }
}
