<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMySkillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('my_skills', function (Blueprint $table) {
            $table->increments('skill_id');
            $table->string("skill_name", 60);
            $table->text("skill_description")->nullable();
            $table->string("skill_price")->nullable();
            $table->text("skill_limits")->nullable();
            $table->integer("skill_duration")->nullable();
            $table->enum("skill_type", ["subscription", "time_limit", "single_purchase"])->default("single_purchase");
            $table->string("skill_image")->nullable();
            $table->integer("parent_skill")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('my_skills');
    }
}
