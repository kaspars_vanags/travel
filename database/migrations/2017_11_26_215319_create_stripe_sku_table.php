<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStripeSkuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('my_skills_sku', function (Blueprint $table) {
            $table->increments('sku_id');
            $table->integer("skill_id")->unsigned();
            $table->foreign("skill_id")
                ->references("skill_id")
                ->on("my_skills")
                ->onDelete("cascade");

            $table->string("currency", 3)->default("gbp");
            $table->integer("price")->default(0);
            $table->string("stripe_product_id");
            $table->integer("active")->default(0);
            $table->string("image")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('my_skills_sku');
    }
}
