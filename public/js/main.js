/**
 * Created by zurka_000 on 16.08.2017..
 */


var path_base = "http://" + window.location.host;

/*var currentLocation = ;

if(currentLocation == "travel.test:8080") {
    path_base =
}*/

//open close side-navbar

$(document).ready(function(){
/*
    $("#extend-nav-sidebar").on("click", function(){

        var sidebarNav = $("#profile-nav-sidebar");
        var subSidebar = $("#info-nav-subnavbar");

        if(sidebarNav.hasClass("nav-extended")) {
           sidebarNav.animate({
               width: "50px"
           }, 500, function(){
               sidebarNav.removeClass("nav-extended");
           });

           if(subSidebar.hasClass("extended-navbar")) {
               subSidebar.removeClass("extended-navbar");
           }


        } else {
            sidebarNav.animate({
                width: "300px"
            }, 500);

            sidebarNav.addClass("nav-extended");
        }
    });
*/


    $(".extra-tabs").on("click", function(){
        var pageName = $(this).data("page");

        switch (pageName) {
            case "newtravel":
                //$("#create-travel-modal").modal("show");
                window.location.replace("/travel/0/step");
                break;
            case "profile":
                window.location.replace("/profile");
                break;
            case "trips":
                window.location.replace("/trips");
                break;
            case "logout":
                window.location.replace("/logout");
                break;
            case "login":
                window.location.replace("/");
                break;
            case "notes":
                window.location.replace("/notes");
                break;
            case "word-bank":
                window.location.replace("/words");
                break;
            case "info":
                break;
            case "admin-panel":
                window.location.replace("/admin");
                break;
            default:
                window.location.replace("/profile");
                break;
        }
    });


    var profileVm = new Vue({
        el: "#profile-info-container",
        data: {
            "profile": {
                "about_me": "",
                "name" : "",
                "email" : "",
                "profile_image" : ""
            },
            "edit" : ""
        },
        methods: {
            getUser: function () {
                var self = this;
                $.getJSON("/api/user","", function(r){
                    self.profile = r;
                });
            },
            updateProfile: function() {
                var self = this;

                $.ajax({
                    url: "/api/user",
                    type: "POST",
                    data: self.profile,
                    success: function(r) {
                        console.log(r);
                        self.getUser();
                    },
                    error: function(r) {
                        console.log(r)
                    },
                    contentData: false,
                });
            }
        },
        mounted: function(){
            this.$nextTick(function(){
                profileVm.getUser();
            })
        }
    });

/*    $("#profile-name").on("click", function(){
        var subSidebar = $("#info-nav-subnavbar");

        if(subSidebar.hasClass("extended-navbar")) {
            subSidebar.removeClass("extended-navbar");
        } else {
            subSidebar.addClass("extended-navbar");
        }
    });*/


    $("#profile-button").on("click", function(){
        $("#profile-info-container").toggleClass("extended");
    });

    $("#profile-description").hover(function(){
        //$(this).find(".edit-button").toggleClass("visible");
    }, function(){
        //$(this).find(".edit-button").toggleClass("visible");
    });


    $(".edit-button").on("click", function(){
        var self = $(this);
        var parentEl = self.parent();
        var elementId = parentEl[0].id;

        if(self.find("i").html() == "create") {
            updateProfile(elementId, false);
            self.find("i").html("save");
        } else {

            var valid = validForm();

            if(valid) {
                profileVm.updateProfile();
                updateProfile(elementId, true);
                self.find("i").html("create");
            }
        }
    });

    function updateProfile(element, mode){

        switch (element){
            case "profile-description":
                if(mode) {
                    profileVm.$data.edit = "";
                } else {
                    profileVm.$data.edit = "about_me";
                }
                break;
            default:
                alert("cits");
                break;
        }
    }

    function validForm() {

        var description = $("#about_me_form");
        var errorHolder = $("#error-messenger").find("h4");

        if(description.val().length > 250) {
            errorHolder.html(' "About me" should be 250 characters long. You have: '+description.val().length + ' characters');

            if(!$("#error-messenger").hasClass("visible")) {
                $("#error-messenger").toggleClass("visible");
            }
            return false;
        }

        if($("#error-messenger").hasClass("visible")) {
            $("#error-messenger").toggleClass("visible");
        }
        return true;
    }

    $("#error-messenger-close").on("click", function(){
        $("#error-messenger").toggleClass("visible");
    });


    $('div.alert').not('.alert-important').delay(3000).fadeOut(350);



    $(".well-travel-image").hover(function(){

        if(!$(this).hasClass("active-box")) {
            $(".well-travel-image").removeClass("active-box");
            $(this).addClass("active-box");
        }


/*        console.log(1);
        $(".well-travel-image").animate({
            backgroundColor: "#000",
            transform: "translateY(-77px)"
        }, 340, "linear");*/
    });

});
//# sourceMappingURL=main.js.map
