<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 22.11.2017.
 * Time: 22:24
 */

?>

<div class="row">
    <div class="col-xs-10 col-xs-offset-1">
        <div class="nav navbar-default" id="admin-navbar">
            <ul class="nav navbar-nav">
                <li><a href="/admin">Home</a></li>
                <li><a href="/admin/users">Users</a></li>
                <li><a href="/admin/skills">Skills</a></li>
                <li><a href="/admin/payments">Payments</a></li>
                <li><a href="/admin/support">Support</a></li>
             {{--   <li><a href="/admin/scramble">Scramble</a></li>--}}
            </ul>
        </div>
    </div>
</div>
