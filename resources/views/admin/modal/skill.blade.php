<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 27.11.2017.
 * Time: 19:31
 */

?>

<div class="modal fade" id="create-skill-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Create new skill</h4>
            </div>
            <div class="modal-body">
                <form action="/admin/skill" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="skill_name" class="control-label">Name</label>
                        <input type="text" name="skill_name" id="skill_name" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="skill_description" class="control-label">Description</label>
                        <textarea class="form-control" id="skill_description" name="skill_description" rows="5" style="resize: none"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="skill_price" class="control-label">Price (GB {{--| EUR | US--}})</label>
                        <div class="form-inline">
                            <input type="number" min="0" step="1" name="skill_price_gb" id="skill_price_gb" class="form-control" placeholder="POUNDS">
                            {{--                                <input type="number" min="0" step="1" name="skill_price_eur" id="skill_price_eur" class="form-control" placeholder="EUR">
                                                            <input type="number" min="0" step="1" name="skill_price_usd" id="skill_price_usd" class="form-control" placeholder="DOLLAR">--}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="skill_limits" class="control-label">Limits (amount)</label>
                        <input type="number" min="0" step="1" name="skill_limits" id="skill_limits" value="0" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="skill_duration" class="control-label">Duration (Days (0 = infinity))</label>
                        <input type="number" min="0" value="0" name="skill_duration" id="skill_duration" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="skill_type" class="control-label">Type</label>
                        <select id="skill_type" name="skill_type" class="form-control">
                            <option value="single_purchase">Single purchase</option>
                            <option value="subscription">Subscription</option>
                            <option value="time_limit">Time limit</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="skill_image" class="control-label">Image</label>
                        <input type="file" name="skill_image" id="skill_image" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="parent_skill" class="control-label">Parent skill ID</label>
                        <input type="number" min="0" step="1" value="0" name="parent_skill" id="parent_skill" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="skill_active" class="control-label">Active</label>
                        <select id="skill_active" name="skill_active" class="form-control">
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-custom-secondary">Create</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
