<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 23.11.2017.
 * Time: 20:53
 */

?>

@extends("admin")

@section("body")
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
            <div class="panel panel-default" style="margin-top: 10%; box-shadow: 3px 3px 15px #212121;">
                <div class="panel-heading">
                    <h4>Admin Panel [<a href="/profile" class="btn btn-xs" style="text-transform: uppercase;">Back</a>]</h4>
                </div>
                <div class="panel-body">
                    @include("admin.widgets.navbar")
                    <hr style="margin-top: 10px; margin-bottom: 10px;">
                    <div class="row">
                        <div class="col-xs-10 col-xs-offset-1">
                            <h4>Users page</h4>
                            <p>This page will contain user data, user information, extended information, user travel, etc</p>
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Country</th>
                                        <th>View</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($response["users"] as $user)
                                        <tr>
                                            <td>{{ $user["user_id"] }}</td>
                                            <td>{{ $user["name"] }}</td>
                                            <td>{{ $user["email"] }}</td>
                                            <td>{{ $user["country"] }}</td>
                                            <td><a href="/admin/user/{{$user["user_id"]}}" class="btn btn-sm btn-default">View</a></td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
