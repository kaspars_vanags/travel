<?php
/**
 * Created by PhpStorm.
 * User: Kaspars
 * Date: 2/24/2018
 * Time: 16:21
 */


?>

@extends("admin")

@section("body")
    <div class="row">
        <div class="col-sm-4 col-sm-offset-4 col-xs-10 col-xs-offset-1">
            <div class="panel panel-default" style="margin-top: 10%; box-shadow: 3px 3px 15px #212121;">
                <div class="panel-heading">
                    <h4>Admin login</h4>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-10 col-xs-offset-1">
                            <form method="post" action="/secretlogin">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label class="control-label" for="email">Email</label>
                                    <input type="email" name="email" id="email" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="password">Email</label>
                                    <input type="password" name="password" id="password" class="form-control">
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-default btn-sm">Login</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section("scripts")
@endsection