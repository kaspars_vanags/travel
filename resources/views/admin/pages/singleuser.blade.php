<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 25.11.2017.
 * Time: 13:14
 */

?>




@extends("admin")

@section("body")
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
            <div class="panel panel-default" style="margin-top: 10%; box-shadow: 3px 3px 15px #212121;">
                <div class="panel-heading">
                    <h4>Admin Panel [<a href="/profile" class="btn btn-xs" style="text-transform: uppercase;">Back</a>]</h4>
                </div>
                <div class="panel-body">
                    @include("admin.widgets.navbar")
                    <hr style="margin-top: 10px; margin-bottom: 10px;">
                    <div class="row">
                        <div class="col-xs-10 col-xs-offset-1">
                            <h4>Users {{ $response["user"]["name"] }}, page</h4>
                            <p>This page contains all users information</p>
                            <div class="row">
                                <div class="col-sm-3 hidden-xs">
                                    <img src="{{ $response["user"]["profile_image"] }}" alt="No Image" class="img-responsive img-thumbnail">
                                    <!-- IMAGE -->
                                </div>
                                <div class="col-sm-9 col-xs-12">
                                    <h4>{{ $response["user"]["name"] }}</h4>
                                    <hr>
                                    <h4>About:</h4>
                                    <p>
                                        {{ $response["user"]["about_me"] }}
                                    </p>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <h4>Travels</h4>
                                    <table class="table table-bordered table-responsive">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Description</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($response["travels"] as $travel)
                                                <tr>
                                                    <td>{{ $travel["name"] }}</td>
                                                    <td>{{ $travel["description"] }}</td>
                                                    <td>{{ $travel["status"] }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <h4>Affiliate travels</h4>
{{--                                    <table class="table table-bordered table-responsive">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Description</th>
                                            <th>Status</th>

                                            "owner_id", "name", "description", "status"
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($response["affiliates"] as $travel)
                                            <tr>
                                                <td>{{ $travel["name"] }}</td>
                                                <td>{{ $travel["description"] }}</td>
                                                <td>{{ $travel["status"] }}</td>
                                            </tr>

                                            "travel_id", "owner_id", "user_id", "token"
                                        @endforeach
                                        </tbody>
                                    </table>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection