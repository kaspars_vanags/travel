<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 09.12.2017.
 * Time: 12:44
 */

?>

@extends("admin")

@section("body")
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
            <div class="panel panel-default" style="margin-top: 10%; box-shadow: 3px 3px 15px #212121;">
                <div class="panel-heading">
                    <h4>Admin Panel [<a href="/profile" class="btn btn-xs" style="text-transform: uppercase;">Back</a>]</h4>
                </div>
                <div class="panel-body">
                    @include("admin.widgets.navbar")
                    <hr style="margin-top: 10px; margin-bottom: 10px;">
                    <div class="row">
                        <div class="col-xs-10 col-xs-offset-1">
                            <h4>Scramble table</h4>
                            <form action="/admin/scramble" method="POST">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="url" class="control-label">Url</label>
                                    <input type="text" name="url" id="url" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-default">Scramble</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

