<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 23.11.2017.
 * Time: 20:55
 */

?>

@extends("admin")

@section("body")
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
            <div class="panel panel-default" style="margin-top: 10%; box-shadow: 3px 3px 15px #212121;">
                <div class="panel-heading">
                    <h4>Admin Panel [<a href="/profile" class="btn btn-xs" style="text-transform: uppercase;">Back</a>]</h4>
                </div>
                <div class="panel-body">
                    @include("admin.widgets.navbar")
                    <hr style="margin-top: 10px; margin-bottom: 10px;">
                    <div class="row">
                        <div class="col-xs-10 col-xs-offset-1">
                            <h4>Payments page</h4>
                            <p>This page will contain all payments, from users or charge-backs, we can see payment history amount, etc</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
