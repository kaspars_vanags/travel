<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 23.11.2017.
 * Time: 20:54
 */

?>

@extends("admin")

@section("body")
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
            <div class="panel panel-default" style="margin-top: 10%; box-shadow: 3px 3px 15px #212121;">
                <div class="panel-heading">
                    <h4>Admin Panel [<a href="/profile" class="btn btn-xs" style="text-transform: uppercase;">Back</a>]</h4>
                </div>
                <div class="panel-body">
                    @include("admin.widgets.navbar")
                    <hr style="margin-top: 10px; margin-bottom: 10px;">
                    <div class="row">
                        <div class="col-xs-10 col-xs-offset-1">
                            @include("vendor.flash.message")
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-10 col-xs-offset-1">
                            <h4>Skills page</h4>
                            {{--<p>This page will manage all skills. Add skills, delete, edit, give users skills, create offers etc</p>--}}
                        </div>
                    </div>
                    <div class="row" style="height: 100px;">
                        <div class="col-xs-5 col-xs-offset-1">
                            <button class="btn btn-custom-danger" data-toggle="modal" data-target="#create-skill-modal">Create skill</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-10 col-xs-offset-1">
                            <table class="table table-responsive table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th>Price [Pounds]</th>
                                        <th>Limits</th>
                                        <th>Duration</th>
                                        <th>Type</th>
                                        <th>Image</th>
                                        <th>Parent ID</th>
                                        <th>Active</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($response["skills"] as $skill)
                                        <tr>
                                            <td>{{ $skill["skill_id"] }}</td>
                                            <td>{{ $skill["skill_name"] }}</td>
                                            <td>{{ $skill["skill_description"] }}</td>
                                            <td>{{ $skill["skill_price"] }}</td>
                                            <td>{{ $skill["skill_limits"] }}</td>
                                            <td>{{ $skill["skill_duration"] }}</td>
                                            <td>{{ $skill["skill_type"] }}</td>
                                            <td>{{ $skill["skill_image"] }}</td>
                                            <td>{{ $skill["parent_skill"] }}</td>
                                            <td>{{ $skill["skill_active"] }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- SKILL MODAL WINDOW -->
    @include("admin.modal.skill")
@endsection
