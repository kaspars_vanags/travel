
<?php
      //get page
        $currentPage = $_SERVER["REQUEST_URI"];
    ?>


<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Travel | App</title>

        @if(Auth::check())
            <link href="{{ URL::asset("css/themes/".Auth::user()->settings()->color_scheme) }}" rel="stylesheet" type="text/css">
        @else
            <link href="{{ URL::asset("css/themes/default.css") }}" rel="stylesheet" type="text/css">
        @endif

        <script src="{{ URL::asset("js/jquery-3.2.1.min.js") }}" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
        <script src="{{ URL::asset("js/jquery-ui.js") }}"></script>

        <script src="{{ URL::asset("js/bootstrap.min.js") }}" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

        <script src="{{ URL::asset("js/vue.js") }}"></script>
        {{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">--}}
        <script src="{{ URL::asset("js/animatelo.min.js") }}"></script>
        <script src="{{ URL::asset("js/classie.js") }}"></script>
        @if(Auth::check())
            <script src="{{ URL::asset("js/main.js") }}"></script>
        @endif

        <script src="{{ URL::asset("js/jquery.validate.min.js") }}"></script>
        <script src="{{ URL::asset("js/additional-methods.min.js") }}"></script>

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">


        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!-- Google login -->
        @if($_SERVER["REQUEST_URI"] == "/")
        <script src="https://apis.google.com/js/platform.js" async defer></script>
        <meta name="google-signin-client_id" content="909165081359-i8q1ckj2ogg9ep156qb0b3jgqs8s5gtd.apps.googleusercontent.com">
        @endif

        <script>
            (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-5BWGVRQ');
        </script>

        <?php
            $date = date("w");
        ?>
        <style>
            #login-container {
                background-image: url("../../img/assets/login-{{$date}}-day.jpg");
                background-color: rgba(33,150,243,0.4);
                background-blend-mode: luminosity;
            }
        </style>
    </head>
    <body>

        @include("pages.widgets.hexagons")
        <noscript>
            <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5BWGVRQ" height="0" width="0" style="display:none;visibility:hidden">
            </iframe>
        </noscript>
        @if(Auth::check())
{{--            <div class="row">
                <div class="col-xs-12 col-sm-8 col-sm-offset-2">--}}
                    @include("auth.widgets.navbar")
{{--                </div>
            </div>--}}
        @endif
        <div class="container-fluid">
            @if($currentPage != "/profile/settings")
                @include("vendor.flash.message")
            @endif
            @yield("body")
            <div class="footer row @if($currentPage == "/") login-footer @endif">
                <p class="text-center">
                    KAITO.D.H | MEDIA © 2017 &nbsp&nbsp&nbsp TRIP SAVAGE
                </p>
                <p class="text-center">
                    <a href="#" data-toggle="modal" data-target="#privacy-modal">Privacy</a> <a href="#" data-toggle="modal" data-target="#terms-and-condition-modal">Terms & Conditions</a>
                </p>
            </div>
            <div id="error-messenger" class="col-xs-8 col-xs-offset-2 col-sm-4 col-sm-offset-4">
                <p id="error-messenger-close" class="close"><i class="material-icons">clear</i></p>
                <h4 class="text-center">This will be error message!</h4>
            </div>
        </div>
        @yield("scripts")
        @include("auth.modal.terms")
        <script>
            $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
        </script>
    </body>
</html>
