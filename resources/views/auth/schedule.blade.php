<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 04.08.2017.
 * Time: 20:07
 */

        ?>

@extends("welcome")
@section("body")
    <div class="row" id="schedule-wrapper">
        <!-- NEW SCHEDULE -->
        <div class="row">
            <div id="schedule-container" class="col-sm-6 col-sm-offset-2 col-xs-12">
                <!-- Main image goes here -->
                <div id="schedule-header" class="row">
                    <div class="col-sm-8 col-xs-12 schedule-header__image" style="background-image: linear-gradient(rgba(33,33,33 ,.8), rgba(33,33,33 ,.8)), url('https://media.coindesk.com/uploads/2017/03/Japan.jpg'); ">

                    </div>
                    <div class="col-sm-4 hidden-xs schedule-header__description">

                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                <h1>hz kjas te bus</h1>
            </div>
        </div>
        <!-- DATE SLIDER -->
        <div id="schedule-dates"  class="row">
            <div class="col-sm-8 col-sm-offset-2 col-xs-12">
                <div class="scheduler-dates-slider-wrapper">
                    <div class="scheduler-dates-slider--navigation scheduler-dates-slider--navigation__left">
                        <i class="material-icons">chevron_left</i>
                    </div>
                    <div class="scheduler-dates-slider--navigation scheduler-dates-slider--navigation__right">
                        <i class="material-icons">chevron_right</i>
                    </div>

                    <div class="scheduler-dates-slider-float">
                        <!-- ALL SLIDER DATES GOES HERE -->
                    </div>
                </div>
            </div>
{{--            <div class="col-sm-2 hidden-xs">
                <h4>Schedule controler</h4>
                <div class="row">
                    <div class="col-xs-12">
                        <button class="btn btn-custom" data-toggle="modal" data-target="#schedule-table-view" style="width: 100%; border-radius: 0px;">@lang('schedule.open_btn')</button>
                    </div>
                    <div class="col-xs-12">
                        <button class="btn btn-custom" data-toggle="modal" data-target="#add-location-modal" style="width: 100%; border-radius: 0px;">@lang('schedule.add_location')</button>
                    </div>
                </div>
            </div>--}}
        </div>
        <!-- DATE ITEMS -->
        <div class="row">
            <div class="col-sm-6 col-sm-offset-2">
                <div class="schedule-item--date">
                    <button class="btn btn-custom-secondary shadow-dark" id="add-task-button" data-datefield="" style="margin-bottom: 20px; width: 250px;">Add task</button>
                    <h2>Your schedule for today</h2>
                </div>
            </div>
            <div class="col-sm-2">
            </div>
        </div>
        <div class="row">
            <!-- Schedule items goes here -->
            <div class="col-sm-6 col-sm-offset-2 col-xs-12">
                <div v-for="item in itemSelected" class="row schedule-item--container">
                    <div class="schedule-item--container__description col-sm-8 col-xs-6">
                        <h3>
                            @{{ item.title }}
                        </h3>
                        <p>
                            @{{ item.description }}
                        </p>
                    </div>
                    <div class="schedule-item--container__date col-sm-3 col-xs-4">
                        <p>
                            <i class="far fa-clock"></i> @{{ item.schedule_time }}
                        </p>
                        <p>
                            <i class="fas fa-map-marker-alt"></i>  @{{ item.location }}
                        </p>
                    </div>
                    <div class="schedule-item--container__options col-sm-1 col-xs-2">
                        <p class="text-center" @click="openEditor(item.id)">
                            <i class="fas fa-edit"></i>
                        </p>
                        <p class="text-center" @click="deleteTask(item.id)">
                            <i class="fas fa-trash"></i>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
            </div>
        </div>

        @include("auth.modal.schedultasks")
    </div>


    @include("auth.modal.scheduletable")
    @include("auth.modal.location")


@endsection

@section("scripts")

    <script src="{{ URL::asset("js/date-slider.js") }}" type="text/javascript"></script>
    <script>

        $(document).ready(function(){


            $("#add-task-button").on("click", function(){

                var date_value = $(this).data("datefield");
                $("#scheduled_date_label").html("Task for: "+date_value);
                $("#scheduled_date").val(date_value);

                $("#create-schedule-task-modal").modal("show");
            });

/*            $("#create-task-form").validate({
                rules : {
                    "title": {
                        required: true,
                        minlength: 4,
                        maxlength: 40
                    },
                    "description": {
                        maxlength: 190
                    }
                },
                messages : {
                    "title": {
                        required: "Field 'Title' is required!",
                        minlength: "Title should be at least {0} characters long.",
                        maxlength: "Max title length is 40 characters."
                    },
                    "description": {
                        maxlength: "Max description length is 190 characters."
                    }
                }
            });*/

            $("#edit-task-form").validate({
                rules : {
                    "title": {
                        required: true,
                        minlength: 4,
                        maxlength: 40
                    },
                    "description": {
                        maxlength: 190
                    }
                },
                messages : {
                    "title": {
                        required: "Field 'Title' is required!",
                        minlength: "Title should be at least {0} characters long.",
                        maxlength: "Max title length is 40 characters."
                    },
                    "description": {
                        maxlength: "Max description length is 190 characters."
                    }
                }
            });



            $("#add-location-form").validate({
                rules : {
                    "location": {
                        required: true,
                        minlength: 1,
                        maxlength: 60
                    },
                },
                messages : {
                    "location": {
                        required: "Field 'Location' is required!",
                        minlength: "Title should be at least {0} characters long.",
                        maxlength: "Max title length is 60 characters."
                    }
                }
            });



        });

        var editModal = {
            props: ["value"],
            template: `<div id="editor-root"><div class="form-group">
            <label class="control-label" for="title">@lang("modal.sch_task_title_lb")</label>
            <input type="text" v-model="value.title"  class="form-control" name="title" id="title">
            <p class="field_title error-holder"></p>
            </div>
            <div class="form-group">
            <label class="control-label" for="description">@lang("modal.sch_task_description_lb")</label>
            <input type="text" v-model="value.description" class="form-control" name="description" id="description">
             <p class="field_description error-holder"></p>
            <input type="hidden" name="task_id" v-model="value.task_id"   id="task_id" value="" hidden>
            </div>
            <div class="form-group">
            <label class="control-label" for="location">@lang("modal.sch_task_location_lb")</label>
            <input type="text" v-model="value.location" id="location" name="location" class="form-control" required="required">
            <p class="field_location error-holder"></p>
            </div>
            <hr>
            <div class="form-group">
            <button type="button" class="btn btn-custom-secondary" v-on:click="$emit('update-task', 'value')">@lang("modal.sch_task_save_btn")</button>
            </div></div>`
        };



        var scheduleVue = new Vue({
            el: "#schedule-wrapper",
            components: {
                'task-editor' : editModal
            },
            data: {
                dates: [],
                items: [],
                itemSelected: [],
                firstDate: "",
                selectedTask: {"title": ""}
            },
            methods: {
                fetchItems: function(){
                    var self = this;

                    $.getJSON("/api/schedule", {"id": {{ $response["scheduleId"] }} }).done(function(r){
                        fakeBoxes(r["dates"], r["dates"].length);
                        self.dates = r["dates"];
                        self.items = r["items"];

                        if(self.firstDate == "") {
                            self.firstDate = self.dates[0].fullDate + " 00:00:00";
                        }

                        if(typeof(self.items[self.firstDate]) != "undefined") {
                            self.itemSelected = self.items[self.firstDate];
                        }

                    }).fail(function(r){
                        console.log("error");
                    });

                    return 1;
                },
                changeDate: function(date){
                    this.itemSelected = this.items[date+" 00:00:00"];
                },
                addTask: function() {
                    let response = validateForm("create");
                    let formHolder = $("#create-task-form");

                    let formData = {
                        "schedule_id": formHolder.find("#schedule_id").val(),
                        "title" :formHolder.find("#title").val(),
                        "description" : formHolder.find("#description").val(),
                        "location": formHolder.find("#location").val(),
                        "scheduled_date" : formHolder.find("#scheduled_date").val(),
                        "schedule_time" : formHolder.find("#schedule_time").val()
                    };

                    var self = this;

                    if(response) {

                        $.ajax({
                            type: "POST",
                            url: "/api/task",
                            data: formData,
                            success: function(r) {
                                console.log(r);
                                self.fetchItems();
                            },
                            error: function (r) {
                                console.log("error");
                                console.log(r);
                            },
                            beforeSend: function(xhr) {
                                xhr.setRequestHeader("X-CSRFToken", "{{ csrf_token() }}");
                            }
                        });

                    }

                    return;
                },
                openEditor: function(id) {

                    var modalEl = $("#edit-schedule-task-modal");
                    var self = this;
                    self.selectedTask = self.itemSelected.filter(function(e) {return e.id == id;})[0];



/*
                    modalEl.find("#description").val(selectedTask["description"]);
                    modalEl.find("#task_id").val(selectedTask["id"]);
                    modalEl.find("#title").val(selectedTask["title"]);
                    modalEl.find("#location").val(selectedTask["location"]);
*/

                    modalEl.modal("show");

                    //console.log(selectedTask);

                },
                updateTask: function() {
                   // console.log(id);

                    var self = this;
                    var isValid = validateForm("edit");
                    var modalEl = $("#edit-schedule-task-modal");

                    if(!isValid) {
                        return;
                    }

                    $.ajax({
                        type: "POST",
                        url: "/api/task/"+self.selectedTask.id,
                        data: self.selectedTask,
                        success: function(r) {
                            console.log(r);

                            modalEl.modal("hide");
                            self.fetchItems();
                        },
                        error: function (r) {
                            console.log("error");
                            console.log(r);
                        },
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader("X-CSRFToken", "{{ csrf_token() }}");
                        }
                    });


                },
                deleteTask: function(id) {
                    var self = this;
                    $.get(" /api/task/delete/" + id, function(r) {
                        console.log(r);
                        self.fetchItems();
                        //TODO: add some global message
                    });
                }
            },
            mounted: function() {
                this.$nextTick(function () {
                    scheduleVue.fetchItems();
                });
            }
        });

        function changeActiveItem(date){
            scheduleVue.changeDate(date);
        }

        function validateForm(name){

            var isValid = true;

            if(name == "create" || name == "edit") {

                $(".error-holder").html("").removeClass("error");

                var formHolder = $("#"+name+"-task-form");
                var allInputs = formHolder.find("input");

                allInputs.each(function(e){

                    switch(allInputs[e].name) {
                        case "title":
                            if(allInputs[e].value.length < 4 || allInputs[e].value.length > 40 ) {
                                isValid = false;
                                $(".field_"+allInputs[e].name+".error-holder").addClass("error").html("Title is 'Required' and should be between 4 and 40 characters");
                            }
                            break;
                        case "description":
                            if(allInputs[e].value.length > 190 ) {
                                isValid = false;
                                $(".field_"+allInputs[e].name+".error-holder").addClass("error").html("Max description length is 190 characters.");
                            }
                            break;
                        case "schedule_time":
                        case "location":
                            if(allInputs[e].value.length > 100 ) {
                                isValid = false;
                                $(".field_"+allInputs[e].name+".error-holder").addClass("error").html("Location should be less than 100 characters");
                            }
                            break;
                    }
                });

                return isValid;
            }

/*
            rules : {
                "title": {
                    required: true,
                        minlength: 4,
                        maxlength: 40
                },
                "description": {
                    maxlength: 190
                }
            },
            messages : {
                "title": {
                    required: "Field 'Title' is required!",
                        minlength: "Title should be at least {0} characters long.",
                        maxlength: "Max title length is 40 characters."
                },
                "description": {
                    maxlength: "Max description length is 190 characters."
                }
            }

*/
        }



    </script>
@endsection
