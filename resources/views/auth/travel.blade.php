<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 09.12.2017.
 * Time: 17:11
 */

?>

@extends("welcome")

@section("body")
    <div class="row" id="profile-container">
        <div class="col-sm-10 col-sm-offset-1 col-xs-12">
            <div class="well" id="travel-creation-container">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1 col-xs-12">
                        <h2 class="text-center" style="text-transform: uppercase;">@lang('travel.title')</h2>
                        <hr class="custom-hr">
                        <div class="row">
                            <div id="big-loader-screen">
                                <h1 class="text-center"><i class="fa fa-spinner fa-spin" aria-hidden="true"></i></h1>
                            </div>
                            <div id="travel-form-container" class="col-sm-6 col-sm-offset-3 col-xs-12">
                                <div :class="['step-container', {complete: travel.step > 0 }]">
                                    <div class="float-step-box">
                                        <p class="float-step-number">1</p>
                                    </div>
                                    <h3>@lang('travel.main_info')</h3>
                                    <hr>
                                    <div class="travels-form">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <div :class="['error-helper', {'has-error': global.message != '' && global.error == 1, 'has-success': global.message != '' && global.error == 0}]">
                                                <p class="text-center">@{{ global.message }}</p>
                                            </div>
                                            <label class="control-label" for="travel_name">@lang('travel.travel_name_lb')</label>
                                            <input type="text" class="form-control" name="name" id="travel_name" v-model="travel.name" required="required">
                                            <div :class="['error-helper', {'has-error': error.name != ''}]">
                                                <p class="text-center">@{{ error.name }}</p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label" for="travel_description">@lang('travel.travel_description_lb') {{--(@lang('travel.optional'))--}}</label>
                                            <textarea class="form-control" name="description" id="travel_description" v-model="travel.description" rows="6" cols="5" style="resize: none"></textarea>
                                            <div :class="['error-helper', {'has-error': error.description != ''}]">
                                                <p class="text-center">@{{ error.description }}</p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="countries" class="control-label">@lang('travel.countries_lb')</label>
                                            <br><em>@lang('travel.countries_info')</em>
                                            <select v-model="travel.country_id" id="countries" name="country_id" class="form-control" required="required">
                                                @foreach($response["countries"] as $country)
                                                    <option value="{{$country["capital_id"]}}">{{ $country["country_name"] }} ({{ $country["capital_name"] }})</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <button v-on:click="updateTravel" :class="['btn btn-custom', {hidden: travel.step > 0}]" style="width: 45%;">@lang('travel.next_step')</button>
                                        </div>
                                    </div>
                                </div>
                                <div :class="['step-container', {complete: travel.step >= 1, incomplete: travel.step < 1 }]">
                                    <div class="float-step-box">
                                        <p class="float-step-number">2</p>
                                    </div>
                                    <div class="float-step-layer">
                                    </div>
                                    <h3>@lang('travel.dates_info')</h3>
                                    <hr>
                                   <div class="travels-form">
                                       <div class="form-group">
                                           <label class="control-label" for="date-from">@lang('travel.sch_date_from_lb')</label>
                                           <input type="date" name="date-from" id="date-from" class="form-control" v-model="travel.date_from" required>
                                           <div :class="['error-helper', {'has-error': error.date_from != ''}]">
                                               <p class="text-center">@{{ error.date_from }}</p>
                                           </div>
                                       </div>
                                       <div class="form-group">
                                           <label class="control-label" for="date-to">@lang('travel.sch_date_to_lb')</label>
                                           <input type="date" name="date-to" id="date-to" class="form-control"  v-model="travel.date_to" required>
                                           <div :class="['error-helper', {'has-error': error.date_to != ''}]">
                                               <p class="text-center">@{{ error.date_to }}</p>
                                           </div>
                                       </div>
                                       <div class="form-group">
                                           <button type="button" @click="updateTravel" :class="['btn btn-custom', {hidden: travel.step != 1}]" style="width: 45%;">@lang('travel.next_step')</button>
                                       </div>
{{--                                       <div class="form-group">
                                           <label class="control-label" for="location">@lang('travel.sch_color_ind_lb')</label><br>
                                           <em>@lang('travel.sch_ext_description_lb')</em><br>
                                           <div class="form-inline" style="margin-top: 10px;">
                                               <input type="text" name="location" id="location" class="form-control">
                                               <input type="color" name="location-color" id="location-color" class="form-control" style="width:75px;">
                                               <button type="button" id="add-location" class="btn btn-custom" style="border-radius: 0px; width: 125px;">@lang('travel.sch_add_btn')</button>
                                           </div>
                                       </div>
                                       <hr>
                                       <div class="form-group">
                                           <label class="control-label">@lang('travel.sch_loc_list_lb')</label>
                                           <div id="location-container">

                                           </div>
                                       </div>--}}
{{--                                       <div class="form-group">
                                           <div class="form-inline">
                                               <a href="/travel/0/step" class="btn btn-custom" style="width: 30%;">@lang('travel.previous_step')</a>
                                               <button type="submit" class="btn btn-custom" style="margin-left: 10px; width: 45%;">@lang('travel.next_step')</button>
                                           </div>
                                       </div>--}}
                                   </div>
                                </div>
                                <div :class="['step-container', {complete: travel.step >= 2, incomplete: travel.step < 2 }]">
                                    <div class="float-step-box">
                                        <p class="float-step-number">3</p>
                                    </div>
                                    <div class="float-step-layer">
                                    </div>
                                    <h3>@lang('travel.invite_info')</h3>
                                    <hr>
                                    <div class="travels-form">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <label for="invites" class="control-label">@lang('travel.invites_lb') {{--(@lang('travel.optional'))--}}</label>
                                            <br><em>@lang('travel.invites_info')</em>
                                            <!-- TODO: replace with loop -->
                                            <div class="form-group">
                                                <input type="email" name="invite[]" v-model="travel.invite[0]" class="form-control emails-input-1" placeholder="@lang('travel.invites_pl')">
                                            </div>
                                            <div :class="['error-helper', {'has-error': error.emails_1 != ''}]">
                                                <p class="text-center">@{{ error.emails_1 }}</p>
                                            </div>

                                            <div class="form-group">
                                                <input type="email" name="invite[]" v-model="travel.invite[1]" class="form-control emails-input-2" placeholder="@lang('travel.invites_pl')">
                                            </div>
                                            <div :class="['error-helper', {'has-error': error.emails_2 != ''}]">
                                                <p class="text-center">@{{ error.emails_2 }}</p>
                                            </div>

                                            <div class="form-group">
                                                <input type="email" name="invite[]" v-model="travel.invite[2]" class="form-control emails-input-3" placeholder="@lang('travel.invites_pl')">
                                            </div>
                                            <div :class="['error-helper', {'has-error': error.emails_3 != ''}]">
                                                <p class="text-center">@{{ error.emails_3 }}</p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <button type="button" @click="updateTravel" :class="['btn btn-custom', {hidden: travel.step != 2}]" style="width: 45%;">@lang('travel.next_step')</button>
                                        </div>
{{--                                        <div class="form-group">
                                            <div class="form-inline">
                                                <a href="/travel/0/step" class="btn btn-custom" style="width: 30%;">@lang('travel.previous_step')</a>
                                                <button type="submit" class="btn btn-custom" style="margin-left: 10px; width:45%;">@lang('travel.finish')</button>
                                            </div>
                                        </div>--}}
                                    </div>
                                </div>
                                <div :class="['step-container', {complete: travel.step >= 3, incomplete: travel.step < 3 }]">
                                    <div class="float-step-box">
                                        <p class="float-step-number"><i class="material-icons">&#xE877;</i></p>
                                    </div>
                                    <div class="float-step-layer">
                                    </div>
                                    <h3>@lang('travel.congratulations')</h3>
                                    <hr>
                                    <div class="travels-form">
                                        <p>
                                            @lang('travel.congrats_text')
                                        </p>
                                        <div class="form-group">
                                            <button type="button" @click="updateTravel" :class="['btn btn-custom', {hidden: travel.step < 3}]" style="width: 45%;">@lang('travel.finish')</button>
                                        </div>
{{--                                        <div class="form-group">
                                            <a href="/profile" class="btn btn-custom" style="width: 30%;">@lang('travel.exit')</a>
                                        </div>--}}
                                    </div>
                                </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
    </div>
   @endsection

   @section("scripts")

       <script>
           $(document).ready(function(){

               function hideIncomplete(){
                   $(".step-container.incomplete").each(function(e,i) {
                       var mainElement = i;
                       var overFlow = null;

                       for(var j=0; j < mainElement.childNodes.length; j++) {
                           if(mainElement.childNodes[j].className == "float-step-layer") {
                               overFlow = mainElement.childNodes[j];
                               break;
                           }
                       }
                       overFlow.style.height = i.scrollHeight+"px";
                   });
               }

               function bigLoaderOpen(){
                   var maxHeight = document.getElementById("travel-form-container").scrollHeight;//+20;
                   var bigLoader = $("#big-loader-screen");

                   bigLoader.css("height", maxHeight);
                   bigLoader.css("display", "block");
               }

               function bigLoaderClose(){
                   var bigLoader = $("#big-loader-screen");
                   bigLoader.css("display", "none");
               }

               var vm = new Vue({
                   el : "#travel-creation-container",
                   data : {
                       travel: {
                           "id": {{ $response["id"] }},
                           "name":"",
                           "description": "",
                           "status" : "",
                           "country_id" : 10,
                           "date_from" : "",
                           "date_to" : "",
                           "step":0,
                           "invite" : ["", "", ""]
                       },
                       error: {
                           "name":"",
                           "description": "",
                           "date_from" : "",
                           "date_to" : "",
                           "emails_1" : "",
                           "emails_2" : "",
                           "emails_3" : ""
                       },
                       global: {
                           "message": "",
                           "error": 0,
                       }
                   },
                   methods: {
                       fetchTravel: function(){
                           var self = this;
                           var requestId = self.travel.id;

                           $.getJSON(path_base + "/api/travel/" + requestId).done(function(r){
                               console.log(r);
                               console.log(r.invites.length);
                               if(r.success) {
                                   self.travel.id = r.payload.id;
                                   self.travel.name = r.payload.name;
                                   self.travel.description = r.payload.description;
                                   self.travel.status = r.payload.status;
                                   self.travel.country_id = r.payload.country_id;
                                   self.travel.step = r.payload.step;

                                   if(r.payload.date_from != null) {
                                       self.travel.date_from = r.payload.date_from.replace(" 00:00:00", "");
                                   }
                                   if(r.payload.date_to != null) {
                                       console.log("shit")
                                       self.travel.date_to = r.payload.date_to.replace(" 23:59:59", "");
                                   }

                                   for(var i=0; i<r.invites.length; i++) {
                                        self.travel.invite[i] = r.invites[i].receiver_email;
                                   }
                               }
                               bigLoaderClose();
                           });
                       },
                       updateTravel: function(){

                           var self = this;
                           var isValid = this.formValidation();

                           self.resetGlobal();

                           if(isValid) {
                               var updateParams = self.travel;
                               bigLoaderOpen();

                               $.ajaxSetup({
                                   headers: {
                                       'X-CSRF-TOKEN': '{{ csrf_token() }}'
                                   }
                               });

                               $.ajax({
                                   "url" : path_base + "/api/travel/update",
                                   "method" : "POST",
                                   "data" : updateParams,
                                   "asynchronous": true
                               }).done(function(r) {
                                   console.log(r);
                                   bigLoaderClose()

                                   if(r.success) {
                                       self.global.error = 0;
                                       self.global.message = "Travel details has been updated!";
                                       self.travel.id = r.payload.id;
                                       self.fetchTravel();
                                   } else {
                                       self.global.error = 1;
                                       self.global.message = r.message;
                                   }
                               });

                           }

                       },
                       formValidation: function(){
                            var returnValue = true;

                           $.each(this.error, function(i, e) {
                               if(e != "") {
                                   console.log(i);
                                   returnValue = false;
                               }
                           });

                           return returnValue;
                       },
                       resetGlobal: function(){
                           this.global.message =  "";
                           this.global.error = 0;
                       }
                   },
                   beforeCreate: function(){
                       bigLoaderOpen();
                   },
                   created: function() {
                       console.log("Vue is created");
                       this.fetchTravel();
                   },
                   mounted: function() {
                       hideIncomplete();
                   }
               });

               //MANA VALIDĀCIJA
               $("#travel_name").on("change", function(){
                   var elLength = $(this).val().length;
                   if(elLength > 40) {
                       vm.$data.error.name = "Travel name is too long!";
                   } else {
                       vm.$data.error.name = "";
                   }
               });

               $("#travel_description").on("change", function(){
                   var elLength = $(this).val().length;
                   if(elLength > 1400) {
                       vm.$data.error.description = "Travel description is too long!";
                   } else {
                       vm.$data.error.description = "";
                   }
               });

               var currentDate = new Date();
               currentDate.setHours(0,0,0,0);

               $("#date-from").on("change", function(){
                   vm.$data.error.date_from = "";

                   var selectedDate = new Date($(this).val());
                   if (selectedDate < currentDate) {
                       //alert("");
                       vm.$data.error.date_from = "You can't select date in past!";
                       $(this).val("");
                   }
               });

               $("#date-to").on("change", function(){
                   vm.$data.error.date_to = "";

                   var selectedDate = new Date($(this).val());
                   if (selectedDate < currentDate) {
                       vm.$data.error.date_to = "You can't select date in past!";
                       $(this).val("");
                   }

                   var dateFrom = new Date($("#date-from").val());
                   console.log(dateFrom);
                   if(selectedDate < dateFrom || dateFrom == "" || dateFrom == "Invalid Date") {
                       vm.$data.error.date_to = "Date to should be after or the same date as from!";
                       $(this).val("");
                   }
               });

                // email regex
               //
               var regExpresion = /^([A-Z|a-z|0-9](\.|_){0,1})+[A-Z|a-z|0-9]\@([A-Z|a-z|0-9])+((\.){0,1}[A-Z|a-z|0-9]){2}\.[a-z]{2,5}$/gm;

               //TODO: Put under one system

               $(".emails-input-1").on("change", function(){
                   var valToCheck = $(this).val();

                   if(valToCheck.match(regExpresion) != null || valToCheck == "") {
                        vm.$data.error.emails_1 = "";
                   } else {
                       vm.$data.error.emails_1 = "Email address is incorrect!";
                   }

                   if(valToCheck != "" && valToCheck.length > 100) {
                       vm.$data.error.emails_1 = "Email address is too long!";
                   }
               });

               $(".emails-input-2").on("change", function(){
                   var valToCheck = $(this).val();

                   if(valToCheck.match(regExpresion) != null || valToCheck == "") {
                       vm.$data.error.emails_2 = "";
                   } else {
                       vm.$data.error.emails_2 = "Email address is incorrect!";
                   }

                   if(valToCheck != "" && valToCheck.length > 100) {
                       vm.$data.error.emails_2 = "Email address is too long!";
                   }
               });

               $(".emails-input-3").on("change", function(){
                   var valToCheck = $(this).val();

                   if(valToCheck.match(regExpresion) != null || valToCheck == "") {
                       vm.$data.error.emails_3 = "";
                   } else {
                       vm.$data.error.emails_3 = "Email address is incorrect!";
                   }

                   if(valToCheck != "" && valToCheck.length > 100) {
                       vm.$data.error.emails_2 = "Email address is too long!";
                   }
               });



/*               $("#travel-form-container").validate({
                   rules : {
                       name: {
                           required: true,
                           minlength: 4,
                           maxlength: 40
                       },
                       currency: {
                           maxlength: 15
                       },
                   },
                   messages : {
                       name: {
                           required: "This field is required",
                           minlength: "You mast enter atleast {0} characters",
                           maxlength: "You have exceeded max characters",
                       },
                       currency: {
                           maxlength: "You have exceeded max characters",
                       }
                   }
               });*/

           });
       </script>
   {{--
       @if($response["step"] == 3)
           <script>
               $(document).ready(function(){

                   $("#travels-form").validate({
                       rules : {
                           "invite[]": {
                               required: false,
                               email: true,
                               maxlength: 80
                           },
                       },
                       messages : {
                           "invite[]": {
                               email: "Please enter a valid email address!",
                               maxlength: "You have exceeded max characters",
                           }
                       }
                   });

               });
           </script>
       @endif



       @if($response["step"] == 2)
           <script>
               var i={{ $response["locations"] }},j=0;
               var locationContainer = $("#location-container");

               $("#add-location").on("click", function(){
                   var location = $("#location").val();
                   var color = $("#location-color").val();

                   if(location == ""){
                       alert("Please enter location");
                       return;
                   }

                   var newElement = "<div class='form-group' id='element_"+i+"'>" +
                       "<h3>"+location+" : "+color+"</h3>" +
                       "<input type='hidden' name='location[]' value='"+location+"'>" +
                       "<input type='hidden' name='location_color[]' value='"+color+"'>" +
                       "<button type='button' data-element='"+i+"' class='btn btn-danger remove-element' style='width: 125px; border-radius: 0px;'>@lang('travel.sch_remove_btn')</button>" +
                       "</div>";
                   locationContainer.append(newElement);
                   i++;
               });


               $(".remove-element").on("click", function(e){
                   var elementId = $(this).data("element");
                   $("#element_"+elementId).remove();
               })

               $(document).ready(function(){





               });
           </script>
       @endif--}}

@endsection
