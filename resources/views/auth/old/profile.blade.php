<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 20.05.2017.
 * Time: 17:33
 */

        ?>


@extends("welcome")
@section("body")
    <div class="row" id="profile-container">
        @include("auth.widgets.sidebar")
        <div class="col-sm-10 col-sm-offset-1 col-xs-12">
            <div class="row">
                <div class="col-xs-12 col-sm-4">
                    <div class="well">
                        <div class="info-box">
                            <div class="info-box-header red-box"><p class="text-center"><i class="material-icons">&#xE410;</i></p></div>{{--<i class="material-icons">&#xE413;</i>--}}
                            <div class="info-box-body">
                                <p class="text-right">@lang('profile.total_trips')</p>
                                <h2 class="text-right">{{ $response["travels_total"] }}</h2>
                            </div>
                            <div class="info-box-footer">
                                <p class="text-left">@lang('profile.total_trips_description')</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="well">
                        <div class="info-box">
                            <div class="info-box-header yellow-box"><p class="text-center"><i class="material-icons">&#xE413;</i></p></div>
                            <div class="info-box-body">
                                <p class="text-right">@lang('profile.total_affiliate_trips')</p>
                                <h2 class="text-right">{{ $response["affiliate_total"] }}</h2>
                            </div>
                            <div class="info-box-footer">
                                <p class="text-left">@lang('profile.total_affiliate_trips_description')</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="well">
                        <div class="info-box">
                            <div class="info-box-header grey-box"><p class="text-center"><i class="material-icons">&#xE8E2;</i></p></div>
                            <div class="info-box-body">
                                <p class="text-right">@lang('profile.saved_words')</p>
                                <h2 class="text-right">{{ $response["words_saved"] }}</h2>
                            </div>
                            <div class="info-box-footer">
                                <p class="text-left">@lang('profile.saved_words_description')</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <h2 class="header-text">@lang('profile.last_travels')</h2>
            <div class="row">
                @foreach($response["travels"] as $tr)
                    <div class="col-xs-12 col-sm-4">
                        <div class="well">
                            <div class="well-travel-body">
                                <div class="well-travel-image">
                                    <h4 class="text-center">Try Again!</h4>
                                </div>
                                <div class="well-travel-info">
                                    <h4 class="text-center">{{ $tr["name"] }}</h4>
                                    <p class="text-center">
                                        @if(strlen($tr["description"]) > 100)
                                            {{ substr($tr["description"], 0, 100)."..." }}
                                        @else
                                            {{ substr($tr["description"], 0, 100) }}
                                        @endif
                                    </p>
                                </div>
                            </div>
                            <div class="well-travel-footer">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <p class="text-left"><i class="material-icons">&#xE227;</i> @if(is_null($tr["budget"])) --.-- @else {{ $tr["budget"] }} @endif</p>
                                    </div>
                                    <div class="col-xs-8">
                                        <p class="text-right"><i class="material-icons">&#xE0C8;</i> {{ $tr["country_name"] }}, {{ $tr["capital_name"] }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            {{--<div class="well">
                <div class="row">
                    <div class="col-sm-4 col-xs-12">
                        <h3 class="text-center">@lang('profile.total_trips')</h3>
                        <div class="total-count-box">
                            <p class="text-center">{{ $response["travels_total"] }}</p>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <h3 class="text-center">@lang('profile.total_affiliate_trips')</h3>
                        <div class="total-count-box">
                            <p class="text-center">{{ $response["affiliate_total"] }}</p>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <h3 class="text-center">@lang('profile.saved_words')</h3>
                        <div class="total-count-box">
                            <p class="text-center">{{ $response["words_saved"] }}</p>
                        </div>
                    </div>
                </div>
                <hr class="custom-hr">
                <div class="row">
--}}{{--                    <h4>Things to put</h4>
                    <ul>
                        <li>Next trip</li>
                        <li>Quick note button</li>
                        <li>Suggested travels</li>
                        <li>etc</li>
                    </ul>--}}{{--
                </div>
            </div>--}}
        </div>
    </div>

    <div class="modal fade" id="open-link-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3>Travel link</h3>
                </div>
                <div class="modal-body">
                    <h3 id="travel-id-modal" style="word-wrap: break-word;"></h3>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("scripts")
    <script>

        function openLink(link) {
            $("#travel-id-modal").html("http://travel.niknais.com/affiliate/open/"+link);
            $("#open-link-modal").modal("show");
        }

    </script>
@endsection
