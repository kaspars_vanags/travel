<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 20.08.2017.
 * Time: 18:20
 */

        ?>

@extends("welcome")
@section("body")
    <div class="row" id="profile-container">
        <div class="col-sm-10 col-sm-offset-1 col-xs-12">
            <div class="row">
                <div class="col-sm-offset-1 col-sm-4 col-xs-12 sidebar-title-box">
                    <h3 class="text-center">Your trips</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 col-xs-12">
                    <div class="row" style="margin-top: 20px">
                    @foreach($response["travels"] as $tr)
                        <div class="col-sm-3 col-xs-12 user-trips">
                            <div class="well well-travel">
                                <div class="well-travel-body">
                                    <div class="well-travel-image"  data-travelid="{{ $tr["id"] }}" data-travelstep="{{ $tr["step"] }}">
                                        <h4 class="text-center">{{ $tr["name"] }}</h4>
                                        <p class="text-center">
                                            @if(strlen($tr["description"]) > 100)
                                                {{ substr($tr["description"], 0, 100)."..." }}
                                            @else
                                                {{ substr($tr["description"], 0, 100) }}
                                            @endif
                                        </p>
                                    </div>
                                    <div class="well-travel-options">
                                        <div class="trip-box-widget" data-travelid="{{ $tr["id"] }}" data-travelop="edit">
                                            <p class="text-center" data-toggle="tooltip" data-placement="bottom" title="Edit trip"><i class="material-icons">create</i></p>
                                        </div>
                                        @if($tr["step"] == 3)
                                            <div class="trip-box-widget" data-travelid="{{ $tr["id"] }}" data-travelop="schedule">
                                                <p class="text-center" data-toggle="tooltip" data-placement="bottom" title="Open schedule"><i class="material-icons">event_note</i></p>
                                            </div>
                                            <div class="trip-box-widget" data-travelid="{{ $tr["id"] }}" data-travelop="basket">
                                                <p class="text-center" data-toggle="tooltip" data-placement="bottom" title="Edit basket"><i class="material-icons">local_atm</i></p>
                                            </div>
                                        <div class="trip-box-widget" data-travelid="{{ $tr["id"] }}" data-affurl="{{ $tr["token"] }}" data-travelop="share">
                                                <p class="text-center" data-toggle="tooltip" data-placement="bottom" title="Share trip"><i class="material-icons">share</i></p>
                                            </div>
                                        @endif
                                    </div>
{{--                                    <div class="well-travel-info">
                                        <h4 class="text-center">{{ $tr["name"] }}</h4>
                                        <p class="text-center">
                                            @if(strlen($tr["description"]) > 100)
                                                {{ substr($tr["description"], 0, 100)."..." }}
                                            @else
                                                {{ substr($tr["description"], 0, 100) }}
                                            @endif
                                        </p>
                                    </div>--}}
                                </div>
                                <div class="well-travel-footer">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <p class="text-left"><i class="material-icons">&#xE227;</i> @if(is_null($tr["budget"])) --.-- @else {{ $tr["budget"] }} @endif</p>
                                        </div>
                                        <div class="col-xs-8">
                                            <p class="text-right"><i class="material-icons">&#xE0C8;</i> {{ $tr["country_code"] }}, {{ $tr["capital_name"] }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 col-sm-offset-1 col-xs-12 sidebar-title-box">
                    <h3 class="text-center">Affiliate trips</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 col-xs-12">
                    <div class="row" style="margin-top: 20px">
                        @foreach($response["affiliates"] as $tr)
                            @if($tr["owner_id"] != Auth::user()->user_id)
                            <div class="col-sm-3 col-xs-12">
                                <div class="well well-travel">
                                    <div class="well-travel-body">
                                        <div class="well-travel-image"  data-travelid="{{ $tr["id"] }}" data-travelstep="{{ $tr["step"] }}">
                                            <h4 class="text-center">{{ $tr["name"] }}</h4>
                                            <p class="text-center">
                                                @if(strlen($tr["description"]) > 100)
                                                    {{ substr($tr["description"], 0, 100)."..." }}
                                                @else
                                                    {{ substr($tr["description"], 0, 100) }}
                                                @endif
                                            </p>
                                        </div>
                                        <div class="well-travel-options">
                                            <div class="trip-box-widget" data-travelid="{{ $tr["id"] }}" data-travelop="edit">
                                                <p class="text-center" data-toggle="tooltip" data-placement="bottom" title="Edit trip"><i class="material-icons">create</i></p>
                                            </div>
                                            @if($tr["step"] == 3)
                                                <div class="trip-box-widget" data-travelid="{{ $tr["id"] }}" data-travelop="schedule">
                                                    <p class="text-center" data-toggle="tooltip" data-placement="bottom" title="Open schedule"><i class="material-icons">event_note</i></p>
                                                </div>
                                                <div class="trip-box-widget" data-travelid="{{ $tr["id"] }}" data-travelop="basket">
                                                    <p class="text-center" data-toggle="tooltip" data-placement="bottom" title="Edit basket"><i class="material-icons">local_atm</i></p>
                                                </div>
                                                <div class="trip-box-widget" data-travelid="{{ $tr["id"] }}" data-travelop="share">
                                                    <p class="text-center" data-toggle="tooltip" data-placement="bottom" title="Share trip"><i class="material-icons">share</i></p>
                                                </div>
                                            @endif
                                        </div>
                                        {{--                                    <div class="well-travel-info">
                                                                                <h4 class="text-center">{{ $tr["name"] }}</h4>
                                                                                <p class="text-center">
                                                                                    @if(strlen($tr["description"]) > 100)
                                                                                        {{ substr($tr["description"], 0, 100)."..." }}
                                                                                    @else
                                                                                        {{ substr($tr["description"], 0, 100) }}
                                                                                    @endif
                                                                                </p>
                                                                            </div>--}}
                                    </div>
                                    <div class="well-travel-footer">
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <p class="text-left"><i class="material-icons">&#xE227;</i> @if(is_null($tr["budget"])) --.-- @else {{ $tr["budget"] }} @endif</p>
                                            </div>
                                            <div class="col-xs-8">
                                                <p class="text-right"><i class="material-icons">&#xE0C8;</i> {{ $tr["country_code"] }}, {{ $tr["capital_name"] }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>



           {{-- <div class="well">
                <div class="row">
                    <div class="col-sm-10 col-xs-12">

                        <div class="hidden-xs">
                            <h3 style="text-transform: uppercase;">@lang('trips.title')</h3>
                            <hr class="custom-hr">
                            <table class="table table-bordered table-hover table-custom">
                                <thead>
                                <tr>
                                    <th>@lang('trips.name_tb')</th>
                                    <th>@lang('trips.description_tb')</th>
                                    <th>@lang('trips.action_tb')</th>
                                    <th>@lang('trips.travel_p_tb')</th>
                                    <th>@lang('trips.share_tb')</th>
                                    <th>@lang('trips.basket_tb')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($response["travels"] as $travel)
                                    <tr>
                                        <td>{{ $travel["name"] }}</td>
                                        <td>{{ $travel["description"] }}</td>
                                        <td>
                                            @if($travel["step"] == 3)
                                                <a href="/travel/{{ $travel["id"] }}" class="btn btn-custom-secondary" style="width: 100%;">@lang('trips.open_btn')</a>
                                            @else
                                                <a href="/travel/{{ $travel["id"] }}/step" class="btn btn-custom-secondary" style="width: 100%;">@lang('trips.edit_btn')</a>
                                            @endif
                                        </td>
                                        <td>
                                            @if($travel["step"] == 3)
                                            <a href="/scheduler/{{ $travel["id"] }}" class="btn btn-custom-secondary" style="width: 100%;">@lang('trips.scheduler_btn')</a>
                                            @else
                                                <h4 class="text-center">
                                                @lang('trips.incomplete')
                                                </h4>
                                            @endif
                                        </td>
                                        <td>
                                            @if($travel["step"] == 3)
                                            <a href="/create/affiliate/{{ $travel["id"] }}" class="btn btn-custom" style="width: 100%;">@lang('trips.share_btn')</a>
                                            @else
                                                <h4 class="text-center">
                                                    @lang('trips.incomplete')
                                                </h4>
                                            @endif
                                        </td>
                                        <td>
                                            @if($travel["step"] == 3)
                                                <a href="/basket/{{ $travel["id"] }}" class="btn btn-custom-secondary" style="width: 100%;">@lang('trips.basket_btn')</a>
                                            @else
                                                <h4 class="text-center">
                                                    @lang('trips.incomplete')
                                                </h4>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <hr>
                            <h3 style="text-transform: uppercase;">@lang('trips.title_2')</h3>
                            <hr class="custom-hr">
                            <table class="table table-bordered table-hover table-custom">
                                <thead>
                                <tr>
                                    <th>@lang('trips.owner_tb')</th>
                                    <th>@lang('trips.travel_tb')</th>
                                    <th>@lang('trips.action_tb')</th>
                                    <th>@lang('trips.scheduler_tb')</th>
                                    <th>@lang('trips.url_tb')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($response["affiliates"] as $aff)
                                    @if($aff["owner_id"] != Auth::user()->user_id)
                                    <tr>
                                        <td>{{ $aff["owner_name"] }}</td>
                                        <td>{{ $aff["travel_name"] }}</td>
                                        <td>
                                            <a href="/travel/{{ $aff["travel_id"] }}" class="btn btn-custom-secondary" style="width: 100%;">@lang('trips.open_btn')</a>
                                        </td>
                                        <td>
                                            <a href="/scheduler/{{ $aff["travel_id"]  }}" class="btn btn-custom-secondary" style="width: 100%;">@lang('trips.scheduler_btn')</a>
                                        </td>
                                        <td>
                                            <button onclick="openLink('{{ $aff["token"] }}')" class="btn btn-custom" style=" width: 100%;">@lang('trips.link_btn')</button>
                                        </td>
                                    </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="visible-xs">
                            <h3 style="text-transform: uppercase;" class="text-center">@lang('trips.title')</h3>
                            <hr class="custom-hr">
                            @foreach($response["travels"] as $travel)
                                <div class="well" style="box-shadow: 1px 2px 9px #90A4AE;">
                                    <h4 class="text-center">{{ $travel["name"] }}</h4>
                                    <hr>
                                    <p class="text-center">{{ $travel["description"] }}</p>
                                    <hr>
                                    @if($travel["step"] == 3)
                                        <a href="/travel/{{ $travel["id"] }}" class="btn btn-custom-secondary" style="width: 100%;">@lang('trips.open_btn')</a>
                                    @else
                                        <a href="/travel/{{ $travel["id"] }}/step" class="btn btn-custom-secondary" style="width: 100%;">@lang('trips.edit_btn')</a>
                                    @endif
                                    <a href="/scheduler/{{ $travel["id"] }}" class="btn btn-success" style="border-radius: 0px; width: 30%; font-size: 0.8em;">@lang('trips.scheduler_btn')</a>
                                    <a href="/create/affiliate/{{ $travel["id"] }}" class="btn btn-primary" style="border-radius: 0px; width: 30%; font-size: 0.8em;">@lang('trips.share_btn')</a>
                                </div>
                            @endforeach
                            <h3 style="text-transform: uppercase;" class="text-center">@lang('trips.title_2')</h3>
                            <hr class="custom-hr">
                            @foreach($response["affiliates"] as $aff)
                                @if($aff["owner_id"] != Auth::user()->user_id)
                                <div class="well" style="box-shadow: 1px 2px 9px #90A4AE;">
                                    <h4 class="text-center">{{ $aff["travel_name"] }}</h4>
                                    <p class="text-center">@lang('trips.owner'): {{ $aff["owner_name"] }}</p>
                                    <hr>
                                    <a href="/travel/{{ $aff["travel_id"] }}" class="btn btn-success" style="border-radius: 0px; width: 30%; font-size: 0.8em;">@lang('trips.open_btn')</a>
                                    <a href="/scheduler/{{ $aff["travel_id"]  }}" class="btn btn-success" style="border-radius: 0px; width: 30%; font-size: 0.8em;">@lang('trips.scheduler_btn')</a>
                                    <button onclick="openLink('{{ $aff["token"] }}')" class="btn btn-primary" style="border-radius: 0px; width: 30%; font-size: 0.8em;">@lang('trips.link_btn')</button>
                                </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>--}}
        </div>
    </div>

    <div class="modal fade" id="open-link-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="text-center">@lang('trips.travel_link')</h4>
                </div>
                <div class="modal-body">
                    <h3 id="travel-id-modal" style="word-wrap: break-word;"></h3>
                </div>
            </div>
        </div>
    </div>

@endsection

@section("scripts")
    <script>

        function openLink(link) {
            $("#travel-id-modal").html("http://travel.niknais.com/affiliate/open/"+link);
            $("#open-link-modal").modal("show");
        }


        $(".trip-box-widget").on("click", function(){

            var self = $(this);
            var url;
            var travelId = self.data("travelid");
            var travelOp = self.data("travelop");

            switch (travelOp) {
                case "edit":
                    url = "/travel/"+travelId+"/step";
                    break;
                case "schedule":
                    url = "/scheduler/"+travelId;
                    break;
                case "basket":
                    url = "/basket/"+travelId;
                    break;
                case "share":
                     url = "/create/affiliate/"+travelId;
                    break;
                default:
                    url = "/trips";
                    break;
            }

            if(travelOp == "share") { 
                var hasUrl = $(this).data("affurl");
                
                if(hasUrl != null && hasUrl != "null" && hasUrl != "") {
                    alert("tripsavage.com/affiliate/join/"+hasUrl);
                } else {
                    window.location.href = url;
                }

            } else {
                window.location.href = url;
            }
        });

        $(".well-travel-image").on("click", function(){

            var self = $(this);
            var travelId = self.data("travelid");
            var travelStep = self.data("travelstep");

            if(travelStep != 3) {
                window.location.href = "/travel/"+travelId+"/step";
            } else {
                window.location.href = "/travel/"+travelId;
            }
        });

    </script>
@endsection
