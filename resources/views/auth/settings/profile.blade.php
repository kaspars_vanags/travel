<?php
/**
 * Created by PhpStorm.
 * User: Kaspars
 * Date: 1/11/2018
 * Time: 18:27
 */

?>

@extends("welcome")
@section("body")
    <div class="row" id="profile-container">
        @include("auth.widgets.sidebar")
        <div class="col-sm-10 col-sm-offset-1 col-xs-12">
            <div class="well">
                <div class="row">
                    <div class="col-sm-4 col-xs-12">
                        <h1 class="text-center">Profile settings</h1>
                    </div>
                </div>
                <hr class="custom-hr">
                <div class="row">
                    <div class="col-xs-12 col-sm-4">
                        <div class="row static-alert">
                            @include("vendor.flash.message")
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <form action="/profile/settings/image" method="post" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <label for="profile_image_upload" class="control-label">@lang('modal.profile_image_lb')</label>
                                        <input type="file" class="form-control" name="profile_image" id="profile_image_upload">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-custom-secondary">@lang('modal.profile_upload_btn')</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                                <form action="/user/settings" method="post">
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <label for="color_scheme" class="control-label">@lang('modal.profile_color_scheme_lb')</label>
                                        <select class="form-control" id="color_scheme" name="color_scheme">
                                            <option value="default.css" @if(Auth::user()->settings()->color_scheme == "default.css") selected="selected" @endif>Default</option>
                                            <option value="light.css" @if(Auth::user()->settings()->color_scheme == "light.css") selected="selected" @endif>Light</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-custom-secondary">@lang('modal.profile_save_btn')</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-5 col-sm-offset-1">
                        <p>This section will be filled with instructions and guidelines?</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("scripts")
@endsection

