<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 28.08.2017.
 * Time: 14:11
 */


    $color_list = array(
            1=>["shadow"=>"#ECEFF1", "background"=>"#FFF", "text"=>"#212121"],
            2=>["shadow"=>"#ffcdd2", "background"=>"#ff5252", "text"=>"#fff"],
            3=>["shadow"=>"#FFF9C4", "background"=>"#FFD54F", "text"=>"#212121"],
            4=>["shadow"=>"#DCEDC8", "background"=>"#AED581", "text"=>"#fff"],
            5=>["shadow"=>"#B3E5FC", "background"=>"#4FC3F7", "text"=>"#212121"],
            6=>["shadow"=>"#BBDEFB", "background"=>"#64B5F6", "text"=>"#fff"],
            7=>["shadow"=>"#CFD8DC", "background"=>"#EEEEEE", "text"=>"#212121"]
    );

?>

@extends("welcome")
@section("body")
    <div class="row" id="profile-container">
        <div class="col-sm-10 col-sm-offset-1 col-xs-12">
            <div class="well">
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="text-center">@lang('notes.title')</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-offset-4 col-sm-4 col-xs-12">
                        <button id="open-new-note" class="btn btn-custom center-block" data-toggle="modal" data-target="#create-note-modal">@lang('notes.add_note_btn')</button>
                    </div>
                </div>
                <hr class="custom-hr">
<!--                <div class="row">
                    <?php
                        $stupidCounter = 0;
                    ?>

                    @foreach($response['notes'] as $note)
                        <?php $stupidCounter++; ?>
                        <div class="col-sm-4 col-xs-12">
                            <div class="well notes" style="box-shadow: 4px 4px 18px 0px {{ $color_list[$note['notes_color']]['shadow'] }}; background-color: {{ $color_list[$note['notes_color']]['background'] }}; color: {{ $color_list[$note['notes_color']]['text'] }};">
                                <div class="option-button-holder">
                                    <i class="material-icons delete-option" data-noteid="{{$note['notes_id']}}" data-title="{{$note['title']}}" data-toggle="tooltip" data-placement="top" title="Delete">&#xE14C;</i>
                                    <i class="material-icons edit-option" data-noteid="{{$note['notes_id']}}" data-title="{{$note['title']}}" data-text="{{$note["body"]}}" data-color="{{$note["notes_color"]}}" data-toggle="tooltip" data-placement="top" title="Edit">&#xE150;</i>
                                </div>
                                @if(!is_null($note['title']))
                                    <h3 class="text-center">{{ $note['title'] }}</h3>
                                    <hr style="border-top: 1px solid {{ $color_list[$note['notes_color']]['text'] }};">
                                @endif
                                @if(!is_null($note['body']))
                                    <p class="text-center">
                                        {{ $note['body'] }}
                                    </p>
                                @endif
                            </div>
                        </div>
                        <?php
                            if($stupidCounter == 3) {

                                echo "</div>";
                                echo "<div class='row'>";
                                $stupidCounter = 0;
                            }
                        ?>
                    @endforeach
                </div>
         -->


                <!-- TODO: Calculate needed height -->
                <div class="row" style="display: flex;  flex-direction: column; flex-wrap: wrap; height: 860px; padding: 2.5em; ">
                    @foreach($response['notes'] as $note)
                        <div style="width: 24%; margin: .5em;">
                            <div class="well notes" style="box-shadow: 4px 4px 18px 0px {{ $color_list[$note['notes_color']]['shadow'] }}; background-color: {{ $color_list[$note['notes_color']]['background'] }}; color: {{ $color_list[$note['notes_color']]['text'] }};">
                                <div class="option-button-holder">
                                    <i class="material-icons delete-option" data-noteid="{{$note['notes_id']}}" data-title="{{$note['title']}}" data-toggle="tooltip" data-placement="top" title="Delete">&#xE14C;</i>
                                    <i class="material-icons edit-option" data-noteid="{{$note['notes_id']}}" data-title="{{$note['title']}}" data-text="{{$note["body"]}}" data-color="{{$note["notes_color"]}}" data-toggle="tooltip" data-placement="top" title="Edit">&#xE150;</i>
                                </div>
                                @if(!is_null($note['title']))
                                    <h3 class="text-center">{{ $note['title'] }}</h3>
                                    <hr style="border-top: 1px solid {{ $color_list[$note['notes_color']]['text'] }};">
                                @endif
                                @if(!is_null($note['body']))
                                    <p class="text-center">
                                        {{ $note['body'] }}
                                    </p>
                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    @include('auth.modal.note')
@endsection


@section("scripts")
    <script>
        $(document).ready(function(){

            $("#open-new-note").on("click", function(){
                resetForm("notes-form");
            });


            $("#notes-form").validate({
                rules : {
                    title: {
                        required: true,
                        minlength: 4,
                        maxlength: 40
                    },
                    body: {
                        maxlength: 1400
                    }
                },
                messages: {
                    required: "This field is required!",
                    minlength: "Your title must be at least {0} characters long.",
                    maxlength: "Your title is too long"
                }
            });



            $(".delete-option").on("click", function(){
                var noteId, noteTitle;

                noteId = $(this).data("noteid");
                noteTitle = $(this).data("title");

                $("#hidden_note_id").val(noteId);
                $("#remove-item-holder").html(noteTitle);

                $("#delete-note-modal").modal("show");
            });

            $(".edit-option").on("click", function(){
                var noteId, noteTitle, noteText, noteColor;

                resetForm("notes-form");

                noteId = $(this).data("noteid");
                noteTitle = $(this).data("title");
                noteText = $(this).data("text");
                noteColor = $(this).data("color");

                $("#notes_id").val(noteId);
                $("#note_title").val(noteTitle);
                $("#note_body").val(noteText);

                document.querySelector("input[name=notes_color][value='"+noteColor+"']").checked = true;
                $("#create-note-modal").modal("show");
                //$('.notes-color-options [name="notes_color"]').find("[value='"+noteColor+"']").attr("checked", "checked");

            });

            function resetForm(name){
                $("#"+name)[0].reset();
            }

        });
    </script>
@endsection
