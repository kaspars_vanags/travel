<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 19.08.2017.
 * Time: 17:52
 */

        $currentPage = $_SERVER["REQUEST_URI"];


        ?>

<div id="profile-nav-sidebar" class="hidden-xs">
{{--    <div class="sidebar-hidden-background">
    </div>--}}
    <div class="row">
        <h4 class="text-center" id="extend-nav-sidebar">
            <i class="material-icons">&#xE5D3;</i>
        </h4>
    </div>
    @if(Auth::check())
    <div class="row extra-tabs" id="info-nav-sidebar" data-page="info">

        <div class="non-extended-text">
            <h4 class="text-center">
                @if(is_null(Auth::user()->profile_image))
                    <div class="center-block" id="profile-image" style="background-image: url('/img/users/default.jpg');">
                    </div>
                @else
                    <div class="center-block" id="profile-image" style="background-image: url('/img/users/{{Auth::user()->user_id}}/{{Auth::user()->profile_image}}');">
                    </div>
                @endif
                {{--<i class="material-icons">&#xE145;</i>--}}
            </h4>
        </div>
        <div class="extended-text">
            <div class="col-xs-3" style="margin-top: 11px; padding-left: 0px;">
                @if(is_null(Auth::user()->profile_image))
                    <div class="center-block" id="profile-image" style="background-image: url('/img/users/default.jpg');">
                    </div>
                @else
                    <div class="center-block" id="profile-image" style="background-image: url('/img/users/{{Auth::user()->user_id}}/{{Auth::user()->profile_image}}');">
                    </div>
                @endif

            </div>
            <div class="col-xs-9" style="margin-top: 11px;">
                <h4 class="text-left" id="profile-name">
                    {{ Auth::user()->name }} <i class="material-icons">&#xE313;</i>{{--<i class="fa fa-chevron-down" aria-hidden="true"></i>--}}
                    {{-- <i class="material-icons">&#xE145;</i> @lang('sidebar.create_new')--}}
                </h4>
                <ul id="info-nav-subnavbar" >
                    <li><a href="/profile/settings"><i class="material-icons">&#xE315;</i> Profile settings</a></li>
                </ul>
            </div>
        </div>
        {{--
        <div class="extended-text">
            <div id="settings-box-small" data-toggle="modal" data-target="#user-info-box-modal">
                <i class="material-icons">&#xE8B8;</i>
            </div>
            @if(is_null(Auth::user()->profile_image))
                <div style="width: 200px;">
                    <div class="center-block" id="profile-image" style="background-image: url('/img/users/default.jpg');">
                    </div>
                </div>
            @else
                <div style="width: 200px;">
                    <div class="center-block" id="profile-image" style="background-image: url('/img/users/{{Auth::user()->user_id}}/{{Auth::user()->profile_image}}');">
                    </div>
                </div>
            @endif
            <h4 class="text-center">{{ Auth::user()->name }}</h4>
        </div>
        --}}
    </div>
    <div class="row extra-tabs @if(strpos($currentPage,"travel") !== false) active @endif" data-page="newtravel">
        <div class="non-extended-text">
            <h4 class="text-center">
                <i class="material-icons">&#xE145;</i>
            </h4>
        </div>
        <div class="extended-text">
            <h4 class="text-left">
                <i class="material-icons">&#xE145;</i> @lang('sidebar.create_new')
            </h4>
        </div>
    </div>
    <div class="row extra-tabs @if($currentPage == "/profile") active @endif" data-page="profile">
        <div class="non-extended-text">
            <h4 class="text-center">
                <i class="material-icons">&#xE7FD;</i>
            </h4>
        </div>
        <div class="extended-text">
            <h4 class="text-left">
                <i class="material-icons">&#xE7FD;</i> @lang('sidebar.profile')
            </h4>
        </div>
    </div>
    <div class="row extra-tabs @if($currentPage == "/trips") active @endif" data-page="trips">
        <div class="non-extended-text">
            <h4 class="text-center">
                <i class="material-icons">&#xE55B;</i>
            </h4>
        </div>
        <div class="extended-text">
            <h4 class="text-left">
                <i class="material-icons">&#xE55B;</i> @lang('sidebar.your_trips')
            </h4>
        </div>
    </div>
    <div class="row extra-tabs @if($currentPage == "/notes") active @endif" data-page="notes">
        <div class="non-extended-text">
            <h4 class="text-center">
                <i class="material-icons">&#xE14F;</i>
            </h4>
        </div>
        <div class="extended-text">
            <h4 class="text-left">
                <i class="material-icons">&#xE14F;</i> @lang('sidebar.notes')
            </h4>
        </div>
    </div>
    <div class="row extra-tabs @if($currentPage == "/words") active @endif" data-page="word-bank">
        <div class="non-extended-text">
            <h4 class="text-center">
                <i class="material-icons">&#xE8E2;</i>
            </h4>
        </div>
        <div class="extended-text">
            <h4 class="text-left ">
                <i class="material-icons">&#xE8E2;</i> @lang('sidebar.word_bank')
            </h4>
        </div>
    </div>
{{--    <div class="row extra-tabs" data-page="my-skills">
        <div class="non-extended-text">
            <h4 class="text-center">
                <i class="material-icons">&#xE859;</i>
            </h4>
        </div>
        <div class="extended-text">
            <h4 class="text-left ">
                <i class="material-icons">&#xE859;</i> @lang('sidebar.my_skills')
            </h4>
        </div>
    </div>--}}
    @if(Auth::user()->admin_level > 49)
    <div class="row extra-tabs" data-page="admin-panel">
        <div class="non-extended-text">
            <h4 class="text-center">
                <i class="material-icons">&#xE88D;</i>
            </h4>
        </div>
        <div class="extended-text">
            <h4 class="text-left ">
                <i class="material-icons">&#xE88D;</i> @lang('sidebar.admin')
            </h4>
        </div>
    </div>
    @endif
    <div class="row extra-tabs" data-page="logout">
        <div class="non-extended-text">
            <h4 class="text-center">
                <i class="material-icons">&#xE8AC;</i>
            </h4>
        </div>
        <div class="extended-text">
            <h4 class="text-left">
                <i class="material-icons">&#xE8AC;</i> @lang('sidebar.logout')
            </h4>
        </div>
    </div>
    @else
        <div class="row extra-tabs" data-page="logout">
            <div class="non-extended-text">
                <h4 class="text-center">
                    <i class="material-icons">&#xE154;</i>
                </h4>
            </div>
            <div class="extended-text">
                <h4 class="text-left">
                    <i class="material-icons">&#xE154;</i> @lang('sidebar.login')
                </h4>
            </div>
        </div>
    @endif
</div>


@if(Auth::check())
    @include("auth.modal.travel")
    @include("auth.modal.info")
@endif