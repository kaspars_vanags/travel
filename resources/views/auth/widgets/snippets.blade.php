<?php
/**
 * Created by PhpStorm.
 * User: Kaspars
 * Date: 2/25/2018
 * Time: 13:35
 */

?>

<!-- TRAVEL LOOK -->

@foreach($response["travels"] as $tr)

@endforeach

{{--                    <div class="col-xs-12 col-sm-4">
                        <div class="well">
                            <div class="well-travel-body">
                                <div class="well-travel-image">
                                    <h4 class="text-center">Try Again!</h4>
                                </div>
                                <div class="well-travel-info">
                                    <h4 class="text-center">{{ $tr["name"] }}</h4>
                                    <p class="text-center">
                                        @if(strlen($tr["description"]) > 100)
                                            {{ substr($tr["description"], 0, 100)."..." }}
                                        @else
                                            {{ substr($tr["description"], 0, 100) }}
                                        @endif
                                    </p>
                                </div>
                            </div>
                            <div class="well-travel-footer">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <p class="text-left"><i class="material-icons">&#xE227;</i> @if(is_null($tr["budget"])) --.-- @else {{ $tr["budget"] }} @endif</p>
                                    </div>
                                    <div class="col-xs-8">
                                        <p class="text-right"><i class="material-icons">&#xE0C8;</i> {{ $tr["country_name"] }}, {{ $tr["capital_name"] }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>--}}

<!-- TRAVEL LOOK END -->


<!-- SIDEBAR INFO BOXES -->

    <div class="col-xs-12 col-sm-4">
        <div class="well">
            <div class="info-box">
                <div class="info-box-header red-box"><p class="text-center"><i class="material-icons">&#xE410;</i></p></div><i class="material-icons">&#xE413;</i>
                <div class="info-box-body">
                    <p class="text-right">@lang('profile.total_trips')</p>
                    <h2 class="text-right">{{ $response["travels_total"] }}</h2>
                </div>
                <div class="info-box-footer">
                    <p class="text-left">@lang('profile.total_trips_description')</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-4">
        <div class="well">
            <div class="info-box">
                <div class="info-box-header yellow-box"><p class="text-center"><i class="material-icons">&#xE413;</i></p></div>
                <div class="info-box-body">
                    <p class="text-right">@lang('profile.total_affiliate_trips')</p>
                    <h2 class="text-right">{{ $response["affiliate_total"] }}</h2>
                </div>
                <div class="info-box-footer">
                    <p class="text-left">@lang('profile.total_affiliate_trips_description')</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-4">
        <div class="well">
            <div class="info-box">
                <div class="info-box-header grey-box"><p class="text-center"><i class="material-icons">&#xE8E2;</i></p></div>
                <div class="info-box-body">
                    <p class="text-right">@lang('profile.saved_words')</p>
                    <h2 class="text-right">{{ $response["words_saved"] }}</h2>
                </div>
                <div class="info-box-footer">
                    <p class="text-left">@lang('profile.saved_words_description')</p>
                </div>
            </div>
        </div>
    </div>

<!-- SIDEBAR INFO BOXES END -->