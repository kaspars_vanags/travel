<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 02.10.2017.
 * Time: 20:25
 */

        ?>
<!--
<div class="visible-xs">
    <button data-toggle="collapse" data-target="#travel-list" class="btn btn-default" style="border-radius: 0px; width: 100%; text-align: center; height: 65px;">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </button>
    <div id="travel-list" class="collapse col-xs-12" style="background-color: #37474F; ">
        <div class="row" style="padding: 20px 5px 5px 5px;">
            <div class="col-xs-12">
                <a href="/travel/0/step" class="btn btn-success" style="width: 100%;">
                    <h4 class="text-left">
                        <i class="material-icons">&#xE145;</i> @lang('sidebar.create_new')
                    </h4>
                </a>
            </div>
        </div>
        <div class="row" style="padding-left: 5px; padding-right: 5px;">
            <div class="col-xs-12">
                <a href="/profile" class="btn btn-success" style="width: 100%;">
                    <h4 class="text-left">
                        <i class="material-icons">&#xE7FD;</i> @lang('sidebar.profile')
                    </h4>
                </a>
            </div>
        </div>
        <div class="row" style="padding: 5px;">
            <div class="col-xs-12">
                <a href="/trips" class="btn btn-success"  style="width: 100%;">
                    <h4 class="text-left">
                        <i class="material-icons">&#xE55B;</i> @lang('sidebar.your_trips')
                    </h4>
                </a>
            </div>
        </div>
        <div class="row" style="padding-left: 5px; padding-right: 5px;">
            <div class="col-xs-12">
                <a href="/notes" class="btn btn-success" style="width: 100%;">
                    <h4 class="text-left">
                        <i class="material-icons">&#xE14F;</i> @lang('sidebar.notes')
                    </h4>
                </a>
            </div>
        </div>
        <div class="row" style="padding: 5px;">
            <div class="col-xs-12">
                <a href="/words" class="btn btn-success" style="width: 100%;">
                    <h4 class="text-left ">
                        <i class="material-icons">&#xE8E2;</i> @lang('sidebar.word_bank')
                    </h4>
                </a>
            </div>
        </div>
        <div class="row" style="padding: 0px 5px 20px 5px;">
            <div class="col-xs-12">
                <a href="/logout" class="btn btn-success" style="width: 100%;">
                    <h4 class="text-left">
                        <i class="material-icons">&#xE8AC;</i> @lang('sidebar.logout')
                    </h4>
                </a>
            </div>
        </div>
    </div>
</div>
-->


<nav class="navbar navbar-default {{--navbar-fixed-top--}}">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="main-nav">
            <ul class="nav navbar-nav">
                <li><a href="/travel/0/step" @if(strpos($currentPage,"travel") !== false) class="active" @endif> <i class="material-icons">&#xE145;</i> @lang('sidebar.create_new') </a></li>
                <li><a href="/feed" @if(strpos($currentPage,"feed") !== false) class="active" @endif> {{--<i class="material-icons">&#xE55B;</i>--}} @lang('sidebar.home') </a></li>
                <li><a href="/trips" @if(strpos($currentPage,"trips") !== false) class="active" @endif> {{--<i class="material-icons">&#xE55B;</i>--}} @lang('sidebar.your_trips') </a></li>
                <li><a href="/notes" @if(strpos($currentPage,"notes") !== false) class="active" @endif>{{-- <i class="material-icons">&#xE14F;</i>--}} @lang('sidebar.notes') </a></li>
                <li><a href="/words" @if(strpos($currentPage,"words") !== false) class="active" @endif>{{-- <i class="material-icons">&#xE8E2;</i>--}} @lang('sidebar.word_bank') </a></li>
                <li><a href="/profile" @if(strpos($currentPage,"profile") !== false) class="active" @endif>{{-- <i class="material-icons">&#xE8E2;</i>--}} @lang('sidebar.profile') </a></li>
                @if(Auth::user()->admin_level > 49)
                    <li><a href="admin" @if(strpos($currentPage,"words") !== false) class="active" @endif> {{--<i class="material-icons">&#xE88D;</i>--}} @lang('sidebar.admin') </a></li>
                @endif
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#" id="profile-button" {{--@if(strpos($currentPage,"profile") !== false) class="active" @endif--}}> <i class="material-icons">settings</i> @lang('sidebar.settings') </a></li>
                <li><a href="/logout"  class="active"> <i class="material-icons">&#xE8AC;</i> @lang('sidebar.logout') </a></li>
            </ul>
        </div>
    </div>
</nav>


<div id="profile-info-container">
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2">
            @if(is_null(Auth::user()->profile_image))
                <div class="center-block" id="profile-image" style="background-image: url('/img/users/default.jpg');">
                </div>
            @else
                <div class="center-block" id="profile-image" style="background-image: url('/img/users/{{Auth::user()->user_id}}/{{Auth::user()->profile_image}}');">
                </div>
            @endif
            <h3 class="text-center">@{{ profile.name }}</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-sm-offset-3">
            <hr class="profile-hr">
            <div class="center-block" id="profile-description">
                <h4 class="text-center">About me</h4>
                <div class="edit-button visible"><i class="material-icons">create</i></div>
                <p class="text-center" :class="['text-center', {'hidden': edit == 'about_me'}]">@{{ profile.about_me }}</p>
                <textarea rows="3" cols="3" :class="['form-control', {'visible': edit == 'about_me'}]" v-model="profile.about_me" id="about_me_form" ></textarea>
            </div>
            <hr class="profile-hr">
        </div>
    </div>
</div>
