<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 18.05.2017.
 * Time: 20:33
 */
        ?>

@extends("welcome")

@section("body")

    <div class="row" id="marker-container">
        <div class="visible-xs">
            <button data-toggle="collapse" data-target="#travel-list" class="btn btn-default" style="border-radius: 0px; width: 100%; text-align: center">
                <i class="fa fa-bars" aria-hidden="true"></i>
            </button>
            <div id="travel-list" class="collapse col-xs-12" style="background-color: #37474F; ">
                <h4 style="color: #fff;">@lang('map.title')</h4>
                <a href="/profile" class="btn btn-primary" style="border-radius: 0px; width: 100%;">@lang('map.go_back_btn')</a>
                <hr>
                <div v-for="ls in list" class="row">
                    <div class="col-xs-12">
                        <div v-if="ls['status'] == 'yes'" class="panel panel-success" style="border-radius: 0px;">
                            <div class="panel-heading" style="padding-top: 5px; padding-bottom: 5px;">
                                <p class="text-center">@{{ ls["name"] }}</p>
                            </div>
                            <div class="panel-body">
                                <p style="font-weight: 600;">@{{ ls["info"] }}</p>
                            </div>
                            <div class="panel-footer">
                                <button @click="editMarker(ls['id'], ls['name'], ls['info'],ls['img_link'], ls['status'], ls['type'])" class="btn btn-default" style="width: 25%; border-radius: 0px;">@lang('map.edit_btn')</button>
                                <button @click="deleteMarker(ls['id'])" class="btn btn-danger" style="width: 25%; border-radius: 0px;">@lang('map.delete_btn')</button>
                            </div>
                        </div>
                        <div v-else-if="ls['status'] == 'maybe'" class="panel panel-primary" style="border-radius: 0px;">
                            <div class="panel-heading" style="padding-top: 5px; padding-bottom: 5px;">
                                <p class="text-center">@{{ ls["name"] }}</p>
                            </div>
                            <div class="panel-body">
                                <p style="font-weight: 600;">@{{ ls["info"] }}</p>
                            </div>
                            <div class="panel-footer">
                                <button @click="editMarker(ls['id'], ls['name'], ls['info'],ls['img_link'], ls['status'], ls['type'])" class="btn btn-default" style="width: 25%; border-radius: 0px;">@lang('map.edit_btn')</button>
                                <button @click="deleteMarker(ls['id'])" class="btn btn-danger" style="width: 25%; border-radius: 0px;">@lang('map.delete_btn')</button>
                            </div>
                        </div>
                        <div v-else class="panel panel-danger" style="border-radius: 0px;">
                            <div class="panel-heading" style="padding-top: 5px; padding-bottom: 5px;">
                                <p class="text-center">@{{ ls["name"] }}</p>
                            </div>
                            <div class="panel-body">
                                <p style="font-weight: 600;">@{{ ls["info"] }}</p>
                            </div>
                            <div class="panel-footer">
                                <button @click="editMarker(ls['id'], ls['name'], ls['info'],ls['img_link'], ls['status'], ls['type'])" class="btn btn-default" style="width: 25%; border-radius: 0px;">@lang('map.edit_btn')</button>
                                <button @click="deleteMarker(ls['id'])" class="btn btn-danger" style="width: 25%; border-radius: 0px;">@lang('map.delete_btn')</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-3 hidden-xs"  style="background-color: #37474F; height: 100%; box-shadow: inset -4px 3px 17px -3px rgba(0,0,0,0.75); padding-top: 20px; overflow: scroll;">
            {{--<button class="btn btn-default" style="width: 100%; margin-top: 20px;" onclick="addFakeMarker()">Add markers</button>--}}
            <h4 style="color: #fff;">@lang('map.title')</h4>
            <a href="/profile" class="btn btn-primary"  style="border-radius: 0px; width: 100%;">@lang('map.go_back_btn')</a>
            <hr>
            <div v-for="ls in list" class="row">
                <div class="col-xs-12">
                    <div v-if="ls['status'] == 'yes'" class="panel panel-success" style="border-radius: 0px;">
                        <div class="panel-heading" style="padding-top: 5px; padding-bottom: 5px;">
                            <p class="text-center">@{{ ls["name"] }}</p>
                        </div>
                        <div class="panel-body">
                            <p style="font-weight: 600;">@{{ ls["info"] }}</p>
                        </div>
                        <div class="panel-footer">
                            <button @click="editMarker(ls['id'], ls['name'], ls['info'],ls['img_link'], ls['status'], ls['type'])" class="btn btn-default" style="width: 25%; border-radius: 0px;">@lang('map.edit_btn')</button>
                            <button @click="deleteMarker(ls['id'])" class="btn btn-danger" style="width: 25%; border-radius: 0px;">@lang('map.delete_btn')</button>
                        </div>
                    </div>
                    <div v-else-if="ls['status'] == 'maybe'" class="panel panel-primary" style="border-radius: 0px;">
                        <div class="panel-heading" style="padding-top: 5px; padding-bottom: 5px;">
                            <p class="text-center">@{{ ls["name"] }}</p>
                        </div>
                        <div class="panel-body">
                            <p style="font-weight: 600;">@{{ ls["info"] }}</p>
                        </div>
                        <div class="panel-footer">
                            <button @click="editMarker(ls['id'], ls['name'], ls['info'],ls['img_link'], ls['status'], ls['type'])" class="btn btn-default" style="width: 25%; border-radius: 0px;">@lang('map.edit_btn')</button>
                            <button @click="deleteMarker(ls['id'])" class="btn btn-danger" style="width: 25%; border-radius: 0px;">@lang('map.delete_btn')</button>
                        </div>
                    </div>
                    <div v-else class="panel panel-danger" style="border-radius: 0px;">
                        <div class="panel-heading" style="padding-top: 5px; padding-bottom: 5px;">
                            <p class="text-center">@{{ ls["name"] }}</p>
                        </div>
                        <div class="panel-body">
                            <p style="font-weight: 600;">@{{ ls["info"] }}</p>
                        </div>
                        <div class="panel-footer">
                            <button @click="editMarker(ls['id'], ls['name'], ls['info'],ls['img_link'], ls['status'], ls['type'])" class="btn btn-default" style="width: 25%; border-radius: 0px;">@lang('map.edit_btn')</button>
                            <button @click="deleteMarker(ls['id'])" class="btn btn-danger" style="width: 25%; border-radius: 0px;">@lang('map.delete_btn')</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-9 col-xs-12" style="height: 100%; padding-left: 0px;padding-right: 0px;">
            <div id="map"></div>
        </div>

        <div class="modal fade" id="marker-edit-modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4>@lang('map.title_2')</h4>
                    </div>
                    <div class="modal-body">
                        <form id="marker-edit-form" action="/marker/update" method="POST">
                            {{ csrf_field() }}
                            <input type="text" name="id" id="marker_id" hidden>
                            <div class="form-group">
                                <label class="control-label" for="name">@lang('map.name_lb')</label>
                                <input type="text" name="name" id="name" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="info">@lang('map.info_text_lb')</label>
                                <input type="text" name="info" id="info" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="img_link">@lang('map.img_link_lb')</label>
                                <input type="text" name="img_link" id="img_link" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="status">@lang('map.will_visit_lb')</label>
                                <select class="form-control" name="status" id="status">
                                    <option value="yes" id="select-yes">@lang('map.yes_op')</option>
                                    <option value="maybe" id="select-maybe">@lang('map.maybe_op')</option>
                                    <option value="no" id="select-no">@lang('map.no_op')</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="type">@lang('map.type_lb')</label>
                                <select class="form-control" name="type" id="type">
                                    <option value="object">@lang('map.object_op')</option>
                                    <option value="restaurant">@lang('map.restaurant_op')</option>
                                    <option value="hotel" id="select-no">@lang('map.hotel_op')</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-default">@lang('map.save_btn')</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section("scripts")

    <script>
        var defaultMarker = {
            info: '<strong>Default is Tokyo</strong><br>',
            lat: 35.652832,
            long: 139.839478
        };

        //VUe stuff

        var mapVue = new Vue({
            el: "#marker-container",
            data: {
                list: []
            },
            methods: {
                fetchMarkers:function() {
                    var self = this;

                    $.ajax({
                        url: "/marker/{{ $response["travel_id"] }}",
                        method:"GET",
                        success: function(response){
                            self.list = response;

                          //  console.log(self.markers);
                            addListMarkers(self.list);
                        },
                        error: function(response){

                        }
                    });
                },
                newMarker: function(lat, long){
                    var self = this;
                    //http://maps.googleapis.com/maps/api/geocode/outputFormat?parameters
                    var info = "neatradu";
                    $.ajax({
                        url: "https://maps.googleapis.com/maps/api/geocode/json?latlng="+lat+","+long+"&sensor=false&components=locality&key={{ env("GOOGLE_API") }}",
                        method: "GET",
                        success: function(response) {
                            var cords = response["results"];

                            for(var j = 0; j<cords.length; j++){
                                if(j == cords.length-2){
                                    console.log(cords[j]["formatted_address"]);
                                    info = cords[j]["address_components"][0]["long_name"];
                                }
                            }
                            self.saveMarker(lat, long, info);
                        },
                        error: function(response){
                            console.log(response);
                        }
                    });

                    console.log(info);
                },
                saveMarker: function(lat, long, info) {
                    var self = this;

                    var dataSend = {
                        "_token": "{{ csrf_token() }}",
                        "lat":lat,
                        "long":long,
                        "info":info,
                        "travel_id": {{ $response["travel_id"] }}
                    };

                    $.ajax({
                        url:"/marker",
                        method: "POST",
                        data: dataSend,
                        async:false,
                        success: function(response){
                            console.log(response);
                            self.fetchMarkers();
                        },
                        error: function(response){
                            console.log(response);
                        }
                    });
                },
                editMarker: function(id, name, info, image, status, type){

                    document.getElementById("marker-edit-form").reset();

                    var markerForm = $("#marker-edit-form");

                    markerForm.find("#marker_id").val(id);
                    markerForm.find("#name").val(name);
                    markerForm.find("#info").val(info);
                    markerForm.find("#img_link").val(image);
                    markerForm.find("#status").val(status);
                    markerForm.find("#type").val(type);

                    $("#marker-edit-modal").modal("show");

                },
                deleteMarker: function (id) {
                    var self = this;

                    $.ajax({
                        url:"/marker/delete/"+id,
                        method: "GET",
                        async:false,
                        success: function(response){
                            console.log(response);
                            self.fetchMarkers();
                        },
                        error: function(response){
                            console.log(response);
                        }
                    });
                }
            },
            mounted: function(){
                this.$nextTick(function () {
                    this.fetchMarkers();
                })
            }
        });

        var locationList = mapVue.markers;

        function addFakeMarker(){
            addListMarkers(locationList);
        }

        // In the following example, markers appear when the user clicks on the map.
        // The markers are stored in an array.
        // The user can then click an option to hide, show or delete the markers.
        var map, infowindow, image;
        var markers = [];

        var icon_path = "/img/";
        var icons;


        function initMap() {

            var styledMap = new google.maps.StyledMapType(
                    [
                        {
                            "stylers": [
                                {
                                    "saturation": -100
                                }
                            ]
                        },
                        {
                            featureType: 'poi',
                            elementType: 'geometry',
                            stylers: [{color: '#dfd2ae'}]
                        },
                        {
                            featureType: 'poi',
                            elementType: 'labels.text.fill',
                            stylers: [{color: '#93817c'}]
                        },
                        {
                            featureType: 'poi.park',
                            elementType: 'geometry.fill',
                            stylers: [{color: '#a5b076'}]
                        },
                        {
                            featureType: 'poi.park',
                            elementType: 'labels.text.fill',
                            stylers: [{color: '#447530'}]
                        },
                        {
                            "featureType": "water",
                            "elementType": "geometry.fill",
                            "stylers": [
                                {
                                    "color": "#0099dd"
                                }
                            ]
                        },
                        {
                            "elementType": "labels",
                            "stylers": [
                                {
                                    "visibility": "ON"
                                }
                            ]
                        },
                        {
                            "featureType": "poi.park",
                            "elementType": "geometry.fill",
                            "stylers": [
                                {
                                    "color": "#aadd55"
                                }
                            ]
                        },
                        {
                            "featureType": "road.highway",
                            "elementType": "labels",
                            "stylers": [
                                {
                                    "visibility": "on"
                                }
                            ]
                        },
                        {
                            "featureType": "road.arterial",
                            "elementType": "labels.text",
                            "stylers": [
                                {
                                    "visibility": "on"
                                }
                            ]
                        },
                        {
                            "featureType": "road.local",
                            "elementType": "labels.text",
                            "stylers": [
                                {
                                    "visibility": "on"
                                }
                            ]
                        },
                        {}
                    ]
            );

            map = new google.maps.Map(document.getElementById('map'), {
                zoom: 8,
                <?php
                    if(Auth::user()->user_id == 3) {
                        echo "center: new google.maps.LatLng(40.767802, -73.996181),";
                    } else {
                        echo "center: new google.maps.LatLng(35.652832, 139.839478),";
                    }
                ?>
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            map.mapTypes.set('styled_map', styledMap);
            map.setMapTypeId('styled_map');

            infowindow = new google.maps.InfoWindow({});

            var marker, i;

            map.addListener('dblclick', function(event) {
                addMarker(event.latLng);
            });
            var image = {

            };

            icons = {
                object: {
                    icon: icon_path + 'object_2.png'
                },
                restaurant: {
                    icon: icon_path + 'food_2.png'
                },
                hotel: {
                    icon: icon_path + 'hotel_2.png'
                }
            };

        }



/*

*/


        function addListMarkers(locations) {
            for (i = 0; i < locations.length; i++) {

                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i]["lat"], locations[i]["lng"]),
                    icon: icons[locations[i]["type"]].icon,
                    map: map
                });

                var text ="<strong>"+locations[i]["name"]+"</strong><br>"+locations[i]["info"]+"<br>";

                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        if(locations[i]["status"] == "yes") {
                            infowindow.setContent("<h4 style='color: limegreen;'>"+locations[i]["name"]+"</h4><br>"+locations[i]["info"]+"<br><img src='"+locations[i]["img_link"]+"' width='450'>");
                        } else if(locations[i]["status"] == "maybe"){
                            infowindow.setContent("<h4 style='color: dodgerblue;'>"+locations[i]["name"]+"</h4><br>"+locations[i]["info"]+"<br><img src='"+locations[i]["img_link"]+"' width='450'>");
                        } else {
                            infowindow.setContent("<h4 style='color: red;'>"+locations[i]["name"]+"</h4><br>"+locations[i]["info"]+"<br><img src='"+locations[i]["img_link"]+"' width='450'>");
                        }

                        infowindow.open(map, marker);
                    }
                })(marker, i));
            }
        }

        // Adds a marker to the map and push to the array.
        function addMarker(location) {
            var marker = new google.maps.Marker({
                position: location,
                map: map
            });

            markers.push(marker);
            mapVue.newMarker(location.lat(), location.lng());
        }

        // Sets the map on all markers in the array.
        function setMapOnAll(map) {
            for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(map);
            }
        }

        // Removes the markers from the map, but keeps them in the array.
        function clearMarkers() {
            setMapOnAll(null);
        }

        // Shows any markers currently in the array.
        function showMarkers() {
            setMapOnAll(map);
        }

        // Deletes all markers in the array by removing references to them.
        function deleteMarkers() {
            clearMarkers();
            markers = [];
        }
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key={{ env("GOOGLE_API") }}&callback=initMap">
    </script>
@endsection

