<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 28.08.2017.
 * Time: 14:13
 */

        ?>

<div class="modal fade" id="create-note-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <p class="close" data-dismiss="modal"><i class="material-icons">&#xE14C;</i></p>
                <h4 class="text-center">@lang('modal.notes_title')</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                        <form id="notes-form" action="/notes" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="number" id="notes_id" name="notes_id" value="0" hidden>
                            <div class="form-group">
                                <label for="note_title" class="control-label">@lang('modal.notes_title_lb')</label>
                                <input type="text" class="form-control" name="title" id="note_title">
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" name="body" id="note_body" rows="5"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="" class="control-label">@lang('modal.notes_color_lb')</label>
                                <input class="notes-color-options" type="radio" name="notes_color" value="1" checked="checked"> <i style="color: #fff; background-color: #212121; font-size: 16px;" class="material-icons">&#xE80E;</i>
                                <input class="notes-color-options" type="radio" name="notes_color" value="2"> <i style="color: #ff5252; font-size: 16px;" class="material-icons">&#xE80E;</i>
                                <input class="notes-color-options" type="radio" name="notes_color" value="3"> <i style="color: #FFD54F; font-size: 16px;" class="material-icons">&#xE80E;</i>
                                <input class="notes-color-options" type="radio" name="notes_color" value="4"> <i style="color: #AED581; font-size: 16px;" class="material-icons">&#xE80E;</i>
                                <input class="notes-color-options" type="radio" name="notes_color" value="5"> <i style="color: #4FC3F7; font-size: 16px;" class="material-icons">&#xE80E;</i>
                                <input class="notes-color-options" type="radio" name="notes_color" value="6"> <i style="color: #64B5F6; font-size: 16px;" class="material-icons">&#xE80E;</i>
                                <input class="notes-color-options" type="radio" name="notes_color" value="7"> <i style="color: #EEEEEE; font-size: 16px;" class="material-icons">&#xE80E;</i>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-custom">@lang('modal.notes_upload_btn')</button>
                            </div>
                        </form>
                    </div>
                </div>
                <hr>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="delete-note-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <p class="close" data-dismiss="modal"><i class="material-icons">&#xE14C;</i></p>
                <h4 class="text-center">@lang('modal.notes_title_2')</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                        <form id="notes-delete-form" action="/notes/remove" method="post">
                            {{csrf_field()}}
                            <input type="number" id="hidden_note_id" name="note_id" value="" hidden>
                            <div class="form-group">
                                <h4>@lang('modal.notes_confirm_message') <span id="remove-item-holder"></span>?</h4>
                                <div class="form-inline">
                                    <div class="row">
                                        <div class="col-xs-7">
                                            <button type="submit" class="btn btn-custom-secondary">@lang('modal.budget_accept')</button>
                                        </div>
                                        <div class="col-xs-5">
                                            <button type="button" data-dismiss="modal" class="btn btn-custom">@lang('modal.budget_cancel')</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <hr>
            </div>
        </div>
    </div>
</div>

