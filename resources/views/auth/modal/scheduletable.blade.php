<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 14.12.2017.
 * Time: 19:00
 */


?>

<div class="modal fade" id="schedule-table-view">
    <div class="modal-dialog" style="width: 85%;">
        <div class="modal-content">
            <div class="modal-header">
                <h3>@lang('schedule.title_2')</h3>
            </div>
            <div class="modal-body">
                <table class="table table-responsive table-bordered">
                    <thead>
                    <tr style="background-color: #03A9F4; color: #fff;">
                        <th>@lang('schedule.date_tb')</th>
                        <th>@lang('schedule.place_tb')</th>
                        <th>@lang('schedule.title_tb')</th>
                        <th>@lang('schedule.description_tb')</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
                <h3 id="travel-id-modal" style="word-wrap: break-word;"></h3>
            </div>
        </div>
    </div>
</div>
