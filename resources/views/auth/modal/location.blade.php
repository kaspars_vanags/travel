<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 14.12.2017.
 * Time: 19:01
 */

?>


<div class="modal fade" id="add-location-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <p class="close" data-dismiss="modal"><i class="material-icons">&#xE14C;</i></p>
                <h4 class="text-center">@lang('schedule.title_3')</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                        <form id="add-location-form" action="/scheduler/location/add" method="post">
                            {{csrf_field()}}
                            <input type="hidden" name="scheduler_id" value="{{ $response["scheduleId"] }}" hidden required>
                            <div class="form-group">
                                <label for="location" class="control-label">@lang('schedule.location_lb')</label>
                                <input type="text" name="location" id="location" class="form-control" required="required">
                            </div>
                            <div class="form-group">
                                <label for="currency" class="control-label">@lang('schedule.color_lb')</label>
                                <input type="color" name="location-color" id="location-color" class="form-control">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-custom-secondary">
                                    @lang('schedule.add_location')
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

