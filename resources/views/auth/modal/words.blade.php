<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 11.09.2017.
 * Time: 21:48
 */
        ?>

<div class="modal fade" id="add-word-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <p class="close" data-dismiss="modal"><i class="material-icons">&#xE14C;</i></p>
                <h4 class="text-center">@lang('modal.word_title')</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                        <form action="/word" method="post">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="original_word" class="control-label">@lang('modal.org_name_lb') <span style="color: crimson;">*</span></label>
                                <input type="text" class="form-control" name="original_word" id="original_word" required>
                            </div>
                            <div class="form-group">
                                <label for="translated_word" class="control-label">@lang('modal.trans_name_lb') <span style="color: crimson;">*</span></label>
                                <input type="text" class="form-control" name="translated_word" id="translated_word" required>
                            </div>
                            <div class="form-group">
                                <label for="language">@lang('modal.select_lang_lb')</label>
                                <select id="language" name="language" class="form-control">
                                    @foreach($response["languages"] as $lan)
                                        <option value="{{ $lan['language_id'] }}">{{ $lan['long_name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-custom">@lang('modal.add_word_btn')</button>
                            </div>
                        </form>
                    </div>
                </div>
                <hr>
            </div>
        </div>
    </div>
</div>

