<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 11.09.2017.
 * Time: 18:57
 */
        ?>

<div class="modal fade" id="add-language-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <p class="close" data-dismiss="modal"><i class="material-icons">&#xE14C;</i></p>
                <h4 class="text-center">@lang('modal.language_title')</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                        <form action="/language" method="post">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="long_name" class="control-label">@lang('modal.long_name_lb') <span style="color: crimson;">*</span></label>
                                <input type="text" class="form-control" name="long_name" id="long_name" required>
                            </div>
                            <div class="form-group">
                                <label for="short_name" class="control-label">@lang('modal.short_name_lb')</label>
                                <input type="text" class="form-control" name="short_name" id="short_name">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-custom">@lang('modal.add_lang_btn')</button>
                            </div>
                        </form>
                    </div>
                </div>
                <hr>
            </div>
        </div>
    </div>
</div>
