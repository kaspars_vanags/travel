<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 04.08.2017.
 * Time: 20:08
 */

        ?>


@extends("welcome")
@section("body")

    <div class="row">
        <div class="col-sm-8 col-sm-offset-2 col-xs-12">
            <div class="well" style="margin-top: 50px;">
                <h2 class="text-center">{{ $travel["name"] }}</h2>
                <hr>
                <form id="schedul-creator" method="POST" action="/scheduler/register/{{ $travel["id"] }}">
                    {{ csrf_field() }}
                    <div class="form-inline">
                        <div class="form-group">
                            <label class="control-label" for="date-from">@lang('modal.sch_date_from_lb')</label>
                            <input type="date" name="date-from" id="date-from" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="date-to">@lang('modal.sch_date_to_lb')</label>
                            <input type="date" name="date-to" id="date-to" class="form-control" required>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label class="control-label" for="location">@lang('modal.sch_color_ind_lb')</label><br>
                        <em>@lang('modal.sch_ext_description_lb')</em><br>
                        <div class="form-inline" style="margin-top: 10px;">
                            <input type="text" name="location" id="location" class="form-control">
                            <input type="color" name="location-color" id="location-color" class="form-control" style="width:75px;">
                            <button type="button" id="add-location" class="btn btn-custom-secondary" style="border-radius: 0px; width: 125px;">@lang('modal.sch_add_btn')</button>
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-custom" type="submit" style="width: 125px; border-radius: 0px;">@lang('modal.sch_create_btn')</button>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label class="control-label">@lang('modal.sch_loc_list_lb')</label>
                        <div id="location-container">
                            <!-- Add all locations and color -->
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section("scripts")
    <script>
        var i=0,j=0;
        var locationContainer = $("#location-container");

        $("#add-location").on("click", function(){
            var location = $("#location").val();
            var color = $("#location-color").val();

            if(location == ""){
                alert("Please enter location");
                return;
            }

            var newElement = "<div class='form-group' id='element_"+i+"'>" +
                    "<h3>"+location+" : "+color+"</h3>" +
                    "<input type='hidden' name='location_"+i+"' value='"+location+"'>" +
                    "<input type='hidden' name='location_color_"+i+"' value='"+color+"'>" +
                    "<button type='button' data-element='"+i+"' class='btn btn-danger remove-element' style='width: 125px; border-radius: 0px;'>@lang('modal.sch_remove_btn')</button>" +
                    "</div>";
            locationContainer.append(newElement);
            i++;
        });

    </script>
@endsection
