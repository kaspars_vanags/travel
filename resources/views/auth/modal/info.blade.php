<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 27.08.2017.
 * Time: 16:54
 */

        ?>


<div class="modal fade" id="user-info-box-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <p class="close" data-dismiss="modal"><i class="material-icons">&#xE14C;</i></p>
                <h4 class="text-center">@lang('modal.profile_title')</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                        <form action="/profile/settings/image" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="profile_image_upload" class="control-label">@lang('modal.profile_image_lb')</label>
                                <input type="file" class="form-control" name="profile_image" id="profile_image_upload">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-custom-secondary">@lang('modal.profile_upload_btn')</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                        <form action="/user/settings" method="post">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="color_scheme" class="control-label">@lang('modal.profile_color_scheme_lb')</label>
                                <select class="form-control" id="color_scheme" name="color_scheme">
                                    <option value="default.css" @if(Auth::user()->settings()->color_scheme == "default.css") selected="selected" @endif>Default</option>
                                    <option value="light.css" @if(Auth::user()->settings()->color_scheme == "light.css") selected="selected" @endif>Light</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-custom-secondary">@lang('modal.profile_save_btn')</button>
                            </div>
                        </form>
                    </div>
                </div>
                <hr>
            </div>
        </div>
    </div>
</div>
