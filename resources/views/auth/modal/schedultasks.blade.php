<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 21.08.2017.
 * Time: 19:26
 */

        ?>

<div class="modal fade" id="create-schedule-task-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="text-center">@lang('modal.sch_task_title')</h4>
                <p class="text-center" id="scheduled_date_label"></p>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                        <form id="create-task-form" {{-- action="/task" method="POST"--}}>
                            {{ csrf_field() }}
                            <div class="form-group">
                                {{--<label class="control-label" id="scheduled_date_label"></label>--}}
                                <input type="hidden" name="scheduled_date" id="scheduled_date">
                            </div>
                            <div class="form-group">
                               {{-- <label class="control-label" for="title">Title</label>--}}
                                <input type="text" class="form-control" name="title" id="title" placeholder="@lang('modal.sch_task_title_lb')" required="required">
                                <p class="field_title error-holder"></p>
                            </div>
                            <div class="form-group">
                               {{-- <label class="control-label" for="description">Description</label>--}}
                                <input type="text" class="form-control" name="description" id="description" placeholder="@lang('modal.sch_task_description_lb')">
                                <input type="hidden" name="schedule_id" id="schedule_id" value="{{ $response["scheduleId"] }}" hidden>
                                <p class="field_description error-holder"></p>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="location">@lang('modal.sch_task_time_lb')</label>
                                <input type="time" id="schedule_time" name="schedule_time" class="form-control">
                                <p class="field_schedule_time error-holder"></p>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="location">@lang('modal.sch_task_location_lb')</label>
                                <input type="text" id="location" name="location" class="form-control" required="required">
                                <p class="field_location error-holder"></p>
                        {{--        <select name="location" id="location" class="form-control" required="required">
                                    @foreach($response["locations"] as $key=>$loc)
                                        <option value="{{$loc[0]}},{{$loc[1]}}">{{$loc[0]}}</option>
                                    @endforeach
                                </select>--}}
                            </div>
                            <hr>
                            <div class="form-group">
                                <button type="button" @click="addTask" class="btn btn-custom-secondary">@lang('modal.sch_task_save_btn')</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="edit-schedule-task-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <p class="close" data-dismiss="modal"><i class="material-icons">&#xE14C;</i></p>
                <h4 class="text-center">@lang('modal.sch_task_title_2')</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                        <form id="edit-task-form">
                            {{ csrf_field() }}
                            <task-editor {{--v-bind:task="selectedTask"--}} v-model="selectedTask" v-on:update-task="updateTask(selectedTask)"></task-editor>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

