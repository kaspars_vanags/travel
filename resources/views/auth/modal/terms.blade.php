<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 16.12.2017.
 * Time: 0:05
 */

?>


<div class="modal fade" id="terms-and-condition-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <p class="close" data-dismiss="modal"><i class="material-icons">&#xE14C;</i></p>
                <h4 class="text-center">@lang('modal.terms_and_condition')</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                        <h4>TRIPSAVAGE.COM Website Terms & Conditions</h4>
                        <p>
                        You must read and agree to these terms and conditions before your subscription can be activated. Please read them carefully.
                            This Agreement shall be governed by and construed in accordance with the laws of United Kingdom.
                        </p>
                        <p>
                        For good and valuable consideration, the sufficiency of which is acknowledged by you and the tripsavage.com,
                            you hereby agree to become a subscriber to tripsavage.com (the "Service"), and agree to be bound by
                            all the terms and conditions set forth in this agreement (the "Agreement").
                            The parties to this Agreement are ("You" or "Subscriber"), and the owner of tripsavage.com (the "Company").
                            Subject to the terms and conditions set forth in this Agreement, the Company agrees to provide you all the privileges of
                            a subscription to this site available to a Subscriber in good standing.
                            This Agreement is subject to change by the Company at any time, and changes are effective upon notice to the
                            Subscriber by e-mail, posting at or via hyperlink to this site, or by mail.
                        </p>
                        <p>
                        YOU HEREBY WARRANT AND AFFIRM THAT IT IS LEGAL TO VIEW THE MATERIALS WHERE YOU ARE LOCATED.
                        </p>
                        <p>
                        Payment for the Service provided to you at and/or through this site may be made by automatic credit
                            card or check debit and you hereby authorize the Company and/or its authorized agent, to
                            transact such payments on your behalf. Unless you have good reason to believe the credit card
                            (or other approved facility) you use to purchase your subscription is lost or stolen, you agree not to report
                            that credit card (or other approved facility) as lost or stolen. Nor will you dispute any authorized charge by
                            the Company or its authorized agent. You agree and acknowledge that if you fraudulently report the credit card
                            (or other approved facility) used to obtain the Service or goods from the Service as stolen, or if you
                            fraudulent report that an authorized charge by the Company or authorized agent is unauthorized,
                            you shall be liable to this authorized agent and the Company for liquidated damages of $10,000.00.
                            The cardholder can cancel the order and require a refund when the service is not provided at the
                            latest 30 days after the order took place. The liability for liquidated damages specified in this
                            Paragraph shall not limit any other liability you may have for breach(es) of any other terms, conditions,
                            promises and warranties set forth in this Agreement.
                        <b>Regular Subscriptions:</b> If you purchase a regular subscription, you authorize the Company and/or its authorized agent
                            to charge your credit card (or other approved facility) for periodic subscription fees according to
                            the then-current billing terms for the Service. You are responsible for paying periodic subscription
                            fees according to the then-current billing terms. Subscription fees are earned upon receipt. For your
                            convenience and satisfaction, all memberships will automatically renew upon expiration unless your subscription is
                            cancelled AT LEAST three (3) days prior to expiration. Subscription rates are subject to change at any time without notice.
                        <b>Terminating Subscriptions:</b> The Company or its authorized agent may terminate your Subscription at
                            any time, and without cause, by. If you wish to terminate your subscription you must either provide the
                            Company or its authorized agent notification by E-mail, or conventional mail. You agree to be personally
                            liable for all charges incurred by you during or through the use of this site. Your liability for such
                            charges shall continue after termination of your membership for any reason.
                        </p>
                        <h4>VIEWING, ACCESSING AND DOWNLOADING THE MATERIALS</h4>
                        <p>
                        You agree not to access, view, download, receive or otherwise use, or cause or enable others to access, view, download, receive or otherwise use materials,
                            directly or indirectly in places which the Company does not authorize such access, viewing, downloading, receipt or other use.
                            You acknowledge and agree that the company does not authorize any Materials to be accessed, viewed, downloaded, used by, transmitted, broadcast or otherwise
                            disseminated to any person or entity located in any and all areas prohibited by law ("Prohibited Areas").
                            You further acknowledge that the Company does not authorize you to cause or enable others to access,
                            view, download, receive or otherwise use the Materials, directly or indirectly.
                        You agree that any and all unauthorized access, viewing, downloading, receipt, duplication or
                            other use of the Materials in which you are directly or indirectly involved, including, but not limited to 1)
                            accessing, viewing, downloading, receiving or other use of the Materials in Prohibited Areas and 2)
                            causing or enabling others to access, view, download, receive or otherwise use the Materials, directly or
                            indirectly, shall constitute intentional infringement(s) of this site's and potentially others? intellectual
                            property rights and other rights in the Materials, and may also constitute a violation of the Company's
                            trademarks, copyrights and other rights, including, but not limited to, the right of privacy.
                        You agree to be personally liable and fully indemnify the Company and EpochEU for any and all damages directly,
                            indirectly and/or consequentially resulting from your attempted or actual unauthorized downloading or
                            other duplication of materials from the Service alone, or with or under the authority of, any other person(s),
                            including, without limitation, any governmental agency(ies), wherein such damages include, without limitation,
                            all direct and consequential damages directly or indirectly resulting from unauthorized downloading of
                            materials from this site including, but not limited to, damages resulting from loss of revenue, loss of
                            property, fines, attorney's fees and costs, including, without limitation, damages resulting from prosecution
                            and/or governmentally imposed seizure(s), forfeiture(s), and/or injunction(s).
                        </p>
                        <h4>LIMITED NON-EXCLUSIVE LICENSCE GRANTED TO SUBSCRIBER</h4>
                        <p>
                        Subject to the terms and conditions set forth herein, this site hereby grants you a limited,
                            non-exclusive and non-transferable license to use the Materials during the period in which
                            you are a current Subscriber in good standing. You may use the Materials only in accordance with the terms and
                            conditions of your membership, only on one computer at a time and, if this site makes downloadable copies of the
                            Materials available, you may make only a single copy of such Materials for your personal use and enjoyment.
                        You may not remove any propriety notices from Materials at any time. You may make no use of Materials
                            not expressly authorized herein or by prior express written authorization from Company. Prohibited uses,
                            include, without limitation: (1) permitting other individuals to directly or indirectly use the Materials;
                            (2) modifying, translating, reverse engineering, decompiling, disassembling the Materials
                            (except to the extent applicable laws specifically prohibit such restriction);
                            (3) making copies or creating derivative works based on the Materials except as provided herein;
                            (4) renting, leasing, or transferring any rights in the Materials; (5) removing any proprietary
                            notices or labels on the Materials; and (6) making any other use of the Materials. This license does
                            not grant you any rights to any software enhancements or updates of any kind.
                        </p>
                        <h4>NO EXPRESS OR IMPLIED WARRANTIES</h4>
                        <p>
                        You agree that Materials and all other services provided to you by Company are provided on an "AS IS"
                            basis, without warranties of any kind, including without limitation
                        </p>
                            <ul>
                                <li>
                                    1)any warranties as to the availability, accuracy, or content of Materials, information, products, or services;
                                </li>
                                <li>
                                    2) any warranties of merchantability or fitness for a particular purpose and non-infringement.
                                </li>
                            </ul>
                        <p>
                        The entire risk as to the quality and performance of the Materials and all services provided by Company is borne by you.
                        Should the Materials or any other service provided by Company prove defective and/or cause any damage to your computer or inconvenience to you, you,
                        and not Company, assume the entire cost and all damages which may result from any and all such defects.
                        This disclaimer of warranty constitutes an essential part of the Agreement. Some states do not allow exclusions of an
                            implied warranty, so this disclaimer may not apply to you and you may have other legal rights that vary from state to s
                            tate or by jurisdiction. Under no circumstances and under no cause of action or legal theory, shall Company,
                            its suppliers, licensees, resellers, or other subscribers, or their suppliers, licensees, resellers or subscribers be
                            liable to you or any other person for any indirect, special, incidental, or consequential damages of any
                            character including, without limitation, damages for loss goodwill, work stoppage, computer failure or
                            malfunction, or any and all other commercial damages resulting from any use of Materials or other use of this site.
                        Goods and Services Offered By Other Parties: Company does not screen or endorse advertisements or communications
                            submitted to this site by third-party licensees, advertisers, or subscribers for electronic dissemination
                            through this site. Subscribers are therefore advised to use their own judgment to evaluate all advertisements
                            and other communications available at or through the use of this site prior to purchasing goods and/or services
                            described at this site or otherwise responding to any communication at this site.
                        </p>
                        <h4>LIMITATION OF LIABILITY</h4>
                        <p>
                        Any liability of the Company and EpochEU, including without limitation any failure of performance,
                            error, omission, interruption, deletion, defect, delay in operation or transmission, communications line failure,
                            theft or destruction or unauthorized access to, alteration of, or use of records, whether for breach of contract,
                            tortuous behavior, negligence, or under any other cause or action, shall be strictly limited to the amount of membership
                            fee paid by or on behalf of the subscriber to Company for the preceding month. Some states do not allow the limitation or
                            exclusion of liability for incidental or consequential damages, so the above limitation or exclusion may not apply to you.
                            The Company is not liable for damages resulting from disseminating, failing to disseminate, or incorrectly or inaccurately
                            disseminating any Materials, data, advertisement or other communication at or through this site.
                        </p>
                        <h4>MISCELLANEOUS</h4>
                        <p>
                        If the Company should at any time provide any service which enables you to communicate with or otherwise share information with other Subscribers or persons providing any kind or service to Subscribers, you agree not to submit, publish, display, disseminate, or otherwise communicate any defamatory, inaccurate, abusive, threatening, offensive, or illegal material while connected to or otherwise directly or indirectly using this site or other services provided to you by Company. Transmission of such material or any material that violates any federal, state, or local law in the United States or anywhere else in the world is strictly prohibited and shall constitute a material breach of this Agreement entitling Company to immediately terminate all rights to access to this site. You are solely responsible for all information that you submit, publish, display, disseminate or otherwise communicate through this site even if a claim should arise after termination of service. If the Company provides any such service described herein, you agree that all messages and other communications by you shall be deemed to be readily accessible to all other Subscribers who are authorized to access this site and agree that all such messages and other communications shall not be deemed to be private or secure. Regardless of whether the Company provides any type of service described herein, you agree that you have hereby been informed and noticed that any and all messages and other communications which you submit to Company directly or through this site can be read by the operators and/or other agents of Company, whether or not they are the intended recipient(s).
                        Notices from this site to Subscribers may be given by means of e-mail, by general posting on this site, or by conventional mail. Communications from you to the Company may be made by e-mail, conventional mail or telephone. All questions, complaints, or notices to this site may be sent in the following manner: a. By means of the web site form; You are responsible for providing all personal computer and communications equipment necessary to gain access to you Subscription. Access to and use of you Subscription is through the use of a password.
                        This Agreement contains the entire agreement between the Subscriber and Company regarding Subscribers' use of this site, Materials and all materials directly and indirectly related thereto. This Agreement supersedes all prior written and oral understandings, writings, and representations and may only be amended upon notice by Company. This Agreement shall be governed by and construed under the laws of the State of California and the United States as applied to agreements between California state residents entered into and to be performed within the State of California, except as governed by Federal law. The application of the United Nations Convention of Contracts for the International Sale of Goods is expressly excluded. If any provision of this Agreement is held to be unenforceable for any reason, such provision shall be reformed only to the extent necessary to make it enforceable. Unless otherwise explicitly stated, the provisions of this Agreement shall survive its termination.
                        YOU HEREBY AFFIRM THAT YOU HAVE READ THIS ENTIRE AGREEMENT AND AGREE TO ALL ITS TERMS AND CONDITIONS BY CLICKING WHERE INDICATED BELOW AND BY AUTHORIZING THE USE OF YOUR CREDIT CARD FOR PAYMENT OF CHARGES AND FEES FOR YOUR OBTAINING A SUBSCRIPTION TO THIS SITE.
                        SELECT "I AGREE" TO INDICATE THAT YOU HAVE READ THE MEMBERSHIP AGREEMENT IN ITS ENTIRETY, UNDERSTAND ITS TERMS, CONSENT TO ALL THE TERMS AND CONDITIONS SET FORTH IN THE MEMBERSHIP AGREEMENT, REPRESENT AND WARRANT THAT YOU ARE CURRENTLY OVER THE AGE OF 18 YEARS.
                        IF YOU DO NOT AGREE TO ALL OF THE TERMS AND CONDITIONS SET FORTH IN THIS AGREEMENT, ARE NOT OVER 18 YEARS OF AGE, OR ARE IN AN UNAUTHORIZED DOWNLOADING LOCATION, LEAVE NOW!
                        </p>
                        <h4 class="text-center">
                        Copyright © 2017 - TRIPSAVAGE.COM - All Rights Reserved.
                        </h4>
                        <p>
                        This website uses Google Analytics, a web analytics service provided by Google, Inc. ("Google"). Google Analytics uses "cookies", which are text files placed on your computer, to help the website analyze how users use the site. The information generated by the cookie about your use of the website (including your IP address) will be transmitted to and stored by Google on servers in the United States . Google will use this information for the purpose of evaluating your use of the website, compiling reports on website activity for website operators and providing other services relating to website activity and internet usage. Google may also transfer this information to third parties where required to do so by law, or where such third parties process the information on Google's behalf. Google will not associate your IP address with any other data held by Google. You may refuse the use of cookies by selecting the appropriate settings on your browser, however please note that if you do this you may not be able to use the full functionality of this website. By using this website, you consent to the processing of data about you by Google in the manner and for the purposes set out above.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="privacy-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <p class="close" data-dismiss="modal"><i class="material-icons">&#xE14C;</i></p>
                <h4 class="text-center">@lang('modal.privacy')</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                        <h4>Privacy</h4>
                        <p>
                        We will collect data via a double-click cookie giving access to information such as age, interests and location.
                        </p>
                        <p>
                        We will not facilitate the merging of personally-identifiable information with non-personally identifiable information previously collected from Display Advertising features that is based on the DoubleClick cookie unless we have robust notice of, and the user's prior affirmative (i.e. opt in) consent to, that merger.
                        </p>
                        <p>
                        Information that we do collect is NOT shared with any third party companies, and is used solely for the purpose of improving the website.
                        </p>
                        <p>
                        Should you wish to opt out then please follow this link: <a href="https://tools.google.com/dlpage/gaoptout/">Google opt-out</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
