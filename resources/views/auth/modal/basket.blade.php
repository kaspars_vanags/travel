<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 11.12.2017.
 * Time: 22:17
 */

?>


<!-- Set budget modal -->


<div class="modal fade" id="set-budget-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <p class="close" data-dismiss="modal"><i class="material-icons">&#xE14C;</i></p>
                <h4 class="text-center">@lang('modal.basket_set_budget_title')</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                        <form action="/basket/{{ $response["travel"]->id }}/budget" method="post">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="currency" class="control-label">@lang('modal.budget_currency_lb') (@lang('modal.budget_optional'))</label>
                                <input type="text" name="currency" id="currency" @if(!is_null($response["travel"]->currency)) value="{{ $response["travel"]->currency }}" @endif class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="budget" class="control-label">@lang('modal.budget_amount_lb')</label>
                                <input type="number" step="0.01" min="0" name="budget" id="budget" @if(!is_null($response["travel"]->budget)) value="{{ $response["travel"]->budget }}" @endif class="form-control" required="required">
                            </div>
                            <div class="form-group">
                                <label for="budget_type" class="control-label">@lang('travel.budget_type') (@lang('travel.optional'))</label>
                                <select id="budget_type" name="budget_type" class="form-control">
                                    <option value="none" @if($response["travel"]->budget_type == "none") selected="selected" @endif>@lang('travel.type_none')</option>
                                    <option value="strict" @if($response["travel"]->budget_type == "strict") selected="selected" @endif>@lang('travel.type_strict')</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-custom-secondary">
                                    @lang('modal.budget_save_btn')
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                <hr>
            </div>
        </div>
    </div>
</div>

<!-- Add item modal -->

<div class="modal fade" id="add-item-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <p class="close" data-dismiss="modal"><i class="material-icons">&#xE14C;</i></p>
                <h4 class="text-center">@lang('modal.basket_add_item_title')</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                        <form id="add-basket-item-form" action="/basket/{{ $response["travel"]->id }}/item" method="post">
                            {{csrf_field()}}
                            <input type="number" id="hidden_item_id" name="item_id" value="0" hidden>
                            <div class="form-group">
                                <label for="item_name" class="control-label">@lang('modal.basket_add_item_name')</label>
                                <input type="text" name="item_name" id="item_name" class="form-control" required="required">
                            </div>
                            <div class="form-group">
                                <label for="description" class="control-label">@lang('modal.basket_add_description') (@lang('modal.budget_optional'))</label>
                                <input type="text" name="description" id="item_description" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="price" class="control-label">@lang('modal.basket_add_price') (@lang('modal.budget_optional'))</label>
                                <input type="number" min="0" step="0.01" name="price" id="price" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="optional" class="control-label">@lang('modal.budget_optional')</label>
                                <select name="optional" id="optional" class="form-control">
                                    <option value="0">@lang('modal.basket_optional_no')</option>
                                    <option value="1">@lang('modal.basket_optional_yes')</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-custom-secondary">@lang('modal.budget_save_btn')</button>
                            </div>
                        </form>
                    </div>
                </div>
                <hr>
            </div>
        </div>
    </div>
</div>


<!-- Remove item modal -->

<div class="modal fade" id="remove-item-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <p class="close" data-dismiss="modal"><i class="material-icons">&#xE14C;</i></p>
                <h4 class="text-center">@lang('modal.basket_remove_item_title')</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                        <form id="remove-item-form" action="/basket/{{ $response["travel"]->id }}/remove" method="post">
                            {{csrf_field()}}
                            <input type="number" id="hidden_remove_id" name="item_id" value="" hidden>
                            <div class="form-group">
                                <h4>@lang('modal.basket_confirm_message') <span id="remove-item-holder"></span>?</h4>
                                <div class="form-inline">
                                    <div class="row">
                                        <div class="col-xs-7">
                                            <button type="submit" class="btn btn-custom-secondary">@lang('modal.budget_accept')</button>
                                        </div>
                                        <div class="col-xs-5">
                                            <button type="button" data-dismiss="modal" class="btn btn-custom">@lang('modal.budget_cancel')</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <hr>
            </div>
        </div>
    </div>
</div>


<!-- Edit item  modal -->
{{--

<div class="modal fade" id="edit-item-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <p class="close" data-dismiss="modal"><i class="material-icons">&#xE14C;</i></p>
                <h4 class="text-center">@lang('modal.basket_edit_item_title')</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                        <form action="/notes" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                        </form>
                    </div>
                </div>
                <hr>
            </div>
        </div>
    </div>
</div>--}}
