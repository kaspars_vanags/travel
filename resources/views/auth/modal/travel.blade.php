<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 19.08.2017.
 * Time: 18:00
 */

        ?>

<div class="modal fade" id="create-travel-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <p class="close" data-dismiss="modal"><i class="material-icons">&#xE14C;</i></p>
                <h4 class="text-center">@lang('modal.travel_title')</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                        <form action="/travel" method="POST" id="create-travel">
                            {{ csrf_field() }}
                            <div class="form-group">
                               {{-- <label class="control-label" for="name">Travel Name</label>--}}
                                <input type="text" class="form-control" name="name" id="name" placeholder="@lang('modal.travel_name_lb')">
                            </div>
                            <div class="form-group">
                            {{--    <label class="control-label" for="description">Description</label>--}}
                                <input type="text" class="form-control" name="description" id="description" placeholder="@lang('modal.travel_description_lb')">
                                <input type="text" name="owner_id" value="{{ Auth::user()->user_id }}" hidden >
                            </div>
                            <hr>
                            <div class="form-group">
                                <button type="submit" class="btn btn-custom">@lang('modal.travel_create_btn')</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
