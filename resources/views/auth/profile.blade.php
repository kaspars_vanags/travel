<?php
/**
 * Created by PhpStorm.
 * User: kasparsvanags
 * Date: 29/07/2018
 * Time: 17:49
 */

?>

@extends("welcome")
@section("body")
    <div class="row" id="profile-container">
        <div class="col-sm-10 col-sm-offset-1 col-xs-12">
            <div class="row">
                <div class="col-sm-offset-1 col-sm-4 col-xs-12">
                    <div class="panel panel-default panel-custom-header">
                        <div class="panel-heading">
                            <h3 class="text-center">Basic Info</h3>
                        </div>
                        <div class="panel-body">
                            <p>
                                Te bus viss basic info !!! name, article count, travel count, etc
                            </p>
                            <ul>
                                <li>Image</li>
                                <li>Username</li>
                                <li>
                                    change password
                                </li>
                                <li>
                                    Delete account (GDPR)
                                </li>
                                <li>
                                    Change email address
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12">
                    <div class="panel panel-default panel-custom-header">
                        <div class="panel-heading">
                            <h3 class="text-center">Other</h3>
                        </div>
                        <div class="panel-body">
                            <p>Te bus viena puse visi skilli lietotajam ir, vai emblemas? tjip achivements</p>
                            <p>Otra puse varetu but tads ka menu ar iespejam uztaisit rakstu, nopirkt speju u.t.t.</p>
                            <div class="row">
                                <div class="col-sm-7 col-xs-12" style="background-color: lightcoral">
                                    <h3>Medalas</h3>
                                </div>
                                <div class="col-sm-5 col-xs-12" style="background-color: lightblue">
                                    <ul>
                                        <li>
                                            Create new article
                                        </li>
                                        <li>
                                            Buy skill
                                        </li>
                                        <li>
                                            ...
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 col-xs-12">
                    <div class="row" style="margin-top: 20px">
                    </div>
                </div>
            </div>
            <div class="row">

            </div>
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 col-xs-12">
                    <div class="row" style="margin-top: 20px">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="open-link-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="text-center">@lang('trips.travel_link')</h4>
                </div>
                <div class="modal-body">
                    <h3 id="travel-id-modal" style="word-wrap: break-word;"></h3>
                </div>
            </div>
        </div>
    </div>

@endsection

@section("scripts")
    <script>
    </script>
@endsection

