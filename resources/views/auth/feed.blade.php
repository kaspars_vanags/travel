<?php
/**
 * Created by PhpStorm.
 * User: Kaspars
 * Date: 1/15/2018
 * Time: 18:04
 */



    $articleExample = "Well, the way they make shows is, they make one show. That show's called a pilot.
                            Then they show that show to the people who make shows, and on the strength of that one show they decide if they're going to make more shows.
                            Some pilots get picked and become television programs. Some don't, become nothing. She starred in one of the ones that became nothing.";

?>


@extends("welcome")

@section("body")
    <div class="row" id="profile-container">
        <!-- ARTICLE, GALLERY, PROFILE HOLDER -->
        <div class="col-sm-6 col-sm-offset-2 col-xs-12">
            <div class="well achievement-box">
                <div class="info-box">
                    <div class="info-box-header lime-box"><p class="text-center"><i class="material-icons">stars</i></p></div>{{--<i class="material-icons">&#xE413;</i>--}}
                    <div class="info-box-body">
                        <p class="text-right">Latest achievement</p>
                        <h2 class="text-right">Internationally recognized pooper!</h2>
                    </div>
                    <div class="info-box-footer">
                        <p class="text-right">You have visited your profile from more than 5 different countries!</p>
                    </div>
                </div>
            </div>
            <!-- GALLERY BLOCK -->
            <div class="gallery-holder hidden-xs">
                <div class="row gallery-box">
                    <div class="col-xs-6 gallery-box-big">
                        <div class="gallery-box-amount">
                            <div class="gallery-box-amount__icon">
                                <i class="material-icons">camera_alt</i>
                            </div>
                            <div class="gallery-box-amount__number">
                                <p>127</p>
                            </div>
                        </div>
                        <h2 class="text-center gallery-box-title">Travel like cat</h2>
                    </div>
                    <div class="col-xs-5 col-xs-offset-1 local-advert local-advert__big">
                        <h1>Contest!</h1>
                    </div>
                </div>
            </div>
            <!-- SOME ARTICLES -->
            <div class="well article-holder">
                <div class="article-category-box">
                    <p class="text-center">Travels</p>
                </div>
                <div class="row article-box">
                        <div class="col-xs-12 col-sm-4 article-image">
                            <img src="https://www.pandotrip.com/wp-content/uploads/2016/12/RossHelen-Female-traveler-980x654.jpg" class="img-responsive">
                        </div>
                        <div class="col-xs-12 col-sm-8 article-text">
                            <div class="article-box-widget article-box-widget__left">
                                <p class="text-center"><i class="material-icons">thumb_up</i></p>
                            </div>
                            <div class="article-box-widget article-box-widget__middle">
                                <p class="text-center"><i class="material-icons">chat_bubble</i></p>
                            </div>
                            <div class="article-box-widget article-box-widget__right">
                                <p class="text-center"><i class="material-icons">more_horiz</i></p>
                            </div>
                            <h3>Travel when hungry! <span>By Lucifer</span></h3>
                            <p>
                                {{ substr($articleExample, 0, 150)."..." }}
                            </p>
                            <button class="btn btn-custom">Read more</button>
                        </div>
                </div>
            </div>

            <div class="well article-holder">
                <div class="article-category-box">
                    <p class="text-center">Travels</p>
                </div>
                <div class="row article-box">
                    <div class="col-xs-12 col-sm-4 article-image">
                        <img src="https://thepointsguy.com/wp-content/uploads/2015/09/Cat-with-Passport.jpg" class="img-responsive">
                    </div>
                    <div class="col-xs-12 col-sm-8 article-text">
                        <div class="article-box-widget article-box-widget__left">
                            <p class="text-center"><i class="material-icons">thumb_up</i></p>
                        </div>
                        <div class="article-box-widget article-box-widget__middle">
                            <p class="text-center"><i class="material-icons">chat_bubble</i></p>
                        </div>
                        <div class="article-box-widget article-box-widget__right">
                            <p class="text-center"><i class="material-icons">more_horiz</i></p>
                        </div>
                        <h3>Travel when hungry! <span>By ANtMAn2</span></h3>
                        <p>
                            {{ substr($articleExample, 0, 150)."..." }}
                        </p>
                        <button class="btn btn-custom">Read more</button>
                    </div>
                </div>
            </div>
            <div class="well article-holder">
                <div class="article-category-box">
                    <p class="text-center">Travels</p>
                </div>
                <div class="row article-box">
                    <div class="col-xs-12 col-sm-4 article-image">
                        <img src="http://www.anorak.co.uk/wp-content/uploads/2015/09/Screen-Shot-2015-09-28-at-17.13.50.png" class="img-responsive">
                    </div>
                    <div class="col-xs-12 col-sm-8 article-text">
                        <div class="article-box-widget article-box-widget__left">
                            <p class="text-center"><i class="material-icons">thumb_up</i></p>
                        </div>
                        <div class="article-box-widget article-box-widget__middle">
                            <p class="text-center"><i class="material-icons">chat_bubble</i></p>
                        </div>
                        <div class="article-box-widget article-box-widget__right">
                            <p class="text-center"><i class="material-icons">more_horiz</i></p>
                        </div>
                        <h3>Travel when hungry! <span>By Garčoks</span></h3>
                        <p>
                            {{ substr($articleExample, 0, 150)."..." }}
                        </p>
                        <button class="btn btn-custom">Read more</button>
                    </div>
                </div>
            </div>
            <div class="well article-holder">
                <div class="article-category-box">
                    <p class="text-center">Travels</p>
                </div>
                <div class="row article-box">
                    <div class="col-xs-12 col-sm-4 article-image">
                        <img src="https://pics.me.me/is-your-cat-about-to-leave-you-what-to-look-14805404.png" class="img-responsive">
                    </div>
                    <div class="col-xs-12 col-sm-8 article-text">
                        <div class="article-box-widget article-box-widget__left">
                            <p class="text-center"><i class="material-icons">thumb_up</i></p>
                        </div>
                        <div class="article-box-widget article-box-widget__middle">
                            <p class="text-center"><i class="material-icons">chat_bubble</i></p>
                        </div>
                        <div class="article-box-widget article-box-widget__right">
                            <p class="text-center"><i class="material-icons">more_horiz</i></p>
                        </div>
                        <h3>Travel when hungry! <span>By BannedTeemo</span></h3>
                        <p>
                            {{ substr($articleExample, 0, 150)."..." }}
                        </p>
                        <button class="btn btn-custom">Read more</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- SIDEBAR -->
        <div class="col-sm-3 hidden-xs">
            <div class="row">
                <div class="col-xs-12 sidebar-title-box">
                    <h3 class="text-center">Epic stuff</h3>
                </div>
            </div>
            <p style="margin-left: -15px; margin-top: 11px;">View other user adventures</p>
            <div class="row user-video">
                <div class="col-md-5 col-xs-12 user-video--image">
                    <i class="material-icons">play_circle_outline</i>
                </div>
                <div class="col-md-7 col-xs-12 user-video--description">
                    <h4>Our amazing travel in North Korea</h4>
                    <p>Views: 13,321</p>
                </div>
            </div>
            <div class="row user-video">
                <div class="col-md-5 col-xs-12 user-video--image">
                    <i class="material-icons">play_circle_outline</i>
                </div>
                <div class="col-md-7 col-xs-12 user-video--description">
                    <h4>Our amazing travel in North Korea</h4>
                    <p>Views: 13,321</p>
                </div>
            </div>
            <div class="row user-video">
                <div class="col-md-5 col-xs-12 user-video--image">
                    <i class="material-icons">play_circle_outline</i>
                </div>
                <div class="col-md-7 col-xs-12 user-video--description">
                    <h4>Our amazing travel in North Korea</h4>
                    <p>Views: 13,321</p>
                </div>
            </div>
            <div class="row user-video">
                <div class="col-md-5 col-xs-12 user-video--image">
                    <i class="material-icons">play_circle_outline</i>
                </div>
                <div class="col-md-7 col-xs-12 user-video--description">
                    <h4>Our amazing travel in North Korea</h4>
                    <p>Views: 13,321</p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 sidebar-title-box">
                    <h3 class="text-center">Other travelers</h3>
                </div>
            </div>
            <p style="margin-left: -15px; margin-top: 11px;">People who is looking for their next adventure</p>
            <div class="row suggested-user">
                <img src="https://images.moviepilot.com/images/c_limit,q_auto:good,w_600/creature_3_pettigrew-1416325146-sirius-black-is-charles-manson-in-freaky-harry-potter-concept-art-jpeg-178978/artwork-by-rob-bliss.jpg" class="suggested-user--image img-responsive img-circle">
                <div class="suggested-user--description">
                    <p class="suggested-user--description__name">Barak O'Neil</p>
                    <p class="suggested-user--description__next_travel">Next: <span class="suggested-user--description__link">Thailand</span>,
                        <span class="suggested-user--description__link">North Korea</span>, <span class="suggested-user--description__link">Russia</span>, <span class="suggested-user--description__link">Australia</span>,
                        <span class="suggested-user--description__link">...</span>
                    </p>
                 {{--   <p class="suggested-user--description__from">From: Spain</p>--}}
                </div>
            </div>
            <div class="row suggested-user">
                <img src="http://www.grani.lv/uploads/posts/2011-11/1320228246_zetler_otakue.jpg" class="suggested-user--image img-responsive img-circle">
                <div class="suggested-user--description">
                    <p class="suggested-user--description__name">Zaļā Liepa</p>
                    <p class="suggested-user--description__next_travel">Next: <span class="suggested-user--description__link">Canada</span>,
                        <span class="suggested-user--description__link">France</span>
                    </p>
                </div>
            </div>
            <div class="row suggested-user">
                <img src="https://static.highsnobiety.com/wp-content/uploads/2013/11/Kanye-West-Closes-His-Own-Label-Office-In-Milan-00.jpg" class="suggested-user--image img-responsive img-circle">
                <div class="suggested-user--description">
                    <p class="suggested-user--description__name">Average Dude</p>
                    <p class="suggested-user--description__next_travel">Next: <span class="suggested-user--description__link">Sweeden</span>,
                        <span class="suggested-user--description__link">Brazil</span>, <span class="suggested-user--description__link">Scotland</span>, <span class="suggested-user--description__link">Germany</span>,
                        <span class="suggested-user--description__link">...</span>
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("scripts")
    <script>

        function openLink(link) {
            $("#travel-id-modal").html("http://travel.niknais.com/affiliate/open/"+link);
            $("#open-link-modal").modal("show");
        }

    </script>
@endsection
