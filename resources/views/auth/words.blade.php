<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 07.09.2017.
 * Time: 21:24
 */

        ?>


@extends("welcome")
@section("body")
    <div class="row" id="profile-container">
        <div class="col-sm-10 col-sm-offset-1 col-xs-12">
            <div class="well">
                <div class="row">
                    <div class="col-sm-4 col-xs-12">
                        <button class="btn btn-custom" data-toggle="modal" data-target="#add-language-modal">@lang('wordBank.add_new_btn')</button>
                        @if(count($response['languages']) > 0)
                            <button class="btn btn-custom"  data-toggle="modal" data-target="#add-word-modal">@lang('wordBank.add_new_word_btn')</button>
                        @else
                            <button class="btn btn-custom disabled">@lang('wordBank.add_new_word_btn')</button>
                        @endif
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <h3 class="text-center">@lang('wordBank.title_dif_lang')</h3>
                        <div class="total-count-box">
                            <p class="text-center">{{ count($response["languages"]) }}</p>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <h3 class="text-center">@lang('wordBank.title_total_words')</h3>
                        <div class="total-count-box">
                            <p class="text-center">{{ count($response["words"]) }}</p>
                        </div>
                    </div>
                </div>
                <hr class="custom-hr">
                <div class="row">
                    <div class="col-sm-12 hidden-xs">
                        <table class="table table-bordered table-hover table-custom">
                            <thead>
                                <tr>
                                    <th>@lang('wordBank.original_tb')</th>
                                    <th>@lang('wordBank.translation_tb')</th>
                                    <th>@lang('wordBank.language_tb')</th>
                                    <th></th>
                                    <th>@lang('wordBank.original_tb')</th>
                                    <th>@lang('wordBank.translation_tb')</th>
                                    <th>@lang('wordBank.language_tb')</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                $_wordCounter = 0;
                            ?>
                            @foreach($response["words"] as $word)
                                @if($_wordCounter == 0)
                                <tr>
                                    <td class="word-original">{{ $word["word_origin"] }}</td>
                                    <td class="word-translated">{{ $word["word_translated"] }}</td>
                                    <td class="word-language">{{ $word["long_name"] }}</td>
                                    <?php
                                        $_wordCounter = 1;
                                    ?>
                                @else
                                    <td></td>
                                    <td class="word-original">{{ $word["word_origin"] }}</td>
                                    <td class="word-translated">{{ $word["word_translated"] }}</td>
                                    <td class="word-language">{{ $word["long_name"] }}</td>
                                </tr>
                                    <?php
                                    $_wordCounter = 0;
                                    ?>
                                @endif
                            @endforeach
                            @if((count($response["words"]) % 2) != 0)
                                </tr>
                            @endif

                            </tbody>
                        </table>
                    </div>
                    <div class="col-xs-12 visible-xs">
                        <table class="table table-bordered table-hover table-custom" style="font-size: 1em;">
                            <thead>
                                <tr>
                                    <th>@lang('wordBank.original_tb')</th>
                                    <th>@lang('wordBank.translation_tb')</th>
                                    <th>@lang('wordBank.language_tb')</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($response["words"] as $word)
                                <tr>
                                    <td class="word-original">{{ $word["word_origin"] }}</td>
                                    <td class="word-translated">{{ $word["word_translated"] }}</td>
                                    <td class="word-language">{{ $word["long_name"] }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include("auth.modal.language")
    @include("auth.modal.words")
@endsection

@section("scripts")
    <script>

    </script>
@endsection
