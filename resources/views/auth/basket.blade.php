<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 11.12.2017.
 * Time: 20:55
 */

?>

@extends("welcome")
@section("body")
    <div class="row" id="profile-container">
        <div class="col-sm-10 col-sm-offset-1 col-xs-12">
            <div class="well">
                <div class="row">
                    <div class="col-xs-12">
                        <h3 style="text-transform: uppercase">{{ $response["travel"]->name }}</h3>
                        <em>
                            @if(is_null($response["travel"]->budget))
                                @lang("basket.budget_not_set")
                            @else
                                @lang('basket.your_budget'): @if(!is_null($response["travel"]->currency))<span>{{ $response["travel"]->currency }}</span>@endif <span>{{ $response["travel"]->budget }}</span>
                            @endif
                        </em>
                    </div>
                </div>
                <hr class="custom-hr">
                <div class="row">
                    <div class="col-sm-3 col-xs-12">
                        <button id="set-budget" class="btn btn-custom">
                            @lang('basket.set_budget_btn')
                        </button>
                    </div>
                    <div class="col-sm-3 col-xs-12">
                        <button id="add-basket-item" class="btn btn-custom">
                            @lang('basket.add_item_btn')
                        </button>
                    </div>
                </div>
                <div class="row" style="margin-top: 20px;">
                    <div class="col-sm-10 col-xs-12">
                        <table class="table table-bordered table-hover table-custom">
                            <thead>
                            <tr>
                                <th>@lang('basket.item_name_tb')</th>
                                <th>@lang('basket.description')</th>
                                <th>@lang('basket.price')</th>
                                <th>@lang('basket.optional')</th>
                                <th>@lang('basket.edit_item')</th>
                                <th>@lang('basket.remove_item')</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($response["basket"] as $item)
                                <tr>
                                    <td>{{ $item["item_name"] }}</td>
                                    <td>{{ $item["description"] }}</td>
                                    <td>{{ $item["price"] }}</td>
                                    <td>@if($item["optional"]) @lang('basket.option_yes') @else @lang('basket.option_no') @endif</td>
                                    <td>
                                        <button data-item="{{$item['basket_id']}}" data-name="{{$item["item_name"]}}" data-description="{{ $item["description"] }}" data-price="{{ $item["price"] }}" data-optional="{{ $item["optional"] }}" class="basket-item-edit btn btn-custom">
                                            @lang('basket.edit_item')
                                        </button>
                                    </td>
                                    <td>
                                        <button data-item="{{$item['basket_id']}}" data-name="{{$item["item_name"]}}" class="basket-item-remove btn btn-custom">
                                            @lang('basket.remove_item')
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include("auth.modal.basket")
@endsection

@section("scripts")

    <script type="text/javascript">

        //budget opener
        $("#set-budget").on("click", function(){
            $("#set-budget-modal").modal("show");
        });

        $("#add-basket-item").on("click", function(){
            resetForm("add-basket-item-form");
            $("#add-item-modal").modal("show");
        });

        $(".basket-item-edit").on("click", function(){
            resetForm("add-basket-item-form");
            $("#hidden_item_id").val($(this).data("item"));
            $("#item_name").val($(this).data("name"));
            $("#item_description").val($(this).data("description"));
            $("#price").val($(this).data("price"));

            $("#optional").find("option[value='"+$(this).data("optional")+"']").attr("selected", "selected");

            $("#add-item-modal").modal("show");
        });


        $(".basket-item-remove").on("click", function(){
            resetForm("remove-item-form");

            $("#remove-item-holder").html($(this).data("name"));
            $("#hidden_remove_id").val($(this).data("item"));

            $("#remove-item-modal").modal("show");
        });

        $(document).ready(function(){
            $("#add-basket-item-form").validate({
                rules : {
                    "item_name": {
                        required: true,
                        minlength: 4,
                        maxlength: 40
                    },
                    "description": {
                        maxlength: 190
                    },
                    "price": {
                        min: 0,
                        max: 9999999999
                    }
                },
                messages : {
                    "title": {
                        required: "Field 'Item name' is required!",
                        minlength: "Name should be at least {0} characters long.",
                        maxlength: "Max title length is 40 characters."
                    },
                    "description": {
                        maxlength: "Max description length is 190 characters."
                    },
                    "price": {
                        min: "Price can't be less than zero.",
                        max: "What are you buying exactly?"
                    }
                }
            });
        });


        function resetForm(name){
            $("#"+name)[0].reset();
        }

    </script>

@endsection

