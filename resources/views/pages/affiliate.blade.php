<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 07.06.2017.
 * Time: 21:42
 */

        ?>

@extends("welcome")

@section("body")

    <div class="row" id="profile-container">
        @include("auth.widgets.sidebar")
        <div class="col-sm-6 col-sm-offset-3 col-xs-12">
            <div class="panel panel-custom" style="margin-top: 40px;">
                <div class="panel-heading">
                    <h3>@lang('affiliate.aff_title')</h3>
                </div>
                <div class="panel-body">
                    @if(!Auth::check())
                        <h3 class="text-center">@lang('affiliate.aff_error_message')</h3>
                        <a href="/" class="btn btn-danger" style="width: 100%; border-radius: 0px;">
                            @lang('affiliate.aff_login_btn')
                        </a>
                    @else

                        <h3>@lang('affiliate.aff_info_owner'): <span style="color: #ef473a;">{{ $name }}</span>&nbsp;&nbsp;@lang('affiliate.aff_info_destination'): <span style="color: #ef473a;">{{ $travel_name }}</span></h3>
                        <a href="/affiliate/join/{{ $token }}" class="btn btn-custom" style="width: 33%">
                            @lang('affiliate.aff_accept_btn')
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection

@section("scripts")

@endsection
