<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 20.05.2017.
 * Time: 15:53
 */



$date = date("w");
        ?>

@extends("welcome")

@section("body")


    <div class="row" id="login-container">
{{--        <div class="col-sm-8 hidden-xs">
        </div>--}}
        <div class="col-sm-8 col-sm-offset-2 col-xs-12" id="login-container-right">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 col-xs-12" id="login-form-box">
                    <div class="panel panel-default">
{{--                        <div class="panel-heading">
                            <div class="login-image">
                                <img src="img/assets/travel_logo.png" class="img-responsive center-block" width="100" style="margin-top: 30px; margin-bottom: 30px;">
                            </div>
                            <h2 class="text-center" id="login-form-title" style="text-transform: uppercase;">Trip Savage</h2>
                        </div>--}}
                        <div class="panel-body" style="padding-top: 0px; padding-bottom: 0px">
                            <div class="row">
                                <div class="col-sm-5 col-xs-12" style="padding: 30px; padding-top: 70px">
{{--                                    <div class="login-image">
                                        <img src="img/assets/travel_logo.png" class="img-responsive center-block" width="100" style="margin-top: 30px; margin-bottom: 30px;">
                                    </div>--}}

                                    <form action="/login" method="post" id="login-form">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="name" id="login_name" placeholder="@lang('auth.username')">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control" name="password" id="login_password" placeholder="@lang('auth.password')">
                                        </div>
                                        <div class="form-group">
                                            <p class="text-center">
                                                <button type="submit" class="btn btn-custom">@lang('auth.login_btn')</button>
                                            </p>
                                            <div class="form-group">
                                                <p class="text-center">
                                                    <button type="button" onclick="onSignIn('facebook')" class="btn btn-provider facebook"><i class="fab fa-facebook"></i>&nbsp;&nbsp;&nbsp;@lang('auth.provider_btn', ["provider" => "Facebook"])</button>
                                                </p>
                                            </div>
                                            <div class="form-group">
                                                <p class="text-center">
                                                    <button type="button" onclick="onSignIn('google')" class="btn btn-provider google"><i class="fab fa-google-plus"></i>&nbsp;&nbsp;&nbsp;@lang('auth.provider_btn', ["provider" => "Google"])</button>
                                                </p>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div id="login-form-sidebox" class="col-sm-7 col-xs-12"> <!--style="background-image: url('../../img/assets/login-{{$date}}-day.jpg');"-->
                                    <h2 class="text-center" id="login-form-title" >Trip Savage</h2>
                                    <ul>
                                        <li><i class="material-icons">&#xE155;</i> Create and customize your own travel planner</li>
                                        <li><i class="material-icons">&#xE3B6;</i> Use one of 100+ available travel guides</li>
                                        <li><i class="material-icons">&#xE55A;</i> Travel with ease and bulletproof plan</li>
                                        <li><i class="material-icons">&#xE80D;</i> Share your adventures with friends</li>
                                        <li><i class="material-icons">&#xE227;</i> Avoid paying more than you need</li>
                                    </ul>
                                    <p class="text-center">
                                        <button type="button" data-target="#register-modal" data-toggle="modal" class="btn btn-custom-secondary shadow-dark">@lang('auth.register_btn')</button>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="login-info-container">
        <div class="col-sm-6 col-sm-offset-3 col-xs-12">

            <h1 class="text-center login-info-titles">GET STARTED</h1>
            <hr class="login-hr">
            <div class="row" style="padding-top: 175px">
                <div class="col-sm-5 col-sm-offset-1 col-xs-12">
                    <div class="float-login-box">
                        <p class="float-login-number"><i class="material-icons">&#xE155;</i></p>
                    </div>
                    <h4 class="text-left login-small-info-title">Create and customize your own travel planner</h4>
                    <ul>
                        <li><i class="material-icons">&#xE14C;</i> Pick a destination</li>
                        <li><i class="material-icons">&#xE14C;</i> Set a length of holidays</li>
                        <li><i class="material-icons">&#xE14C;</i> Add places to visit</li>
                        <li><i class="material-icons">&#xE14C;</i> Be your own travel master</li>
                    </ul>
                </div>
                <div class="col-sm-6 col-xs-12">
                    <img src="{{ URL::asset("/img/assets/login/text_1.png") }}" width="100%">
                </div>
            </div>
            <div class="row" style="padding-top: 275px">
                <div class="col-sm-6 col-xs-12">
                    <img src="{{ URL::asset("/img/assets/login/text_2.png") }}" width="100%">
                </div>
                <div class="col-sm-5 col-sm-offset-1 col-xs-12">
                    <div class="float-login-box">
                        <p class="float-login-number"><i class="material-icons">&#xE80D;</i></p>
                    </div>
                    <h4 class="text-left login-small-info-title">Share, Join, Enjoy</h4>
                    <ul>
                        <li><i class="material-icons">&#xE14C;</i> Share your trip with others</li>
                        <li><i class="material-icons">&#xE14C;</i> Join to other member adventures</li>
                        <li><i class="material-icons">&#xE14C;</i> Enjoy your amazing next trip</li>
                    </ul>
                </div>
            </div>
            <div class="row" style="padding-top: 275px">
                <h3 class="text-center login-title-shadow ">TRIPS CREATED</h3>
                <div class="col-sm-8 col-sm-offset-2 col-xs-12 {{--login-total-count-img--}}" style="min-height: 450px;">
                    <img src="{{ URL::asset("/img/assets/login/text_3.png") }}" width="100%" style="position: absolute;">
                    <h1 id="trips-created-title" class="text-center"><i class="material-icons">&#xE145;</i> {{ $response["travel_count"] }}</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="register-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="text-center">@lang('auth.register_title')</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                            <form action="/register" method="post" id="register-form">
                                {{ csrf_field() }}
                                <div class="form-group">
                                   {{-- <label class="control-label" for="name">Username</label>--}}
                                    <input type="text" name="name" id="name" class="form-control" placeholder="@lang('auth.username')">
                                </div>
                                <div class="form-group">
                                   {{-- <label class="control-label" for="email">Email</label>--}}
                                    <input class="form-control" id="email" name="email" placeholder="@lang('auth.email')">
                                </div>
                                <div class="form-group">
                                    {{--<label class="control-label" for="password">Password</label>--}}
                                    <input type="password" class="form-control" id="password" name="password" placeholder="@lang('auth.password')">
                                </div>
                                <div class="form-group">
                                    {{--<label class="control-label" for="password">Password</label>--}}
                                    <input type="password" class="form-control" id="repeat-password" name="repeat-password" placeholder="@lang('auth.repeat_password')">
                                </div>
                                <div class="form-group">
                                    <p class="text-center">
                                        <button type="submit" class="btn btn-custom">@lang('auth.register_btn')</button>
                                    </p>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="login-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row">
                        <div id="iframe-container" class="col-xs-12 col-sm-10 col-sm-offset-1">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section("scripts")
    <div id="fb-root"></div>
    <script>
        window.fbAsyncInit = function() {
            FB.init({
                appId      : '510255572687593',
                cookie     : true,
                xfbml      : true,
                version    : 'v2.11',
                oauth      : true
            });
            FB.AppEvents.logPageView();
        };


        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));


        function statusChangeCallback(res) {
            console.log(res);
        }

        function checkStatus() {
            FB.getLoginStatus(function(response) {
                if(response.status === "connected") {
                    getCurrentAndLogIn(response);
                }/* else {
                    FB.login(function(response) {
                        if (response.authResponse){
                            getCurrentAndLogIn(response)
                        } else {
                            console.log('Auth cancelled.')
                        }
                    }, { scope: 'email' });
                }*/
            });
        }

        function getCurrentAndLogIn(res) {

            FB.api(
                '/me?fields=id,email,name',
                'GET',
                {},
                function(myUser) {
                    //window.location.replace("http://tripsavage.com/auth/facebook?name="+myUser.name+"&email="+myUser.email+"&id="+res.authResponse.userID+"&token="+res.authResponse.accessToken);
                    window.location.replace("http://tripsavage.com/auth/facebook?token="+res.authResponse.accessToken);
                }
            );
        }



    </script>


    <script>
        $(document).ready(function(){
            window.animatelo.bounceIn("#login-form-title");

            $("#register-form").validate({
                rules : {
                    password: "required",
                    "#repeat-password": {
                        equalTo: "#password"
                    }
                }
            });

        });


        function onSignIn(provider) {



/*            var profile = googleUser.getBasicProfile();
            console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
            console.log('Name: ' + profile.getName());
            console.log('Image URL: ' + profile.getImageUrl());
            console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.

            var id_token = googleUser.getAuthResponse();//.id_token;
            console.log(id_token);*/
            //console.log("ID Token: " + id_token);
            window.location.replace("http://tripsavage.com/redirect/"+provider);
            //auth/google?token="+id_token);

        }
    </script>
@endsection


