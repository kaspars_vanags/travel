<?php
/**
 * Created by PhpStorm.
 * User: Kaspars
 * Date: 2/12/2018
 * Time: 21:07
 */

?>

<div class="hidden-xs">
    <div class="hexagon hexagon__large hexagon__accent hexagon--position__first">
    </div>


    <div class="hexagon hexagon__middle hexagon__light_green hexagon--position__second">
    </div>

    <div class="hexagon hexagon__small hexagon__pink hexagon--position__third">
    </div>

    <div class="parallelogram--small">
    </div>


    <div class="hexagon hexagon__medium_large hexagon__grey hexagon--position_grey_first">

    </div>

    <div class="hexagon hexagon__middle hexagon__dark_grey hexagon--position_grey_second">
    </div>

    <div class="hexagon hexagon__middle hexagon__light_grey hexagon--position_grey_third">
    </div>

    <div class="parallelogram--medium">
    </div>



    <div class="hexagon hexagon__medium_large hexagon__light_blue hexagon--position__fourth">
    </div>

    <div class="hexagon hexagon__middle hexagon__yellow hexagon--position__fifth">
    </div>

    <div class="hexagon hexagon__middle hexagon__light_green hexagon--position__sixth">
    </div>

    <div class="hexagon hexagon__small hexagon__pink hexagon--position__seventh">
    </div>

    <div class="parallelogram--right-small">
    </div>

</div>




