<?php
/**
 * Created by PhpStorm.
 * User: Kaspars
 * Date: 2/22/2018
 * Time: 21:19
 */

?>


@extends("maintenance")

@section("body")
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
            <div class="well maintenance-well">
                <div class="row">
                    <div class="col-xs-8 col-xs-offset-2">
                        <h1 class="text-center"><i class="fa fa-wrench" aria-hidden="true"></i></h1>
                        <h1 class="text-center">Dear TripSavage user,</h1>
                        <hr>
                        <p>This website is currently undergoing maintenance. We do not eat, sleep, shower and definitely do not play any video games, just so you could get best experience.</p>
                        <h1 class="maintenance-time text-center"> FEB 24th -  APR 1st </h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection