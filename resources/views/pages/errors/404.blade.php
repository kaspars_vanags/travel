<?php
/**
 * Created by PhpStorm.
 * User: Kaspars
 * Date: 1/11/2018
 * Time: 20:31
 */

?>


@extends("welcome")
@section("body")
    <div class="row error-container" id="profile-container">
        <div class="col-sm-6 col-sm-offset-3 col-xs-12">
            <div class="well">
                <h1 class="text-left">{{ $response["code"].", ".$response["message"] }}</h1>
                <p class="text-left">{{ $response["description"] }}</p>
                <a href="/" class="btn btn-custom" style="width: 150px;">Home</a>
            </div>
        </div>
    </div>
@endsection

@section("scripts")
@endsection