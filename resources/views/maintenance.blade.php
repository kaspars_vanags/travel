<?php
/**
 * Created by PhpStorm.
 * User: Kaspars
 * Date: 2/22/2018
 * Time: 21:28
 */
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Travel | ADMIN</title>

    <!-- Fonts -->
    {{--<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">--}}
    <link href="{{ URL::asset("css/maintenance.css") }}" rel="stylesheet" type="text/css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <script src="{{ URL::asset("js/maintenance.js") }}"></script>

    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
</head>
<body>
<div class="container-fluid">
    @include("vendor.flash.message")
    @yield("body")
    <div class="footer">
        <p class="text-center">
            KAITO.D.H | MEDIA © 2017 &nbsp&nbsp&nbsp TRIP SAVAGE
        </p>
    </div>
</div>
@yield("scripts")
</body>
</html>
