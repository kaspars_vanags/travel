var screenWidth = 0;
var boxSize = 100;
var currentPosition = 0;
var screenSizes = 1;

var itemsInRow = 6;
var maxSlide = 0;


$(document).ready(function(){

    var paddingValue = 30;

    screenWidth = $("#schedule-dates > .col-xs-12")[0].scrollWidth-paddingValue;
    boxSize = screenWidth/itemsInRow;

    //fakeBoxes();
});



//Component 1


//Component 2




/*

var scheduleVue = new Vue({
    el: "#schedule-wrapper",
    data: {
        "date": [],
        "payload": [],
    },
    methods: {
        loadSchedule: function(){
        }
    },
    mounted: function(){
        this.$nextTick(function(){
            this.loadSchedule();
        });
    }
});
*/


//fake function

var fakeBoxes = function(dt, count){
    var sliderWrapper = $(".scheduler-dates-slider-wrapper");
    var sliderFloat = $(".scheduler-dates-slider-float");
    var sliderActive = "";
    var newBoxHtml = "";

    for(let i = 0; i<count; i++) {

        if(i == 0) {
            sliderActive = "schedule-date--container__active";

            //TODO: change later for current date if it isn't in future
            $("#add-task-button").data("datefield",dt[i].fullDate);
        }

        newBoxHtml +=  "<div data-fulldate='"+dt[i].fullDate+"' class='schedule-date--container "+sliderActive+"' style='width: "+boxSize+"px; height: "+boxSize+"px;'>" +
            "<div class='schedule-date--container__fitted'>" +
            "<div class='schedule-date--container__name'>" +
            "<h4> "+dt[i].name+"</h4>" +
            "</div>" +
            "<div class='schedule-date--container__number'>" +
            "<h1>"+dt[i].date+"</h1>" +
            "</div></div>";

        if(i == 0) {
            newBoxHtml += "<div class='schedule-date--container__triangle' style='margin-left: "+((boxSize/2)-10)+"px;'></div>";
        }

        newBoxHtml += "</div>";



        sliderActive = "";
    }

    $(".scheduler-dates-slider--navigation").css("line-height", boxSize+"px");
    sliderWrapper.css("width", screenWidth).css("height", boxSize+12);

    console.log("Box Size:" + boxSize + " Count: "+ count);
    sliderFloat.width(boxSize*count);
    sliderFloat.append(newBoxHtml);

    screenSizes = Math.round((boxSize*count)/screenWidth);
    maxSlide = (boxSize*count) - screenWidth;

    bindContainers();

};




function slideDates(direction){

    var sliderFloat = $(".scheduler-dates-slider-float");
    var maxWidth = $("#schedule-dates")[0].scrollWidth - boxSize;
console.log(maxSlide);
    switch(direction) {

        case "left" :
          //  console.log((screenWidth-(screenSizes*screenWidth)));

           // $("#schedule-dates")[0].scrollWidth;
            if(currentPosition >= 0) {
                sliderFloat.css("transform", "translateX(-"+maxWidth+"px)");
                currentPosition = (maxWidth*(-1));
            } else {
                currentPosition+=boxSize;
                sliderFloat.css("transform", "translateX("+currentPosition+"px)");
            }


/*            if((screenWidth-(screenSizes*screenWidth)) < (currentPosition-boxSize)){
                currentPosition -= boxSize;
                sliderFloat.css("transform", "translateX("+currentPosition+"px)");
            }*/
            break;
        case "right":

            if(currentPosition >= ((maxSlide)*-1)){
                currentPosition = 0;
                sliderFloat.css("transform", "translateX("+currentPosition+"px)");
            } else {
                currentPosition-=boxSize;
                sliderFloat.css("transform", "translateX("+currentPosition+"px)");
            }


            break;
        default:
            break;

    }
}




function bindContainers(){
    $(".schedule-date--container").on("click", function(){

        if(!$(this).hasClass("schedule-date--container__active")) {
            $(".schedule-date--container.schedule-date--container__active").find(".schedule-date--container__triangle").remove();
            $(".schedule-date--container.schedule-date--container__active").removeClass("schedule-date--container__active");

            changeActiveDate(this);

            if(typeof(changeActiveItem) != undefined) {
                changeActiveItem($(this).data("fulldate"));
            }
        }
    });


    $(".scheduler-dates-slider--navigation__left").on("click", function(){
        slideDates("left");
    });

    $(".scheduler-dates-slider--navigation__right").on("click", function(){
        slideDates("right")
    });

}

function changeActiveDate(element) {

    var self = $(element);
    var newTriangle = "<div class='schedule-date--container__triangle' style='margin-left: "+((boxSize/2)-10)+"px;'></div>";

    $("#add-task-button").data("datefield",self.data("fulldate"));

    self.addClass("schedule-date--container__active");
    self.append(newTriangle);

}

