<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 08.12.2017.
 * Time: 19:33
 */

return [
    'create_new' => 'Create new travel',
    'profile' => 'Profile',
    'your_trips' => 'Your trips',
    'notes' => 'Notes',
    'word_bank' => 'Word bank',
    'my_skills' => 'My Skills',
    'admin' => 'Admin',
    'logout' => 'Logout',
    'login' => 'Login',
    'home' => 'Home',
    'settings' => 'Settings'
];