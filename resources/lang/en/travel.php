<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 09.12.2017.
 * Time: 18:04
 */

return [
    'title' => 'Create Your Own Adventures',
    'travel_name_lb' => 'Travel name',
    'travel_description_lb' => 'Description',
    'optional' => 'Optional',
    'exit' => 'Exit',
    'next_step' => 'Next step',
    'finish' => 'Update & Finish',
    'previous_step' => 'Previous step',
    'currency' => 'Currency',
    'currency_info' => 'Example: Pounds, Dollars, GBP, $...',
    'budget' => 'Budget',
    'countries_lb' => 'Destination',
    'countries_info' => 'Your main destination point',
    'budget_type' => 'Budget restrictions',
    'type_none' => 'None (No restrictions)',
    'type_strict' => 'Strict (Can\'t go over budget)',

    //step 2
    'sch_date_from_lb' => 'From',
    'sch_date_to_lb' => 'To',
    'sch_color_ind_lb' => 'Add locations/indication color',
    'sch_ext_description_lb' => 'Main regions, cities, arias in which you will travel. It will allow you to manage your travel by locations.',
    'sch_add_btn' => 'ADD',
    'sch_create_btn' => 'Create',
    'sch_loc_list_lb' => 'Location list',
    'sch_remove_btn' => 'Remove',
    //step 3
    'invites_lb' => 'Invite',
    'invites_info' => 'Send invitation request to your friends. You can share link later from "Trip" section.',
    'invites_pl' => 'Email',

    //step 4
    'congratulations' => 'Congratulation!',
    'congrats_text' => 'Your trip has been created! You can see your active travels under "Trip" section.',

    //new
    'main_info' => 'Main',
    'dates_info' => 'Dates',
    'invite_info' => 'Invite'
];