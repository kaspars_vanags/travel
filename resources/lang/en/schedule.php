<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 08.12.2017.
 * Time: 20:18
 */

return [
    'title' => 'Affiliate trips',
    'title_2' => 'Travel plan',
    //buttons
    'open_btn' => 'Open table view',
    'edit_btn' => 'Edit',
    'delete_btn' => 'Delete',
    'add_task_btn' => 'Add task',
    //table
    'date_tb' => 'Date',
    'place_tb' => 'Place',
    'title_tb' => 'Title',
    'description_tb' => 'Description',
    'add_location' => 'Add location',

    //modal
    'title_3' => 'Add location',
    'location_lb' => 'Location name',
    'color_lb' => 'Color for location'
];