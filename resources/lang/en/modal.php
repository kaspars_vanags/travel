<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 08.12.2017.
 * Time: 21:34
 */

return [
    /*
     * Profile settings modal
     */
    'profile_title' => 'Profile settings',
    'profile_image_lb' => 'Upload new profile image',
    'profile_upload_btn' => 'Upload',
    'profile_save_btn' => 'Save',
    'profile_color_scheme_lb' => 'Color scheme',
    /*
     * Language adding modal
     */
    'language_title' => 'Add new language',
    'long_name_lb' => 'Long name',
    'short_name_lb' => 'Short name',
    'add_lang_btn' => 'Add language',
    /*
     * Word adding modal
     */
    'word_title' => 'Add new word',
    'org_name_lb' => 'Original name',
    'trans_name_lb' => 'Translation',
    'select_lang_lb' => 'Select language',
    'add_word_btn' => 'Add word',
    /*
     * Notes modal
     */
    'notes_title' => 'Create new note',
    'notes_title_lb' => 'Title',
    'notes_color_lb' => 'Notes color',
    'notes_upload_btn' => 'Upload',
    'notes_title_2' => 'Delete note',
    'notes_confirm_message' => 'Are you really want to remove',
    /*
     * Scheduler modal
     */
    'sch_date_from_lb' => 'Date from',
    'sch_date_to_lb' => 'Date to',
    'sch_color_ind_lb' => 'Add locations/indication color',
    'sch_ext_description_lb' => 'Main regions, cities, arias in which you will travel. It will allow you to manage your travel by locations.',
    'sch_add_btn' => 'ADD',
    'sch_create_btn' => 'Create',
    'sch_loc_list_lb' => 'Location list',
    'sch_remove_btn' => 'Remove',
    /*
     * Schedule task modal
     */
    'sch_task_title' => 'Create new task',
    'sch_task_title_2' => 'Edit task',
    'sch_task_title_lb' => 'Title',
    'sch_task_description_lb' => 'Description',
    'sch_task_location_lb' => 'Location',
    'sch_task_save_btn' => 'Save',
    'sch_task_time_lb' => 'Time',
    /*
     * Travel modal
     */
    'travel_title' => 'Create new travel',
    'travel_name_lb' => 'Travel name',
    'travel_description_lb' => 'Description',
    'travel_create_btn' => 'Create',

    /*
     * Basket modal's
     */

    'basket_set_budget_title' => 'Set budget',
    'basket_add_item_title' => 'Add item',
    'basket_remove_item_title' => 'Remove item',
    'basket_edit_item_title' => 'Edit item',
    'budget_currency_lb' => 'Currency',
    'budget_optional' => 'Optional',
    'budget_amount_lb' => 'Budget',
    'budget_save_btn' => 'Save',
    'basket_add_item_name' => 'Item name',
    'basket_add_description' => 'Description',
    'basket_add_price' => 'Price',
    'basket_optional_yes' => 'Yes',
    'basket_optional_no' => 'No',
    'basket_confirm_message' => 'Are you really want to remove',
    'budget_accept' => 'Accept',
    'budget_cancel' => 'Cancel',

    /*
     * Terms and Privacy
     */

    'terms_and_condition' => 'Terms & Conditions',
    'privacy' => 'Privacy',

];