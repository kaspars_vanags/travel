<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 11.12.2017.
 * Time: 20:56
 */


return [
    //labels
    'item_name_tb' => 'Name',
    'description' => 'Description',
    'price' => 'Price',
    'optional' => 'Optional',
    'remove_item' => 'Remove',
    'edit_item' => 'Edit',
    'option_no' => 'No',
    'option_yes' => 'Yes',
    //buttons
    'set_budget_btn' => 'Set budget',
    'add_item_btn' => 'Add item',
    //messages
    'budget_not_set' => 'You have not set budget for this trip. If you want you can do it now.',
    'your_budget' => 'Your budget',
];
