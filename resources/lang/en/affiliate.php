<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 08.12.2017.
 * Time: 21:58
 */

return  [
    'aff_title' => 'Accept trip',
    'aff_error_message' => 'You need login to join this trip',
    'aff_info_owner' => 'Trip owner',
    'aff_info_destination' => 'Trip destination',

    //buttons
    'aff_login_btn' => 'Login',
    'aff_accept_btn' => 'Accept',
];