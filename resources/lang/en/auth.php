<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'username' => 'Username',
    'password' => 'Password',
    'repeat_password' => 'Repeat password',
    'email' => 'Email',
    'register_title' => 'Register to TripSavage',
    'button_connector' => 'OR',
    //BUTTONS
    'login_btn'    => 'Login',
    'register_btn' => 'Sign up',
    'provider_btn'     => 'Login with :provider',

];
