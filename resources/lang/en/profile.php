<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 08.12.2017.
 * Time: 19:31
 */

return [
    'total_trips' => 'Trips',
    'total_affiliate_trips' => 'Affiliate',
    'saved_words' => 'Words',

    'total_trips_description' => 'Number of your trips',
    'total_affiliate_trips_description' => 'Trips which you have joined',
    'saved_words_description' => 'Words in "Word Bank"',

    'last_travels' => 'Last trips',
];