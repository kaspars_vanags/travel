<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 08.12.2017.
 * Time: 20:08
 */

return [
    //buttons
    'add_new_btn' => 'Add new language',
    'add_new_word_btn' => 'Add new word',
    //titles
    'title_dif_lang' => 'Different languages',
    'title_total_words' => 'Total words',
    //table
    'original_tb' => 'Original',
    'translation_tb' => 'Translation',
    'language_tb' => 'Language',
];