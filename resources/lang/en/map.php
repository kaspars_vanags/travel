<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 08.12.2017.
 * Time: 20:35
 */

return [
    'title' => 'Your markers',
    'title_2' => 'Edit marker',

    //buttons
    'go_back_btn' => 'Go back',
    'edit_btn' => 'Edit',
    'delete_btn' => 'Delete',
    'save_btn' => 'Save',

    //form and labels and
    'name_lb' => 'Name',
    'info_text_lb' => 'Info Text',
    'img_link_lb' => 'Img link',
    'will_visit_lb' => 'Will you visit it?',
    'type_lb' => 'Type',
    //options
    'yes_op' => 'Yes',
    'maybe_op' => 'Maybe',
    'no_op' => 'No',
    'object_op' => 'Object',
    'restaurant_op' => 'Restaurant',
    'hotel_op' => 'Hotel',
];