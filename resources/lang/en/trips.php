<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 08.12.2017.
 * Time: 19:45
 */

return [
    //table 1
    'title' => 'Your trips',
    'name_tb' => 'Name',
    'description_tb' => 'Description',
    'action_tb' => 'Action',
    'travel_p_tb' => 'Travel planer',
    'share_tb' => 'Share',
    'basket_tb' => 'Basket',
    //table 2
    'title_2' => 'Affiliate trips',
    'owner_tb' => 'Owner',
    'travel_tb' => 'Travel',
    'scheduler_tb' => 'Schedule',
    'url_tb' => 'Url',
    //buttons
    'open_btn' => 'Open',
    'edit_btn' => 'Edit',
    'scheduler_btn' => 'Scheduler',
    'link_btn' => 'Link',
    'share_btn' => 'Share',
    'basket_btn' => 'Basket',
    //titles
    'travel_link' => 'Travel link',
    'owner' => 'Owner',
    'incomplete' => 'Incomplete',
];