<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 08.12.2017.
 * Time: 19:45
 */

return [
    //table 1
    'title' => 'Jūsu ceļojumi',
    'name_tb' => 'Nosaukums',
    'description_tb' => 'Apraksts',
    'action_tb' => 'Darbība',
    'travel_p_tb' => 'Plānotājs',
    'share_tb' => 'Dalīties',
    'basket_tb' => 'Grozs',
    //table 2
    'title_2' => 'Saistītie ceļojumi',
    'owner_tb' => 'Īpašnieks',
    'travel_tb' => 'Nosaukums',
    'scheduler_tb' => 'Plānotājs',
    'url_tb' => 'Saite',
    //buttons
    'open_btn' => 'Atvērt',
    'edit_btn' => 'Labot',
    'scheduler_btn' => 'Plānotājs',
    'link_btn' => 'Saite',
    'share_btn' => 'Dalīties',
    'basket_btn' => 'Grozs',
    //titles
    'travel_link' => 'Ceļojuma saite',
    'owner' => 'Autors',
    'incomplete' => 'Nepabeigts',
];