<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 08.12.2017.
 * Time: 21:34
 */

return [
    /*
     * Profile settings modal
     */
    'profile_title' => 'Profila uzstādījumi',
    'profile_image_lb' => 'Augšuplādēt profila attēlu',
    'profile_upload_btn' => 'Augšuplādēt',
    'profile_save_btn' => 'Saglabāt',
    'profile_color_scheme_lb' => 'Krāsu palete',
    /*
     * Language adding modal
     */
    'language_title' => 'Pievienot valodu',
    'long_name_lb' => 'Pilnais nosaukums',
    'short_name_lb' => 'Abreviatūra',
    'add_lang_btn' => 'Pievienot',
    /*
     * Word adding modal
     */
    'word_title' => 'Pievienot tulkojumu',
    'org_name_lb' => 'Oriģinālais',
    'trans_name_lb' => 'Tulkojums',
    'select_lang_lb' => 'Izvēlēties valodu',
    'add_word_btn' => 'Pievienot',
    /*
     * Notes modal
     */
    'notes_title' => 'Izveidot piezīmi',
    'notes_title_lb' => 'Virsraksts',
    'notes_color_lb' => 'Piezīmes krāsa',
    'notes_upload_btn' => 'Pievienot',
    'notes_title_2' => 'Dzēst piezīmi',
    'notes_confirm_message' => 'Vai Jūs tiešām vēlaties dzēst',
    /*
     * Scheduler modal
     */
    'sch_date_from_lb' => 'Datums no',
    'sch_date_to_lb' => 'Datums līdz',
    'sch_color_ind_lb' => 'Pievienojiet lokācijas/indikātora krāsu',
    'sch_ext_description_lb' => 'Galvenie reģioni, pilsētas, apvidi kuros paredzēts ceļot. Šis ļaus grupēt plānotāju pēc lokācijām.',
    'sch_add_btn' => 'Pievienot',
    'sch_create_btn' => 'Izveidot',
    'sch_loc_list_lb' => 'Lokāciju saraksts',
    'sch_remove_btn' => 'Izņemt',
    'sch_task_time_lb' => 'Laiks',
    /*
     * Schedule task modal
     */
    'sch_task_title' => 'Izveidot uzdevumu',
    'sch_task_title_2' => 'Labot uzdevumu',
    'sch_task_title_lb' => 'Virsraksts',
    'sch_task_description_lb' => 'Apraksts',
    'sch_task_location_lb' => 'Lokācija',
    'sch_task_save_btn' => 'Pievienot',
    /*
     * Travel modal
     */
    'travel_title' => 'Izveidot ceļojumu',
    'travel_name_lb' => 'Ceļojuma nosaukums',
    'travel_description_lb' => 'Apraksts',
    'travel_create_btn' => 'Pievienot',

    /*
     * Basket modal's
     */

    'basket_set_budget_title' => 'Uzlikt budžetu',
    'basket_add_item_title' => 'Pievienot',
    'basket_remove_item_title' => 'Noņemt',
    'basket_edit_item_title' => 'Labot',
    'budget_currency_lb' => 'Valūta',
    'budget_optional' => 'Izvēles',
    'budget_amount_lb' => 'Budžets',
    'budget_save_btn' => 'Saglabāt',
    'basket_add_item_name' => 'Vienuma nosaukums',
    'basket_add_description' => 'Apraksts',
    'basket_add_price' => 'Cena',
    'basket_optional_yes' => 'Jā',
    'basket_optional_no' => 'Nē',
    'basket_confirm_message' => 'Vai Jūs tiešām vēlaties dzēst',
    'budget_accept' => 'Piekrist',
    'budget_cancel' => 'Atcelt',
];