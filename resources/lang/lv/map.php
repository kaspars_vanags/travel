<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 08.12.2017.
 * Time: 20:35
 */

return [
    'title' => 'Jūsu marķieri',
    'title_2' => 'Labot marķieri',

    //buttons
    'go_back_btn' => 'Atgriezties',
    'edit_btn' => 'Labot',
    'delete_btn' => 'Dzēst',
    'save_btn' => 'Saglabāt',

    //form and labels and
    'name_lb' => 'Nosaukums',
    'info_text_lb' => 'Apraksts',
    'img_link_lb' => 'Attēla saite',
    'will_visit_lb' => 'Vai apmeklēsiet?',
    'type_lb' => 'Tips',
    //options
    'yes_op' => 'Jā',
    'maybe_op' => 'Varbūt',
    'no_op' => 'Nē',
    'object_op' => 'Objekts',
    'restaurant_op' => 'Restorāns',
    'hotel_op' => 'Viesnīca',
];