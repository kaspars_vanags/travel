<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 08.12.2017.
 * Time: 21:58
 */

return  [
    'aff_title' => 'Pievienoties ceļojumam',
    'aff_error_message' => 'Jums vajag pieslēgties sistēmai, lai pievienotos ceļojumam.',
    'aff_info_owner' => 'Ceļojuma autors',
    'aff_info_destination' => 'Ceļojuma mērķis',

    //buttons
    'aff_login_btn' => 'Pieslēgties',
    'aff_accept_btn' => 'Piekrist',
];