<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 09.12.2017.
 * Time: 18:04
 */

return [
    'title' => 'Solis',
    'travel_name_lb' => 'Ceļojuma nosaukums',
    'travel_description_lb' => 'Apraksts',
    'optional' => 'Izvēles',
    'exit' => 'Iziet',
    'next_step' => 'Nākošais solis',
    'finish' => 'Pabeigt',
    'previous_step' => 'Solis atpakaļ',
    'currency' => 'Valūta',
    'currency_info' => 'Piemēri: Eiro, Dolāri, GBP, $...',
    'budget' => 'Budžets',
    'countries_lb' => 'Valsts (Galvaspilsēta)',
    'countries_info' => 'Jūsu galvenais ceļojuma mērķis',
    'budget_type' => 'Budžeta ierobežojumi',
    'type_none' => 'Nekāds (Bez ierobežojumiem)',
    'type_strict' => 'Ierobežots (Nevarēsiet pārsniegt budžetu)',

    //step 2
    'sch_date_from_lb' => 'Datums no',
    'sch_date_to_lb' => 'Datums līdz',
    'sch_color_ind_lb' => 'Pievienot lokācijas/indikātora krāsu',
    'sch_ext_description_lb' => 'Galvenie reģioni, pilsētas, apvidi uz kuriem jūs ceļosiet. Šie iedalījumi palīdzēts jums grupēt plānotāja ierakstus.',
    'sch_add_btn' => 'Pievienot',
    'sch_create_btn' => 'Izveidot',
    'sch_loc_list_lb' => 'Lokāciju saraksts',
    'sch_remove_btn' => 'Izmest',
    //step 3
    'invites_lb' => 'Uzaicināt',
    'invites_info' => 'Nosūtiet uzaicinājumu saviem draugiem. Jūs varēsiet dalīties ar saiti arī vēlāk no "Jūsu ceļojumi" sadaļas.',
    'invites_pl' => 'E-pasts',

    //step 4
    'congratulations' => 'Apsveicam!',
    'congrats_text' => 'Jūsu ceļojums ir izveidots! Jūs varat aplūkot savus aktīvos ceļojumus zem "Jūsu ceļojumi" sadaļas.',
];