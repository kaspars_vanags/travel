<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 08.12.2017.
 * Time: 18:55
 */

return [
    'username' => 'Lietotāja vārds',
    'password' => 'Parole',
    'repeat_password' => 'Atkārtot paroli',
    'email' => 'E-pasts',
    'register_title' => 'Reģistrēties TripSavage',
    'button_connector' => 'VAI',
    //BUTTONS
    'login_btn'    => 'Pieslēgties',
    'register_btn' => 'Reģistrēties',
    'provider_btn'     => 'Pieslēgties ar :provider',
];