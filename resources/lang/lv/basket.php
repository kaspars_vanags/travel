<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 11.12.2017.
 * Time: 20:56
 */


return [
    //labels
    'item_name_tb' => 'Nosaukums',
    'description' => 'Apraksts',
    'price' => 'Cena',
    'optional' => 'Izvēles',
    'remove_item' => 'Noņemt',
    'edit_item' => 'Labot',
    'option_no' => 'Nē',
    'option_yes' => 'Jā',
    //buttons
    'set_budget_btn' => 'Uzstādīt budžetu',
    'add_item_btn' => 'Pievienot',
    //messages
    'budget_not_set' => 'Jūs vēl neesat noteicis budžetu šim ceļojumam. Jūs varat to izdarīt tagad.',
    'your_budget' => 'Jūsu budžets',
];
