<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 08.12.2017.
 * Time: 20:08
 */

return [
    //buttons
    'add_new_btn' => 'Pievienot valodu',
    'add_new_word_btn' => 'Pievienot tulkojumu',
    //titles
    'title_dif_lang' => 'Valodu skaits',
    'title_total_words' => 'Vārdu skaits',
    //table
    'original_tb' => 'Oriģināls',
    'translation_tb' => 'Tulkojums',
    'language_tb' => 'Valoda',
];