<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 08.12.2017.
 * Time: 19:31
 */

return [
    'total_trips' => 'Visi ceļojumi',
    'total_affiliate_trips' => 'Visi kopīgotie ceļojumi',
    'saved_words' => 'Saglabātie vārdi'
];