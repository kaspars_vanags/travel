<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 08.12.2017.
 * Time: 20:18
 */

return [
    'title' => 'Saistītie ceļojumi',
    'title_2' => 'Ceļojuma plāns',
    //buttons
    'open_btn' => 'Atvērt tabulas skatu',
    'edit_btn' => 'Labot',
    'delete_btn' => 'Dzēst',
    'add_task_btn' => 'Pievienot',
    //table
    'date_tb' => 'Datums',
    'place_tb' => 'Vieta',
    'title_tb' => 'Nosaukums',
    'description_tb' => 'Apraksts',
    'add_location' => 'Pievienot lokāciju',

    //modal
    'title_3' => 'Pievienot lokāciju',
    'location_lb' => 'Lokācijas nosaukums',
    'color_lb' => 'Lokācijas indikātora krāsa'
];