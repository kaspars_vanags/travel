<?php
/**
 * Created by PhpStorm.
 * User: zurka_000
 * Date: 08.12.2017.
 * Time: 19:33
 */

return [
    'create_new' => 'Jauns ceļojums',
    'profile' => 'Profils',
    'your_trips' => 'Jūsu ceļojumi',
    'notes' => 'Piezīmes',
    'word_bank' => 'Vārdu krātuve',
    'my_skills' => 'Manas spējas',
    'admin' => 'Admins',
    'logout' => 'Atslēgties',
    'login' => 'Pieslēgties',
];