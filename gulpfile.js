const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(mix => {
    mix.sass('themes/default.scss', 'public/css/themes/default.css');
    mix.sass('themes/light.scss', 'public/css/themes/light.css');
    mix.sass('admin.scss').sass("maintenance.scss");

    mix.scripts('main.js','public/js/main.js');
    mix.scripts('admin.js','public/js/admin.js');
    mix.scripts('date-slider.js', 'public/js/date-slider.js');
});
